import React, { useRef, createRef, useEffect } from "react";

// import * as webix from 'webix/webix.js';
import * as webix from "@xbs/webix-pro/webix.js";
import "webix/webix.css";
import { $$ } from "@xbs/webix-pro";
//https://github.com/boypanjaitan16/webix-react-hooks/blob/cefe81ec8e53e83bce4b1a86de83e4a5ccace0be/src/components/WebixComponent.js

export function resizeWebixComponent() {
  let webixitems = document.getElementsByClassName("webixitem");
  let itemArray = Array.from(webixitems);
  itemArray.map((item) => {
    if ($$(item.children[0])) {
      // console.log("adjust",item.children[0]);
      // $$(item.children[0]).adjust() ;
      $$(item.children[0].getAttribute("view_id")).adjust();
    }
  });
}

export function scroll(view, id = "") {
  let v = { view: "scrollview", scroll: "y", css: "demoscroll", body: view };
  if (id != "") {
    v["id"] = id;
  }
  return v;
}

function UpdateItems(current, dataList) {
  current.reconstruct();
  // let n = current._cells.length;
  dataList.forEach((element) => {
    current.addView({
      view: "accordionitem",
      header: element.header,
      body: element.body,
      id: element.id,
      onAfterExpand: function (id, event) {
        console.log("onAfterExpand");
      },
    });
  });
  let n = current._cells.length;
  for (var i = 0; i < n - 1; i++) {
    current._cells[i].collapse();
  }
}

function WebixComponent({ data, ui, newui, newdata, height, id }) {
  const webixRef = createRef();
  const uiState = useRef();

  const setWebixData = (dataToUpdate) => {
    // console.log(uiState.current);
    // uiState.current.refresh();
    if (uiState.current["setValues"]) {
      // console.log("setWebixDataW setValues");
      uiState.current.setValues(dataToUpdate);
    } else if (uiState.current.parse) {
      // Использвуется для Table
      // console.log("setWebixDataW parse");
      uiState.current.parse(dataToUpdate);
    } else if (uiState.current.setValue) uiState.current.setValue(dataToUpdate);
    else if (uiState.current.addView) {
      console.log("uiState.current.addView");
      console.log("uiState.current.rows = ", uiState.current.index);
      let n = uiState.current._cells.length;
      //UpdateItems(uiState.current,dataToUpdate);
    }

    if (uiState.current.refresh) {
      uiState.current.refresh();
    }

    if (uiState.current["config"]["view"] == "scrollview") {
      // console.log("scrollview");
      if (uiState.current["_body_cell"]["config"]["view"] == "accordion") {
        // console.log("accordion");
        UpdateItems(uiState.current["_body_cell"], dataToUpdate);
      }
    }
    if (uiState.current["config"]["view"] == "accordion") {
      UpdateItems(uiState.current, dataToUpdate);
    }
  };
  // setWebixData(data);
  useEffect(() => {
    console.log("webixRef.current", webixRef.current);
    uiState.current = webix.ui(ui, webixRef.current);
    // Вопрос надо ли это оставить?
    return () => {
      ui.current = null;
    };
  }, []);

  useEffect(() => {
    // console.log("updateData",webixRef.current,newui);
    //
    if (newui) {
      // uiState.current  = null;
      // webixRef.current = null
      // uiState.current = ""
      // ref = document.getElementById("devicesInfoTab");

      let viewid = webixRef.current.children[0].getAttribute("view_id");
      // let item = webixRef.current.children[0];
      // console.log("Update ",viewid,newdata);
      if (viewid) {
        webix.ui(newui(newdata), $$(viewid));
      }
    } else if (data) {
      // console.log("data");
      console.log("use effect setWebixData", data);
      setWebixData(data);
    }
  }, [data]);
  let style;
  if (height) {
    style = { height: height };
  }
  return <div id={id} className="webixitem" style={style} ref={webixRef}></div>;
}

export default WebixComponent;
