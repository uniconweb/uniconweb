// const RequestHelper = require("./fr_requesthelper.mjs");
import RequestHelper from "./fr_requesthelper.mjs";

const REQ_GET_DEVICE = "GET_DEVICE"
const REQ_GET_MODULE = "GET_MODULE"
const REQ_GET_DEVICES = "GET_DEVICES"
const REQ_GET_STATUS = "GET_STATUS"
const REQ_GET_LINKS = "GET_LINKS"
const REQ_SYSTEM_INIT = "SYSTEM_INIT"
const REQ_GET_OSC = "GET_OSC"
const REQ_SET_OSC_SETTINGS = "SET_OSC_SETTINGS"
const REQ_GET_OSC_STREAM_OPEN = "GET_OSC_STREAM_OPEN"
const REQ_GET_OSC_STREAM_CLOSE = "GET_OSC_STREAM_CLOSE"

export class DeviceProvider {
    m_reqHelper = null;

    constructor(reqHelper = RequestHelper) {
        this.m_reqHelper = reqHelper;
    }

    requestHelper() {
        return this.m_reqHelper;
    }

    setRequestHelper(reqHelper = RequestHelper) {
        this.m_reqHelper = reqHelper;
    }
    
    request(cmd, timeout) {
        return this.requestHelper().request(cmd, true, timeout);
    }

    async reqDevice(sysTypeId, deviceId) {
        let reqObj = this._createDeviceReqObj(REQ_GET_DEVICE, sysTypeId, deviceId);

        return this.request(reqObj);
    };

    async reqDevices(sysTypeId) {
        let reqObj = this._createDeviceReqObj(REQ_GET_DEVICES, sysTypeId);

        return this.request(reqObj);
    };

    async reqLinks(sysTypeId) {
        let reqObj = this._createDeviceReqObj(REQ_GET_LINKS, sysTypeId);

        return this.request(reqObj);
    };

    async reqModule(sysTypeId, deviceId, moduleId) {
        let reqObj = this._createDeviceReqObj(REQ_GET_MODULE, sysTypeId, deviceId, moduleId);

        return this.request(reqObj);
    };

    async reqOsc(sysTypeId, deviceId, oscId, step) {
        if (oscId === undefined) oscId = deviceId
        let reqObj = this._createOscReqObj(REQ_GET_OSC, sysTypeId, deviceId, oscId, step);
        return this.request(reqObj, 10000); // 10000 for demo purpose only
    }

    async setOscSettings(sysTypeId, deviceId, settings) {
        let cmdName = REQ_SET_OSC_SETTINGS

        let reqObj = {
            sys_type_id: sysTypeId,
            cmd: this._deviceCmd(cmdName),
            body: this._deviceOscBody(deviceId, deviceId)
        };

        if (settings.displayResolution) {
            reqObj.body["display_resolution_ms"] = settings.displayResolution;
        }

        if (settings.trigMode) {
            reqObj.body["trig_mode"] = settings.trigMode;
        }

        return this.request(reqObj);
    }

    async reqOpenOscStream(sysTypeId, deviceId, oscId, oscVars, step) {
        let reqName = REQ_GET_OSC_STREAM_OPEN;
        let reqObj = {
            sys_type_id: sysTypeId,
            cmd: this._deviceCmd(reqName),
            body: this._deviceOscBody(deviceId, oscId, oscVars, step)
        }

        return this.request(reqObj);
    };

    async reqOscData(sysTypeId, deviceId, oscId, oscVars) {
        let reqName = REQ_GET_OSC_STREAM_OPEN;
        let reqObj = {
            sys_type_id: sysTypeId,
            cmd: this._deviceCmd(reqName),
            body: this._deviceOscBody(deviceId, oscId, oscVars, undefined, true)
        }

        let timeOut = 5000;
        return this.request(reqObj, timeOut);
    };

    async reqCloseOscStream(sysTypeId, deviceId, oscId) {
        let cmd = REQ_GET_OSC_STREAM_CLOSE;
        let reqObj = this._createOscReqObj(cmd, sysTypeId, deviceId, oscId);

        return this.request(reqObj);
    }

    async reqStatus(sysTypeId) {
        let reqObj = this._createDeviceReqObj(REQ_GET_STATUS, sysTypeId);

        return this.request(reqObj);
    };

    async reqSystemInit(sysTypeId) {
        let req = {
            sys_type_id: sysTypeId,
            cmd: this._deviceCmd(REQ_SYSTEM_INIT)
        };

        return this.request(req);
    };

    _createDeviceReqObj(reqName, sysTypeId, deviceId, moduleId) {
        let req = {
            sys_type_id: sysTypeId,
            cmd: this._deviceCmd(reqName),
            body: this._deviceBody(deviceId, moduleId)
        };

        return req;
    }

    _createOscReqObj(reqName, sysTypeId, deviceId, oscId, step) {
        
        let req = {
            sys_type_id: sysTypeId,
            cmd: this._deviceCmd(reqName),
            body: this._deviceOscBody(deviceId, oscId, undefined, step)
        };

        return req;
    }

    _deviceCmd(reqName) {
        let res;
        if (reqName == REQ_GET_DEVICE) {
            res = {
                name: "device_header",
                type: "get"
            };
        } else if (reqName == REQ_GET_DEVICES) {
            res = {
                name: "device_headers",
                type: "get"
            };
        } else if (reqName == REQ_GET_MODULE) {
            res = {
                name: "module_header",
                type: "get"
            };
        } else if (reqName == REQ_GET_STATUS) {
            res = {
                name: "system_status",
                type: "get"
            };
        } else if (reqName == REQ_GET_LINKS) {
            res = {
                name: "device_links",
                type: "get"
            };
        } else if (reqName == REQ_SYSTEM_INIT) {
            res = {
                name: "system_init",
                type: "get"
            };
        } else if (reqName == REQ_GET_OSC_STREAM_OPEN) {
            res = {
                name: "osc_data",
                type: "open_stream"
            };
        } else if (reqName == REQ_GET_OSC_STREAM_CLOSE) {
            res = {
                name: "osc_data",
                type: "close_stream"
            }
        } else if (reqName == REQ_GET_OSC) {
            res = {
                name: "osc_header",
                type: "get"
            };
        } else if (reqName == REQ_SET_OSC_SETTINGS) {
            res = {
                name: "osc_header",
                type: "set"
            };
        }

        return res;
    }

    _deviceBody(deviceId, moduleId) {

        let res = {
            device_id: deviceId,
            module_id: moduleId,
        };

        return res;
    }

    _deviceOscBody(deviceId, oscId, oscVars, step, getData) {
        let res = {
            device_id: deviceId,
            osc_id: oscId
        };

        if (oscVars) {
            res.osc_vars = oscVars;
        }
        if (step !== undefined && step !== null) {
            res.step = step;
        }

        if (getData !== undefined && getData !== null) {
            res.getData = getData;
        }

        return res;
    }
}

/*
module.exports = {
    DeviceProvider: DeviceProvider
};
*/