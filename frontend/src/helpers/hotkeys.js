export function enable(app){
	function hasFocus(){
		var now  = webix.UIManager.getFocus();

		if (now && now.getParentView() && now.getParentView().name === "form")
			return false;
		return true;
	}

	let key = webix.env.isMac ? "command" : "ctrl";

	webix.UIManager.addHotKey(key + "-c", function(){
		if (!hasFocus()) return true;
		return app.callEvent("onAction", [ "copy" ]);
	});

	webix.UIManager.addHotKey(key + "-v", function(){
		if (!hasFocus()) return true;
		return app.callEvent("onAction", [ "paste" ]);
	});

	webix.UIManager.addHotKey(key + "-a", function(v,e){
		if (!hasFocus()) return true;
		app.callEvent("onAction", [ "select-all" ]);
		return webix.html.preventEvent(e);
	});

	webix.UIManager.addHotKey(key + "+shift+z", function(){
		if (!hasFocus()) return true;
		return app.callEvent("onAction", [ "redo" ]);
	});

	webix.UIManager.addHotKey(key + "-z", function(){
		if (!hasFocus()) return true;
		return app.callEvent("onAction", [ "undo" ]);
	});

	webix.UIManager.addHotKey("ctrl-y", function(){
		if (!hasFocus()) return true;
		return app.callEvent("onAction", [ "redo" ]);
	});

	webix.UIManager.addHotKey("delete", function(){
		if (!hasFocus()) return true;
		return app.callEvent("onAction", [ "delete" ]);
	});
}

export function disable(){
	let key = webix.env.isMac ? "command" : "ctrl";

	webix.UIManager.removeHotKey(key + "-c");
	webix.UIManager.removeHotKey(key + "-v");
	webix.UIManager.removeHotKey(key + "-a");
	webix.UIManager.removeHotKey(key + "shift-z");
	webix.UIManager.removeHotKey(key + "-z");
	webix.UIManager.removeHotKey("ctrl-y");
	webix.UIManager.removeHotKey("delete");
}