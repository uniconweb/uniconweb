// import {RequestHelper} from "./fr_requesthelper.mjs"
import {default as RequestHelper} from "./fr_requesthelper.mjs";

const REQ_GET_PARAMS = "GET_PARAMS"
const REQ_GET_PARAMS_DATA = "GET_PARAMS_DATA"
const REQ_SET_PARAMS_DATA = "SET_PARAMS_DATA"
const REQ_GET_PARAMS_STREAM_OPEN = "GET_PARAMS_STREAM_OPEN"
const REQ_GET_PARAMS_STREAM_CLOSE = "GET_PARAMS_STREAM_CLOSE"

export class ParamProvider {

    constructor(reqHelper = RequestHelper) {
        this.m_reqHelper = reqHelper;
    }

    requestHelper() {
        return this.m_reqHelper;
    }

    setRequestHelper(reqHelper = RequestHelper) {
        this.m_reqHelper = reqHelper;
    }

    request(cmd, waitResponce = true) {
        return this.requestHelper().request(cmd, waitResponce);
    }

    async reqParams(sysTypeId, deviceId, moduleId) {
        let paramEmptyID = {
            sysTypeId: sysTypeId,
            deviceId: deviceId, 
            moduleId : moduleId, 
            paramId: 0 
        }

        let reqCmd = this._createParamReqCmd(REQ_GET_PARAMS, paramEmptyID);

        return this.request(reqCmd, true);
    };

    async reqParam(paramID) {
        let reqCmd = this._createParamReqCmd(REQ_GET_PARAMS, paramID);

        return this.request(reqCmd);
    };

    async reqParamValue(paramID, stream, args) {
        let cmd = REQ_GET_PARAMS_DATA
        let openStream = null;
        if (stream === "true" || stream === "on") {
            cmd = REQ_GET_PARAMS_STREAM_OPEN;
            openStream = true;
        } else if (stream === "false" || stream === "off") {
            cmd = REQ_GET_PARAMS_STREAM_CLOSE;
            openStream = false;
        }

        const waitResponce = (openStream == null); 
        let reqCmd = this._createParamReqCmd(cmd, paramID, args);
        return this.request(reqCmd, waitResponce);
    };

    async reqParamsValue(sysTypeId, paramIDList, args) {
        if (paramIDList.length == 0) {
            return;
        }

        let cmdName = REQ_GET_PARAMS_STREAM_OPEN;

        let params = [];
        for (var i = 0; i < paramIDList.length; ++i) {
            let paramID = paramIDList[i];
            let param = {
                device_id: paramID.deviceId,
                module_id: paramID.moduleId,
                param_id: paramID.paramId
            };
            params.push(param);
        }

        let body = {
            params: params
        };

        body = {...body, ...args}

        let req = {
            sys_type_id: sysTypeId,
            cmd: this._paramCmd(cmdName),
            body: body
        };

        const waitResponce = false; 
        return this.request(req, waitResponce);
    };

    async setParamValue(paramID, value) {
        let cmdName = REQ_SET_PARAMS_DATA
        let args = {'value' : value};

        let reqCmd = this._createParamReqCmd(cmdName, paramID, args);

        return this.request(reqCmd);
    };

    _createParamReqCmd(cmdName, paramID, args) {
        let req = {
            sys_type_id: paramID.sysTypeId,
            cmd: this._paramCmd(cmdName),
            body: this._paramBody(paramID, args)
        };

        return req;
    }

    _paramCmd(reqName) {
        let res;
        if (reqName == REQ_GET_PARAMS) {
            res = {
                name: "param_header",
                type: "get"
            };
        }

        if (reqName == REQ_GET_PARAMS_DATA) {
            res = {
                name: "param_data",
                type: "get"
            };
        }

        if (reqName == REQ_SET_PARAMS_DATA) {
            res = {
                name: "param_data",
                type: "set"
            };
        }

        if (reqName == REQ_GET_PARAMS_STREAM_OPEN) {
            res = {
                name: "param_data",
                type: "open_stream"
            };
        }

        if (reqName == REQ_GET_PARAMS_STREAM_CLOSE) {
            res = {
                name: "param_data",
                type: "close_stream"
            };
        }

        return res;
    }

    _paramBody(paramID, args) {
        let res = {
            device_id: paramID.deviceId,
            module_id: paramID.moduleId,
            param_id: paramID.paramId
        };

        res = {...res, ...args}

        return res;
    }
}
/*
module.exports = {
    ParamProvider: ParamProvider
};
*/