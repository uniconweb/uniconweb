import "webix/webix.css";
import { $$ } from "@xbs/webix-pro";
import * as webix from "@xbs/webix-pro/webix.js";
import WebixComponent, { scroll } from "./WebixComponent";
import React from "react";
import Chart from "./components/Chart";
import { Model } from "./data_model/fr_model.mjs";
import { Context } from "./Context";

function disableEnableElement(id, show) {
  const elem = document.getElementById(id);
  if (elem != null) {
    if (show) {
      elem.style.display = "block";
    } else {
      elem.style.display = "none";
    }
  }
}

function showElementChart(chartID, visibility = "visible") {
  var sc = document.getElementById(chartID);
  sc.style.setProperty("visibility", visibility);
}

function showChart(chartID, parentID) {
  var sc = document.getElementById(chartID);
  sc.style.setProperty("visibility", "visible");
  const m = document.getElementById(parentID);
  if (sc.parentElement.id != parentID) {
    m.appendChild(sc);
  }
}

const toolBar = () => {
  return {
    view: "toolbar",
    // id: "myToolbar",
    cols: [
      {
        view: "button",
        value: "Add Chart",
        width: 100,
        align: "left",
        click: function (id, event) {},
      },
      {
        view: "button",
        value: "Remove Chart",
        autowidth: true,
        align: "center",
        click: function (id, event) {},
      },

      {
        view: "button",
        value: "Start",
        autowidth: true,
        align: "center",
        click: async function (id, event) {
          console.log("Start osc");
        },
      },
      {
        view: "button",
        value: "Pause",
        autowidth: true,
        align: "center",
        click: async function (id, event) {
          console.log("Stop osc");
        },
      },
    ],
  };
};

const webixButton = (props = { width: "100" }) => {
  return {
    view: "button",
    value: "Button",
    css: "webix_primary",
    inputWidth: props.width,
    click: function (id, event) {
      // console.log("webixButton");
      // console.log(this.onclickMessage);
      this.onclickMessage();
    },
  };
};

// function mark_votes(value, config){
//   if (value > 0 )
//       return { "background":colorsArr[value-1], "color":colorsTitleArr[value-1] };
// };

function addFunction(x) {
  // console.log("addFunction");
  return Math.sin(x * 0.01) * (1 + 0.5 * Math.random());
}

// function addFunction1(x) {
//   // console.log("addFunction");
//   return Math.sin(x * 0.01) * Math.cos(x * 0.01) * (1 + 0.5 * Math.random());
// }

export default class ViewCPlotWeb extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div id="ViewCPlotWeb">
        <WebixComponent ui={toolBar()} />
        {/*<Chart id="chartCPlotWeb" title="&nbsp;" addFunction={addFunction} />*/}
      </div>
    );
  }
}
