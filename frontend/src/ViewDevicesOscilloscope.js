import React, { useEffect } from "react";
import "webix/webix.css";
import ReactDOM from "react-dom";
import WebixComponent from "./WebixComponent";
import webix from "@xbs/webix-pro";
// import {$$, template} from 'webix';
import { $$, template } from "@xbs/webix-pro";
import { Context } from "./Context";
import { SyncCharts, initSciChart } from "./components/ChartMulti";
import ViewDevicesSelectChart from "./ViewDevicesSelectChart";
import OscilloscopeChartsObserver from "./components/oscilloscopecharts";
import {loadHistoryOscData, startNewOsc, restartOscData, closeOsc, stopDrawOsc,} from "./components/oscilloscopecharts";
import { ImBin } from "react-icons/im";
import { observer } from "mobx-react";
import { height } from "@mui/system";


const toolBar = () => {
  return {
    cols: [
      {
        view: "toolbar",
        // id: "myToolbar2",
        margin: 10,
        paddingX: 0,
        height: 45,

        cols: [
          { width: 20 },
          {
            view: "button",
            id: "leftbtn",
            type: "htmlbutton",
            tooltip: "Previous",
            label: `<div class = 'button-icon2'><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-caret-left-fill" viewBox="0 0 16 16">
<path d="m3.86 8.753 5.482 4.796c.646.566 1.658.106 1.658-.753V3.204a1 1 0 0 0-1.659-.753l-5.48 4.796a1 1 0 0 0 0 1.506z"/>
</svg>
          </div>`,
            autowidth: true,
            align: "center",
            click: async function() {
              Context.isHistory = true;
              Context.isHalted = true;
              await closeOsc(Context.currentDevice);
              let step = Context.osc_state.step !== null ? Context.osc_state.step + 1 : 0;
              Context.osc_state.step = step;
              await loadHistoryOscData(Context.states.indexDevice, step);
            }            
          },
          {
            rows: [
              {
                cols: [
                  {
                    view: "template",
                    template: "<div class='templCss'>Time:</div>",
                    tooltip: "Trigger time",
                    borderless: true,
                    autoheight: true,
                    width: 70,
                  },
                  {
                    view: "template",
                    template: "<div class='triggerCss'></div>",
                    id: "timeTemplate",
                    borderless: true,
                    autoheight: true,
                    width: 150,
                  },
                ],
              },
              {
                cols: [
                  {
                    view: "template",
                    template: "<div class='templCss'>Reason:</div>",
                    tooltip: "Trigger reason",
                    borderless: true,
                    autoheight: true,
                    width: 70,
                  },
                  {
                    view: "template",
                    template: "<div class='triggerCss'></div>",
                    id: "reasonTemplate",
                    borderless: true,
                    autoheight: true,
                    width: 150,
                  },
                ],
              },
            ],
          },
          {
            view: "button",
            id: "rightbtn",
            type: "htmlbutton",
            tooltip: "Next",
            label: `<div class = 'button-icon2'><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-caret-right-fill" viewBox="0 0 16 16">
  <path d="m12.14 8.753-5.482 4.796c-.646.566-1.658.106-1.658-.753V3.204a1 1 0 0 1 1.659-.753l5.48 4.796a1 1 0 0 1 0 1.506z"/>
</svg>
          </div>`,
            autowidth: true,
            align: "center",
            click: async function() {
              Context.isHistory = true;
              Context.isHalted = true;
              
              await closeOsc(Context.currentDevice);
              let step = Context.osc_state.step > 0 ? Context.osc_state.step - 1 : 0;
              Context.osc_state.step = step;
              await loadHistoryOscData(Context.states.indexDevice, step);
            }
          },
          {width: 20},
        ],
          },
      {
        view: "toolbar",
        height: 45,

        // id: "myToolbar",
        cols: [
          { width: 30 },
          {
            view: "toggle",
            id: "cursor",
            tooltip: "Cursor",
            label: `<div class = 'button-icon1'><svg stroke="currentColor" fill="currentColor" stroke-width="0" viewBox="0 0 24 24" height="1em" width="1em" xmlns="http://www.w3.org/2000/svg">
           <path d="M11 2h2v7h-2zm0 13h2v7h-2zm4-4h7v2h-7zM2 11h7v2H2z"></path>
           </svg>
           </div>`,
            autowidth: true,
            align: "left",
            click: async function (id, event) {
              await SwitchCursor($$(id).getValue());
              $$(id).blur();
            },
          },
          {
            view: "toggle",
            id: "point_marker",
            tooltip: "Point marker",
            label:`<div class = 'button-icon1'><svg height="48" viewBox="0 0 48 48" width="48" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><defs><path d="M0 0h48v48H0V0z" id="a"/></defs><clipPath id="b"><use overflow="visible" xlink:href="#a"/></clipPath><path clip-path="url(#b)" d="M46 16c0 2.2-1.8 4-4 4-.36 0-.7-.04-1.02-.14l-7.12 7.1c.1.32.14.68.14 1.04 0 2.2-1.8 4-4 4s-4-1.8-4-4c0-.36.04-.72.14-1.04l-5.1-5.1c-.32.1-.68.14-1.04.14s-.72-.04-1.04-.14l-9.1 9.12c.1.32.14.66.14 1.02 0 2.2-1.8 4-4 4s-4-1.8-4-4 1.8-4 4-4c.36 0 .7.04 1.02.14l9.12-9.1c-.1-.32-.14-.68-.14-1.04 0-2.2 1.8-4 4-4s4 1.8 4 4c0 .36-.04.72-.14 1.04l5.1 5.1c.32-.1.68-.14 1.04-.14s.72.04 1.04.14l7.1-7.12c-.1-.32-.14-.66-.14-1.02 0-2.2 1.8-4 4-4s4 1.8 4 4z"/></svg>
            </div`,
            width: 40,
            align: "center",

            on: {
              onChange: function (newState) {
                switchPointMarkerClick(newState);
              },
            },
          },
          {
            view: "button",
            id: "halt_go",
            type: "htmlbutton",
            tooltip: Context.isHalted ? "Go" : "Halt",
            label: getButtonLabel(Context.isHalted),
            autowidth: true,
            align: "center",
            click: async function () {
                if (Context.isHalted) {
                  Context.isHalted = false; 
                  if (Context.isHistory === true) {
                    await closeOsc(Context.currentDevice);
                    Context.isHistory = false;
                    Context.osc_state.step = null;
                    await startNewOsc(Context.states.indexDevice);                    
                  } else {
                    stopDrawOsc(Context.currentDevice);
                    await restartOscData(Context.states.indexDevice);
                  }
                } else {
                  Context.isHalted = true;                     
                  stopDrawOsc(Context.currentDevice);
//                await closeOsc(Context.currentDevice);

                }              
                $$("halt_go").define("label", getButtonLabel(Context.isHalted));
                $$("halt_go").define("tooltip", Context.isHalted ? "Go" : "Halt");
                $$("halt_go").refresh();
            },
        },
          {
            view: "button",
            id: "remove_chart",
            tooltip: "Remove ALL charts",
            label: `<div class = 'button-icon3'><svg stroke="currentColor" fill="currentColor" stroke-width="0" version="1.1" viewBox="0 0 16 16" height="1em" width="1em" xmlns="http://www.w3.org/2000/svg"><path d="M2 5v10c0 0.55 0.45 1 1 1h9c0.55 0 1-0.45 1-1v-10h-11zM5 14h-1v-7h1v7zM7 14h-1v-7h1v7zM9 14h-1v-7h1v7zM11 14h-1v-7h1v7z"></path><path d="M13.25 2h-3.25v-1.25c0-0.412-0.338-0.75-0.75-0.75h-3.5c-0.412 0-0.75 0.338-0.75 0.75v1.25h-3.25c-0.413 0-0.75 0.337-0.75 0.75v1.25h13v-1.25c0-0.413-0.338-0.75-0.75-0.75zM9 2h-3v-0.987h3v0.987z"></path>
            </svg>
            </div`,
            width: 40,
            align: "center",

            click: async function (id, event) {
              removeButtonClick();
            },
          },
          {width: 20},
          {
            view: "button",
            value: "Add Chart",
            width: 100,
            align: "left",
            click: function (id, event) {
              addButtonClick();
            },
          },
          {
            view: "button",
            value: "Add Discretes",
            width: 120,
            align: "left",
            click: function (id, event) {
              addDiscretesButtonClick();
            },
          },
          {width: 30},
        ],
      },
      {
        view: "toolbar",
        // id: "myToolbar2",
        margin: 10,
        paddingX: 0,
        height: 45,

        cols: [
          { width: 15 },
          {
            view: "button",
            id: "restore_scale",
            tooltip: "restore scale",
            label: `<div class = 'button-icon1'><svg stroke="currentColor" fill="currentColor" stroke-width="0" viewBox="0 0 512 512" height="1em" width="1em" xmlns="http://www.w3.org/2000/svg">
            <path d="M64 64c0-17.7-14.3-32-32-32S0 46.3 0 64V400c0 44.2 35.8 80 80 80H480c17.7 0 32-14.3 32-32s-14.3-32-32-32H80c-8.8 0-16-7.2-16-16V64zm406.6 86.6c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0L320 210.7l-57.4-57.4c-12.5-12.5-32.8-12.5-45.3 0l-112 112c-12.5 12.5-12.5 32.8 0 45.3s32.8 12.5 45.3 0L240 221.3l57.4 57.4c12.5 12.5 32.8 12.5 45.3 0l128-128z"></path>
            </svg>
          </div>`,
            autowidth: true,
            align: "left",
            click: async function (id, event) {
              await SwitchOverview(true); 
              $$(id).blur();
            },
          },
          {
            view: "button",
            id: "lock_scale",
            tooltip: "lock scale",
            label: `<div class = 'button-icon1'><svg data-name="Layer 2" id="e735b1c9-e295-4064-925f-cbd97a5d1482" viewBox="0 0 38 38" xmlns="http://www.w3.org/2000/svg"><polygon points="37.496 19 31.812 13.316 31.812 24.684 37.496 19"/><polygon points="0.504 19 6.188 13.316 6.188 24.684 0.504 19"/><rect height="2.5" width="27.96" x="4.92" y="17.75"/><polygon points="19 0.504 13.316 6.188 24.684 6.188 19 0.504"/><polygon points="19 37.496 13.316 31.812 24.684 31.812 19 37.496"/><rect height="2.5" transform="translate(0 38) rotate(-90)" width="28.081" x="4.96" y="17.75"/></svg>
           </div>`,
            autowidth: true,
            align: "left",
            click: function() {
              saveZoomStateClick();
            }
          },
          { 
            rows: [
              {
                cols: [
                  {
                    view: "template",
                    tooltip: "Time scale",
                    template: "<div class='templCss'>ScaleX:</div>",
                    width: 58,
                    borderless: true,
                    autoheight: true,
                    css: "custom-scale-template",
                  },
                  {
                    view: "button",
                    value: "+",
                    tooltip: "Time scale",
                    width: 40,
                    align: "left",
                    borderless: true,
                    autoheight: true,
                    css: "custom-margin-right",
                    click: async function (id, event) {
                      await ScaleXPlus();
                    },
                  },
                  {
                    view: "template",
                    id: "timeX_Label",
                    tooltip: "Time scale",
                    template: "<div class='labelCss'></div>",
                    width: 100,
                    borderless: true,
                    css: "custom-scale-template",
                  },
                  {
                    view: "button",
                    value: "-",
                    tooltip: "Time scale",
                    width: 40,
                    align: "left",
                    borderless: true,
                    autoheight: true,
                    css: "custom-margin-left",
                    click: async function (id, event) {
                      await ScaleXMinus();
                    },
                  },
                ],
              },
              {
                height: 2, 
                css: "custom-row-spacing",
              },
              {
                cols: [
                  {
                    view: "template",
                    tooltip: "Y scale",
                    template: "<div class='templCss'>ScaleY:</div>",
                    width: 58,
                    borderless: true,
                    autoheight: true,
                    css: "custom-scale-template",
                  },
                  {
                    view: "button",
                    value: "+",
                    tooltip: "Y scale",
                    width: 40,
                    align: "left",
                    borderless: true,
                    autoheight: true,
                    css: "custom-margin-right custom-scale-button",
                    click: async function (id, event) {
                      await ScaleYPlus();
                    },
                  },
                  {
                    view: "template",
                    id: "scaleY_Label",
                    tooltip: "Y scale",
                    template: "<div class='labelCss'></div>",
                    width: 100,
                    borderless: true,
                    css: "custom-scale-template",
                  },
                  {
                    view: "button",
                    value: "-",
                    tooltip: "Y scale",
                    width: 40,
                    align: "left",
                    borderless: true,
                    autoheight: true,
                    css: "custom-margin-left custom-scale-button",
                    click: async function (id, event) {
                      await ScaleYMinus();
                    },
                  },
                ],
              },
            ],
          },
          { width: 25 },
        ],
      },
      
    ],
  };
};


const saveZoomStateClick = () => {
  let charts = Context.currDeviceCharts();
  let actions = Context.chartActions;
  charts.forEach((chartID) => {
    let state = actions[chartID]?.saveCurrentZoomState();
    if (state) {
    } else {
      console.error(`Failed to save state for chart ${chartID}: action not found or invalid state`);
    }
  });
};

function getButtonLabel(isHalted) {
  if (!isHalted) {
      return  `<div class = 'button-icon1'><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-stop-circle" viewBox="0 0 16 16">
      <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14m0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16"/>
      <path d="M5 6.5A1.5 1.5 0 0 1 6.5 5h3A1.5 1.5 0 0 1 11 6.5v3A1.5 1.5 0 0 1 9.5 11h-3A1.5 1.5 0 0 1 5 9.5z"/>
    </svg></div>`;
  } else {
      return `<div class='button-icon1'><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-play-circle" viewBox="0 0 16 16">
  <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14m0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16"/>
  <path d="M6.271 5.055a.5.5 0 0 1 .52.038l3.5 2.5a.5.5 0 0 1 0 .814l-3.5 2.5A.5.5 0 0 1 6 10.5v-5a.5.5 0 0 1 .271-.445"/>
</svg></div>`;
  }
}

const addButtonClick = async () => {
  if (Context.currDeviceCharts().length >= 3) {
    return;
  }
  let device = Context.currentDevice;
  if (!device) {
    return;
  }
  let osc = device.getOsc();
  if (!osc || (osc && !osc.isReceiving())) {
    osc = await device.requestNewOsc();
  }
  let comp = $$("showSelectChartWindowData");
  let dataForChoose = [];
  osc.analogChannels.forEach(function (item, index, array) {
    let name1 = item.name + " [" + device.deviceId + "." + item.var_id + "]";
    if (item.name == "") name1 = "—";
    dataForChoose.push({
      id: item.var_id,
      channel: item.ch_num,
      name: name1,
      color: item.color,
      status: 0,
      var_id: item.var_id,
      userName: "",
      isDiscrete: false,
    });
  });
  comp.define({ data: dataForChoose, apply: applyAddChart });
  $$("minY").setValue("");
  $$("maxY").setValue("");

  $$("minY").enable();
  $$("maxY").enable();

  $$("showSelectChartWindow").show();
};

const addDiscretesButtonClick = async () => {
  if (Context.currDeviceCharts().length >= 3) {
    return;
  }
  let device = Context.currentDevice;
  if (!device) {
    return;
  }
  let osc = device.getOsc();
  if (!osc || (osc && !osc.isReceiving())) {
    osc = await device.requestNewOsc();
  }
  let comp = $$("showSelectChartWindowData");
  let dataForChoose = [];
  osc.discreteChannels.forEach(function (item, index, array) {
    let name1 = item.name + " [" + device.deviceId + "." + item.var_id + "]";
    if (item.name == "") name1 = "—";
    dataForChoose.push({
      id: index,
      channel: item.ch_num,
      name: name1,
      color: item.color,
      status: 0,
      var_id: item.var_id,
      userName: "",
      isDiscrete: true,
    });
  });

  comp.define({ data: dataForChoose, apply: applyAddChart });
  $$("minY").setValue("");
  $$("maxY").setValue("");

  $$("minY").disable();
  $$("maxY").disable();

  $$("showSelectChartWindow").show();
};

const removeButtonClick = () => {
  let indexDevice = Context.states.indexDevice;
  Context.clearCharts(indexDevice);
  closeOsc(Context.currentDevice);
};

let state = false;
const switchPointMarkerClick = (newState) => {
  newState = !state;
  state = newState;
  let charts = Context.currDeviceCharts();
  let actions = Context.chartActions;
  charts.forEach(function (chart, index, array) {
    actions[chart].switchPointMarker(state);
  });
};

async function ScaleXPlus() {
  let device = Context.currentDevice;
  let osc = device?.getOsc();
  let currentResolution = Context.osc_state?.display_resolution_ms;
  let charts = Context.currDeviceCharts();
  let actions = Context.chartActions;
  if (!device || !osc || !currentResolution || !charts || charts.length === 0 || !actions) {
    console.warn('Necessary context or parameters are missing');
    return;
  }
  let newVisiblePointsX = currentResolution * 2;
  osc.setDisplayResolution(newVisiblePointsX);
  charts.forEach(function (chart) {
    if (actions[chart]?.Scale) {
      actions[chart].Scale(newVisiblePointsX);
    }
  });
  updateScaleXLabel(newVisiblePointsX);
  Context.osc_state.display_resolution_ms = newVisiblePointsX;
  saveZoomStateClick();
}

async function Adjust() {
  let charts = Context.currDeviceCharts();
  let actions = Context.chartActions;
  charts.forEach(function (chart, index, array) {
    actions[chart].Adjust();
  });
}

async function ScaleXMinus() {
  let device = Context.currentDevice;
  let osc = device?.getOsc();
  let currentResolution = Context.osc_state?.display_resolution_ms;
  let charts = Context.currDeviceCharts();
  let actions = Context.chartActions;
  if (!device || !osc || !currentResolution || !charts || charts.length === 0 || !actions) {
    console.warn('Necessary context or parameters are missing');
    return;
  }
  let newVisiblePointsX = currentResolution / 2;
  osc.setDisplayResolution(newVisiblePointsX);
  charts.forEach(function (chart) {
    if (actions[chart]?.Scale) {
      actions[chart].Scale(newVisiblePointsX);
    }
  });

  updateScaleXLabel(newVisiblePointsX);
  Context.osc_state.display_resolution_ms = newVisiblePointsX;
  saveZoomStateClick();
}


async function ScaleYPlus() {
  let minY = Context.osc_state?.yAxisRange?.minY;
  let maxY = Context.osc_state?.yAxisRange?.maxY;
  let charts = Context.currDeviceCharts();
  let actions = Context.chartActions;
  if (minY == null || maxY == null || !charts || charts.length === 0 || !actions) {
    console.warn('Necessary context or parameters are missing');
    return;
  }
  minY *= 2;
  maxY *= 2;
  Context.osc_state.yAxisRange = { minY: minY, maxY: maxY };
  updateScaleYLabel(maxY - minY);
  charts.forEach(function (chart) {
    if (actions[chart]?.updateYAxis) {
      actions[chart].updateYAxis(minY, maxY);
    }
  });
  saveZoomStateClick();
}

async function ScaleYMinus() {
  let minY = Context.osc_state?.yAxisRange?.minY;
  let maxY = Context.osc_state?.yAxisRange?.maxY;
  let charts = Context.currDeviceCharts();
  let actions = Context.chartActions;
  if (minY == null || maxY == null || !charts || charts.length === 0 || !actions) {
    console.warn('Necessary context or parameters are missing');
    return;
  }
  minY /= 2;
  maxY /= 2;
  Context.osc_state.yAxisRange = { minY: minY, maxY: maxY };
  updateScaleYLabel(maxY - minY);
  charts.forEach(function (chart) {
    if (actions[chart]?.updateYAxis) {
      actions[chart].updateYAxis(minY, maxY);
    }
  });
  saveZoomStateClick();
}

async function applyAddChart() {
  updateScaleXLabel(Context.osc_state.display_resolution_ms);
  updateScaleYLabel(Context.osc_state.yAxisRange.maxY - Context.osc_state.yAxisRange.minY);

  if (Context.isHistory) {
    let step = Context.osc_state.step;
    let deviceInd = Context.states.indexDevice;    
  
    setTimeout(await loadHistoryOscData(deviceInd, step), 1000);
  }
}

function updateScaleXLabel(newVisiblePointsX) {

  let roundedVisiblePointsX = Math.round(newVisiblePointsX);
  let templateComponent = $$("timeX_Label");
  if (templateComponent) {
    templateComponent.setHTML(
      "<div class='labelCss'>" + roundedVisiblePointsX.toString() + "</div>"
    );
    templateComponent.refresh();
  } else {
    console.error("Template component with id 'timeX_Label' was not found.");
  }
}

function updateScaleYLabel(newVisiblePointsY) {
  let roundedVisiblePointsY = Math.round(newVisiblePointsY);
  let templateComponent = $$("scaleY_Label");
  if (templateComponent) {
    templateComponent.setHTML(
      "<div class='labelCss'>" + roundedVisiblePointsY.toString() + "</div>"
    );
    templateComponent.refresh();
  } else {
    console.error("Template component with id 'scaleY_Label' was not found.");
  }
}

async function SwitchCursor(value) {
  let charts = Context.currDeviceCharts();
  let actions = Context.chartActions;

  charts.forEach(function (chart, index, array) {
    actions[chart].SwitchCursor(!value);
  });
}

const SwitchOverview = async (restorePrevious) => {
  let charts = Context.currDeviceCharts();
  let actions = Context.chartActions;
  charts.forEach((chartID) => {
    let state = Context.getZoomState(chartID);
    if (restorePrevious && state) {
      let yVisibleRange = state.yVisibleRange[0].max - state.yVisibleRange[0].min;
      let xVisibleRange = state.xMax - state.xMin;

      actions[chartID].Scale(xVisibleRange, yVisibleRange);
    } else {
      actions[chartID].zoomExtents();
    }
  });
};

function UpdateToolBarState(osc_state) {
  let time = osc_state.trig_time;
  let reason = osc_state.trig_reason;

  let timeElement = $$("timeTemplate");
  let reasonElement = $$("reasonTemplate");

  if (reason == 0) {
    reason = "";
  }

  let formattedTime = "";
  if (time) {
    const date = new Date(time);
    formattedTime = `${date.toLocaleDateString()} ${date.toLocaleTimeString()}`;
  }

  if (timeElement) {
    timeElement.setHTML(`<div class='triggerCss'>${formattedTime}</div>`);
  }

  if (reasonElement) {
    reasonElement.setHTML(`<div class='triggerCss'>${reason}</div>`);
  }
}


//Временная генерация данных
function addFunction(x) {
  // console.log("addFunction");
  return Math.sin(x * 0.01) * (1 + 0.5 * Math.random());
}

const ViewDevicesOscilloscope = observer((props) => {
  
  useEffect(() => {
    UpdateToolBarState(Context.osc_state);
    updateScaleXLabel(Context.osc_state.display_resolution_ms);
    updateScaleYLabel(Context.osc_state.yAxisRange.maxY - Context.osc_state.yAxisRange.minY);
  
  }, [
    Context.osc_state.trig_time,
    Context.osc_state.trig_reason,
    Context.osc_state.display_resolution_ms,
    Context.osc_state.yAxisRange
  ]);

  useEffect(() => {
    $$("halt_go").define("label", getButtonLabel(Context.isHalted));
    $$("halt_go").define("tooltip", Context.isHalted ? "Go" : "Halt");
    $$("halt_go").refresh();
  
  }, [ Context.isHalted, Context.isHistory ]);

  return (
    <div id={props.id} className="pages">
      <WebixComponent ui={toolBar()} />
      <ViewDevicesSelectChart />
      <OscilloscopeChartsObserver />
    </div>
  );
});

export default ViewDevicesOscilloscope;
