import React, { createContext, useState, useContext, useEffect } from "react";
import { $$ } from "@xbs/webix-pro";
import * as webix from "@xbs/webix-pro/webix.js";
import { Context } from "./Context";
import moment from "moment";
import { Param } from "./data_model/param.mjs";
import { PLC_ConfigHelper } from "./data_model/plc_config.js";

export const PLCContext = createContext();
var _configHelper = new PLC_ConfigHelper();
const CONFIG_KEY = "PLC_VARS";

export async function loadConfig(configKey, setSelectedParameters) {
  try {
    await _configHelper.load(configKey);
    let confObj = _configHelper.get(configKey);
    if (!confObj) {
      throw new Error("Configuration not found");
    }
    if (!Array.isArray(confObj.parameters)) {
      console.error("Parameters is not an array:", confObj.parameters);
      return;
    }
    const selectedParams = {};
    confObj.parameters.forEach((paramID) => {
      const uniqueId = `${paramID.sysTypeId}-${paramID.deviceId}-${paramID.moduleId}-${paramID.paramId}`;
      selectedParams[uniqueId] = paramID;
    });
    setSelectedParameters(selectedParams);
    console.log("Configuration loaded successfully");
  } catch (error) {
    console.log("Failed to load configuration: " + error.message);
  }
}

async function saveConfig(configKey, selectedParameters) {
  try {
    const confObj = {
      parameters: Object.values(selectedParameters),
    };
    _configHelper.set(configKey, confObj);
    await _configHelper.save(configKey);
    console.log("The configuration is saved");
  } catch (error) {
    console.log("Failed to save configuration: " + error.message);
  }
}

export const PLCProvider = ({ children }) => {
  const [selectedParameters, setSelectedParameters] = useState({});
  const [shouldSaveConfig, setShouldSaveConfig] = useState(false);

  useEffect(() => {
    if (shouldSaveConfig && Object.keys(selectedParameters).length >= 0) {
      saveConfig(CONFIG_KEY, selectedParameters);
      setShouldSaveConfig(false);
    }
  }, [shouldSaveConfig, selectedParameters]);

  const addParameter = (param) => {
    if (param.isModule) {
      return;
    }
    if (!param.ID || !param.ID.deviceId) {
      console.error("Invalid parameter structure:", param);
      return;
    }
    const uniqueIdParam = param.uniqueId();
    if (selectedParameters[uniqueIdParam]) {
      console.log("Parameter in Variables with the same ID already exists:", uniqueIdParam);
      return;
    }
    const paramID = {
      sysTypeId: param.ID.sysTypeId,
      deviceId: param.ID.deviceId,
      moduleId: param.ID.moduleId,
      paramId: param.ID.paramId,
    };
    setSelectedParameters((prev) => ({ ...prev, [uniqueIdParam]: paramID }));
    setShouldSaveConfig(true);
  };

  const removeParameter = (param) => {
    const uniqueId = param.uniqueId();
    console.log("Removing parameter with id:", uniqueId);
    setSelectedParameters((prev) => {
      const { [uniqueId]: removed, ...rest } = prev;
      return rest;
    });
    setShouldSaveConfig(true);
  };

  return (
    <PLCContext.Provider
      value={{
        selectedParameters,
        setSelectedParameters,
        addParameter,
        removeParameter,
        shouldSaveConfig,
        setShouldSaveConfig,
      }}
    >
      {children}
    </PLCContext.Provider>
  );
};

export const usePLC = () => useContext(PLCContext);
