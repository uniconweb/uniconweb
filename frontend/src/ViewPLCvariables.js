import "webix/webix.css";
import ReactDOM from "react-dom";
import React, { useEffect, useState, useContext } from "react";
import WebixComponent from "./WebixComponent";
import { $$ } from "@xbs/webix-pro";
import { Context } from "./Context";
import moment from "moment";
import { ValueFormatEnum } from "./data_model/param.mjs";
import { observer } from "mobx-react";
import * as webix from "@xbs/webix-pro/webix.js";
import { usePLC } from "./PLCContext";
import { selectParameter } from "./device_control/DeviceParamSelect";
import { loadConfig } from "./PLCContext";

const CONFIG_KEY = "PLC_VARS";

let actions = {};

function importPLCFunctions(plcFunctions) {
  actions = plcFunctions;
}

function frequencyByMode(editMode, turboMode) {
  return editMode ? (turboMode ? 2 : 1) : turboMode ? 8 : 3;
}
function changeFrequencyByMode(editMode, turboMode) {
  openParamStreams();
}

let _editMode = false;
let _turboMode = false;

function getAllItems(grid) {
  return grid.serialize();
}

async function closeAllStreams() {
  if (Context?.currentDevice === undefined) {
    return; // not inited yet
  }
  await Context.model.forceCloseAllStreams(
    Context.currentDevice?.deviceId ?? 0
  );
}

function openParamStreams(frequency) {
  if (!frequency) {
    frequency = frequencyByMode(_editMode, _turboMode);
  }

  let grid = $$("variablesGrid");
  let items = getAllItems(grid);
  for (let i = 0; i < items.length; i++) {
    let row = items[i];
    openParamStream(row.param, row.id, frequency);
  }

  Context.model.startParamStreams(frequency);
}

async function openParamStream(
  param,
  rowId,
  frequency = frequencyByMode(_editMode, _turboMode)
) {
  //console.log("Opening param stream for param:", param, "with frequency:", frequency);
  const stream = Context.model.openParamStream(param, frequency);
  const reader = stream?.getReader();

  const updateValues = async () => {
    const { done, value: values } = await reader?.read();

    if (done) {
      return;
    }
    if (values && values.length > 0) {
      let value = values[values.length - 1];
      updateRowValue(value, param, rowId);
      setTimeout(await updateValues());
    }
  };

  setTimeout(await updateValues());
}

function updateRowValue(value, param, rowId) {
  var timeStr = moment(value.valueTime).format("hh:mm:ss.SSS");
  let grid = $$("variablesGrid");

  grid.updateItem(rowId, {
    value: param.displayValue(value.value, true),
    time: timeStr,
  });
}

const manageStreams = async (currentTabId) => {
  const grid = $$("variablesGrid");
  if (grid && currentTabId === "PLCParametersTab" && grid.count()) {
    setTimeout(() => {
      openParamStreams();
    }, 0);
  } else {
    await closeAllStreams();
  }
};

function mark_items_editable(value, config) {
  let res = "no_editable";
  if (config.rw == "W") res = "editable";
  return res;
}

function mark_items_readable(value, config) {
  let res = "no_editable";
  return res;
}

function getViewVariablesDataTable() {
  return {
    view: "datatable",
    drag: "order",
    id: "variablesGrid",
    type: {
      height: "auto",
    },
    rowHeight: 25,
    rowLineHeight: 25,
    select: true,
    columns: [
      { id: "deviceName", header: "Device Name", width: 150 },
      { id: "module", header: "Module", width: 170 },
      {
        id: "name",
        header: [{ content: "textFilter", placeholder: " Name" }],
        width: 220,
        sort: "string",
      },
      { id: "value", header: "Value", width: 120, editor: "text" },
      { id: "dimension", header: "Dimension", width: 90 },
      { id: "time", header: "Time", width: 100 },
      { id: "desc", header: "Description", fillspace: true },
    ],
    select: "row",
    navigation: true,
    editable: true,
    editaction: "custom",
    data: [],
    on: {
      onItemClick: function (id) {
        let item = $$("variablesGrid").getItem(id);
        let cellParam = item.param;

        if (cellParam != undefined) {
          if (!cellParam.isReadOnly() & _editMode) {
            this.editCell(id, "value");
          }
        }
        if (id.column == "chart") {
          this.getItem(id.row).chart = !this.getItem(id).chart;
          this.refresh(id.row);
        }
      },
      onBeforeEditStart: function (id) {
        let item = $$("variablesGrid").getItem(id.row);
        let cellParam = item.param;

        if (id.column === "value" && cellParam.rw == "W") {
          if (cellParam.valueFormat == ValueFormatEnum.Text) {
            let column = this.getColumnConfig(id.column);
            column.collection = [];
            let val;
            for (let key in cellParam.valueTexts) {
              val = cellParam.valueTexts[key];
              column.collection.push({ id: val, value: val });
            }
            column.editor = "richselect";
          } else {
            this.getColumnConfig(id.column).editor = "text"; // "inline-text";
          }
        }
      },
      onAfterEditStop: function (state, editor, ignoreUpdate) {
        if (state.value === state.old) {
          return;
        }

        let item = $$("variablesGrid").getItem(editor.row);
        let cellParam = item.param;

        let value;
        switch (cellParam.valueFormat) {
          case ValueFormatEnum.Text:
            {
              for (let key in cellParam.valueTexts) {
                if (cellParam.valueTexts[key] === state.value) {
                  value = Number(key).toString();
                  break;
                }
              }
            }
            break;
          default:
            value = state.value.toString();
        }
        console.log("cellParam", cellParam);
        cellParam.asyncSetValue(value);
        if (cellParam.lastError < 0) {
          //todo cancel editing value
          let value = cellParam.lastValue();
          updateRowValue(value, cellParam, editor.row);
        }
      },
      onAfterFilter: function (id) {
        openParamStreams();
      },
    },
  };
}

const toolBar = () => {
  return {
    view: "toolbar",
    id: "varToolbar",
    // css:"webix_dark",
    cols: [
      {
        view: "toggle",
        name: "buttonEdit",
        label: "Edit",
        width: 100,
        align: "left",
        // css:"webix_primary" ,
        click: function (id, event) {
          _editMode = !_editMode;

          let tree = $$("variablesGrid");
          let column = tree.getColumnConfig("value");

          column.cssFormat = _editMode
            ? mark_items_editable
            : mark_items_readable;
          tree.refresh();
          changeFrequencyByMode(_editMode, _turboMode);
        },
      },
      {
        view: "toggle",
        name: "buttonTurbo",
        label: "Turbo",
        width: 100,
        align: "center",
        click: function (id, event) {
          _turboMode = !_turboMode;
          changeFrequencyByMode(_editMode, _turboMode);
        },
      },
      {
        view: "button",
        value: "Add Parameter",
        id: "add_paramVar",
        width: 200,
        align: "left",
        click: addParameterClick,
      },
      {
        view: "button",
        id: "remove_paramVar",
        tooltip: "Remove parameter",
        label: `<div class = 'button-icon3'><svg stroke="currentColor" fill="currentColor" stroke-width="0" version="1.1" viewBox="0 0 16 16" height="1em" width="1em" xmlns="http://www.w3.org/2000/svg"><path d="M2 5v10c0 0.55 0.45 1 1 1h9c0.55 0 1-0.45 1-1v-10h-11zM5 14h-1v-7h1v7zM7 14h-1v-7h1v7zM9 14h-1v-7h1v7zM11 14h-1v-7h1v7z"></path><path d="M13.25 2h-3.25v-1.25c0-0.412-0.338-0.75-0.75-0.75h-3.5c-0.412 0-0.75 0.338-0.75 0.75v1.25h-3.25c-0.413 0-0.75 0.337-0.75 0.75v1.25h13v-1.25c0-0.413-0.338-0.75-0.75-0.75zM9 2h-3v-0.987h3v0.987z"></path>
          </svg>
          </div`,
        width: 40,
        align: "center",
        click: removeParamClick,
      },
      {},
    ],
  };
};

function addParameterToState(param) {
  const uniqueIdParam = param.uniqueId();
  if (actions.selectedParameters[uniqueIdParam]) {
    console.log(
      "Parameter in Variables with the same ID already exists:",
      uniqueIdParam
    );
    return;
  }
  const paramID = {
    sysTypeId: param.ID.sysTypeId,
    deviceId: param.ID.deviceId,
    moduleId: param.ID.moduleId,
    paramId: param.ID.paramId,
  };

  actions.setSelectedParameters((prev) => ({
    ...prev,
    [uniqueIdParam]: paramID,
  }));
  actions.setShouldSaveConfig(true);
}

function addParameterClick() {
  selectParameter("variablesGrid", function (selectedParam) {
    const paramAdded = addParameterToState(selectedParam);
    console.log("Parameter added to state:", selectedParam);
    if (paramAdded) {
      openParamStreams(); 
    }
  });
}


function removeParameterFromState(param) {
  const uniqueId = param.uniqueId();
  actions.setSelectedParameters((prev) => {
    const { [uniqueId]: removed, ...rest } = prev;
    return rest;
  });
  actions.setShouldSaveConfig(true);
}

async function removeParamClick() {
  const grid = $$("variablesGrid");
  if (grid) {
    const selectedId = grid.getSelectedId();
    if (selectedId) {
      const item = grid.getItem(selectedId);
      await Context.model.closeParamStream(item.param);
      removeParameterFromState(item.param);
      console.log("Removing parameter :", item.param);
    } else {
      console.error("No parameter selected");
    }
  } else {
    console.error("Grid not found");
  }
}

function createGridRow(param) {
  let selectedDevice = Context.model.device(param.ID.deviceId);
  if (!param || !param.ID || !param.ID.deviceId) {
    console.error("Invalid parameter structure:", param);
    return null;
  }
  const module = selectedDevice
    ? selectedDevice.module(param.ID.moduleId)
    : { name: "Unknown Module" };
  return {
    ID: param.ID,
    id: param.uniqueId(),
    uniqueId: param.uniqueId(),
    name: param.displayName(),
    value: " ",
    dimension: param.valueUnit,
    rw: param.rw,
    time: " ",
    module: module ? module.displayName() : "Unknown Module",
    deviceName: selectedDevice
      ? selectedDevice.displayName()
      : "Unknown Device",
    deviceId: param.ID.deviceId,
    desc: param.desc || "No description provided",
    param: param,
  };
}

function createGridData(parameters) {
  if (!Array.isArray(parameters)) {
    console.error("parameters is not an array:", parameters);
    return [];
  }
  const gridData = [];

  parameters.forEach((paramID) => {
    if (!paramID || !paramID.deviceId) {
      console.error("Invalid parameter ID (missing deviceId):", paramID.id);
      return;
    }

    const device = Context.model.device(paramID.deviceId);
    if (!device) {
      console.error("Device not found for parameter ID:", paramID);
      return;
    }
    const paramObj = device.param(paramID.moduleId, paramID.paramId);
    if (!paramObj) {
      console.error("Parameter not found for ID:", paramID.paramId);
      return;
    }

    const row = createGridRow(paramObj);
    if (row) {
      gridData.push(row);
    } else {
      console.error("Parameter not found for ID:", paramID.id);
    }
  });

  return gridData;
}

function plcDesignTemplate() {
  return {
    rows: [toolBar(), getViewVariablesDataTable()],
  };
}

const PLCViewObserver = observer(({}) => {
  const [isConfigLoadedForTab, setIsConfigLoadedForTab] = useState(false);
  const {selectedParameters, setSelectedParameters} = usePLC();
  const currTabViewId = Context.states.currTabViewId;
  const plcFunctions = usePLC();

  useEffect(() => {
    importPLCFunctions(plcFunctions);
    const checkGrid = async () => {
      const grid = $$("variablesGrid");
      if (!grid) {
        console.error("Cannot find the variablesGrid");
        setTimeout(checkGrid, 2000);
      } else {
        const paramsArray = Object.keys(selectedParameters).map(
          (key) => selectedParameters[key]
        );
        const gridData =createGridData(paramsArray);
        grid.clearAll();
        grid.parse(gridData);
      }
    };
    checkGrid();   
  }, [selectedParameters]);

  useEffect(() => {
      manageStreams(currTabViewId);
    if (currTabViewId === "PLCParametersTab" && !isConfigLoadedForTab) {
      loadConfig(CONFIG_KEY, setSelectedParameters).then(() =>
        setIsConfigLoadedForTab(true)
      );
    }
  }, [currTabViewId, selectedParameters]);

  return <WebixComponent ui={plcDesignTemplate()} />;
});

function ViewPLCvariables(props) {
  return (
    <div id={props.id} className="pages">
      <PLCViewObserver />
    </div>
  );
}

export default ViewPLCvariables;
