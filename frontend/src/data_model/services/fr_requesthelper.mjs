import EventEmitter from 'events'
import Config from '../../config.js';
import cbor from 'cbor-web'

let DATA_SERVER_PORT = 1235;
let CONNECTION_TIMEOUT_MSC = 5000;
let DEFAULT_SRV_HOST = "localhost";

export class _RequestHelper {

    m_connected = false;
    m_socketUrl = "ws://";
    m_events = new EventEmitter();

    constructor() {
        this.m_cachedServerIP = null;
    }

    async initConnection(srvHost) {

        if (this.m_connected) {
            console.log("Already connected.");
            return;
        }
       
        const socketIP = await this.resolveSocketIP(srvHost);
        if (!socketIP) {
            setTimeout(async () => {
                await this.initConnection(srvHost);
            }, CONNECTION_TIMEOUT_MSC);
            return;
        }

        this.m_socketUrl = `ws://${socketIP}:${DATA_SERVER_PORT}`;
        this.m_socket = new WebSocket(this.m_socketUrl);

        this.m_socket.onopen = (event) => {
            let cmd = {}
            let cmdStr = JSON.stringify(cmd);
            try {
                this.m_socket.send(cmdStr);
                console.log(`Connected to cmd socket ${socketIP}`);
                this.m_connected = true;
            } catch(error) {
                console.log('Connection error: ' + error.message);
                this.m_connected = false;
            }
        };

        this.m_socket.onerror = (error) => {
            console.log('Connection error: ' + error.message);
            this.m_connected = false;
        };

        this.m_socket.onclose = () => {
            console.log('Connection closed.');
            
            this.m_connected = false;
            this.m_socket = null;

            setTimeout(async () => {
                await this.initConnection(srvHost);
            }, CONNECTION_TIMEOUT_MSC);
            
        };

        this.m_socket.onmessage = this.handleSocketMessage.bind(this);
       
    }

   async resolveSocketIP(srvHost) {

        if (this.m_cachedServerIP && this.m_cachedServerIP !== "") {
            console.log("Using cached server IP:", this.m_cachedServerIP);
            return this.m_cachedServerIP;
        } 

        let socketIP = "";
        if (srvHost && srvHost !== "") {
            socketIP = srvHost;
        }

        if (socketIP === "") {
            try {
                socketIP = await this.fetchServerIP();
                this.m_cachedServerIP = socketIP;
            } catch (error) {
                console.error("Error fetching server IP:", error);
                return null;
            }
        }
        
        return socketIP;
    }

    async handleSocketMessage(message) {
        var messageData;
        var data = message.data;
    
        if (data instanceof Blob) {
            var buffer = await data.arrayBuffer();
            messageData = cbor.decode(buffer);
        } else {
            messageData = JSON.parse(message.data);
        }
    
        if (messageData.request_id === undefined) {
            console.log(`Inappropriate message is received: ${messageData}`);
            return;
        }
    
        this.m_events.emit(messageData.request_id, messageData);
    }
    

    waitIsConnected() {
        return new Promise(async (resolve, reject) => {
            if (this.m_connected) {
                resolve();
            }

            let timerId = setInterval(async () => {
                if (this.m_connected) {
                    clearTimeout(timerId);
                    resolve();
                }
            }, 500);
        })
    }

    async isDemoMode()
    {
        let url = "/api/server/demo";
        console.log(`Fetching demo mode from ${url}`);
        try {
            let response = await fetch(url, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json;charset=utf-8'
                }
            });
            
            console.log(`Response status: ${response.status}`);
            if (response.ok) {
                let result = await response.json();
                console.log(`demo mode = ` + result.isDemoMode);
                return result.isDemoMode;
            } else {
                throw new Error(`Network response was not ok: ${response.status}`);
            }
        } catch (error) {
            console.error(`Failed to fetch demo mode from server: ${error}`);
            return false;
        }
    }

    request(cmd, waitResponce, timeout) {
        timeout = timeout ?? (Config.requestTimeOut ?? 1000);

        let promise = new Promise((resolve, reject) => {

            if (!this.m_connected) {
                reject({ status: 500, msg: "The web socket is not connected now." });
            }

            if (!cmd.request_id) {
                cmd.request_id = this._generateReqId(cmd.sys_type_id)
            }

            let cmdStr = JSON.stringify(cmd);
            this.m_socket.send(cmdStr);
            console.log('sended cmd = ' + cmdStr);

            if (waitResponce === undefined || waitResponce === false) {
                let res = {};
                res.status = 200; // ok
                resolve(res);
            }
        
            setTimeout(() => reject({ status: 500, msg: `Request time out for cmd = ${cmdStr}` }), timeout)

            this.m_events.once(cmd.request_id, (data) => {
//              console.log("Received data: " + JSON.stringify(data));

                let res = data.body
                if (res === undefined) {
                    reject({ status: 500, msg: `Inappropriate response is recevied for the cmd = ${cmdStr}` });
                }

                res.status = 200; // ok
                resolve(res);
            });
        });

        return promise;
    };

    _generateReqId(sysTypeId) {

        const TIME_STAMP_MAX = 1000*60*60*24; // Milisec per round the clock
        const today = new Date()
        const start = new Date(today.getFullYear(), today.getMonth(), today.getDate());
        const timeStamp = Date.now() - start;
        const RANGE = 10000

        const sysTypeId_ = sysTypeId ? sysTypeId : 0;
        let base = sysTypeId_ * TIME_STAMP_MAX + Math.floor(Math.random() * RANGE);
        let offset = timeStamp;
/*        
        if (base == 0) {
            return Math.floor(Math.random() * (MAX_ID - MIN_ID + 1)) + MIN_ID
        }
*/
        let res = base + offset;
        return res;
    }

    async fetchServerIP() {
        let url = "/api/server/IP";
        console.log(`Fetching server IP from ${url}`);
        try {
            let response = await fetch(url, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json;charset=utf-8'
                }
            });
            
            console.log(`Response status: ${response.status}`);
            if (response.ok) {
                let result = await response.json();
                if(result.ip === ""){
                    console.log(`Server IP fetched: failed!`);
                    return null;
                }

                console.log(`Server IP fetched: ${result.ip}`);
                return result.ip;
            } else {
                throw new Error(`Network response was not ok: ${response.status}`);
            }
        } catch (error) {
            console.error(`Failed to fetch server IP: ${error}`);
            return null;
        }
    }
}



const RequestHelper = new _RequestHelper();
export default RequestHelper;
// module.exports = RequestHelper;


