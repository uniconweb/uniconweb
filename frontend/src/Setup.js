import { Model, StatusEnum } from "./data_model/fr_model.mjs";
import Config from "./config.js";
import { Context } from "./Context";
import * as webix from "@xbs/webix-pro";

export const model = new Model(Config.ip);
model.init();

Context.model = model;

model.on("system_status", (status) => {
  if (status === StatusEnum.Inited) {
    model.load();    

  } else if (status === StatusEnum.ReInited) {
    webix.alert({
      title: "Warning",
      text: "The connection is restored. Data reloaded",
      type:"alert-warning"
    });

    model.load();    
  } else if(status === StatusEnum.Changed) {
    webix.message({
      title: "Warning",
      text: "Devices are changed",
      type:"debug"
    });

    Context.updateModel = Date.now();
    Context.setDevices(Context.model.devices());
//  let currDevice = Context.getDeviceByInd(Context.states.indexDevice);
//  Context.setCurrentDivice(currDevice); // Reset current device, because it was reconstructered   
    Context.status = "Core connected";
  } else if(status === StatusEnum.Loaded) {

    Context.updateModel = Date.now();
    Context.status = "Core connected";
    Context.setCurrentDivice(null);
    Context.setDevices(Context.model.devices());

  } else if (status === StatusEnum.Loading) {
    Context.status = "Loading...";
  } else if (status === StatusEnum.Cancelled) {
    Context.updateModel = Date.now();
    Context.setDevices(Context.model.devices());
    Context.status = "Core lost";
  } else if(status === StatusEnum.Disabled) {
    webix.message({
      title: "Warning",
      text: "Another client is connected, this client is disabled",
      type:"alert-warning"
    });
    
    Context.setDevices(Context.model.devices());
    let currDevice = Context.getDeviceByInd(Context.states.indexDevice);
    Context.setCurrentDivice(currDevice); // Reset current device, because it was reconstructered   

    Context.status = "Disabled";
  } else if(status === StatusEnum.Enabled) {
    Context.updateModel = Date.now();
    Context.status = "Core connected";

    webix.message({
      title: "Warning",
      text: "The client is enabled now",
      type:"debug"
    });

  }
});

model.on("error", (text) => {
  console.log(text);
});
