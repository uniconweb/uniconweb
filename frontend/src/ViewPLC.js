import "webix/webix.css";
import WebixComponent, { scroll, resizeWebixComponent } from "./WebixComponent";
import { $$ } from "@xbs/webix-pro";
import * as webix from "@xbs/webix-pro/webix.js";
import React, { useEffect } from "react";
import ViewPLCcontrol from "./ViewPLCcontrol";
import ViewPLCvariables from "./ViewPLCvariables";
import { observer } from "mobx-react";
import { Context, setVisibleElements, setStyleByID } from "./Context";
import { action } from 'mobx';
import {PLCProvider} from "./PLCContext";

export const PLC_Tabs = [
  "PLCParametersTab",
  "PLCControlTab",
];

export function setCurrentTab(id) {
  setVisibleElements([id], PLC_Tabs);
  resizeWebixComponent();
  resizeWebixComponent();
  setTimeout(() => {
    Context.states.currTabViewId = id;
  });
};

export function restoreCurrentTab() {
  let currentTab = $$("PLCtabViewControl").getTabbar().getValue();
  if (currentTab) {
    setVisibleElements([currentTab], []);
    resizeWebixComponent();
    setTimeout(() => {
      Context.states.currTabViewId = currentTab;
    });
  }
}

function PLCtabViewControl() {
  return {
    view: "tabview",
    id: "PLCtabViewControl",
    type:{
      height:"auto"
    },
    cells: [
      {
        id: "PLCsettings",
        header: "Variables",
        select: true,
        body: {
          id: "PLCParametersTab",
          select: true,
        },
      },
      {
        id: "PLCcontrol",
        header: "HMI",
        body: {
          id: "PLCControlTab",
        },
      },      
    ],
    tabbar: {
      // id: "tabbarParam",
      on: {
        onAfterTabClick: function (id, ev) {
          setCurrentTab(id);
        },
      },
    },
  };
}

const PLCTabViewObserver = observer(({}) => {
  useEffect(() => {
    console.log("Render PLCTabViewObserver");
    // console.log(Context.states.indexDevice);
    console.log(" PLC TabView rendered with currTabViewId:", Context.states.currTabViewId);
    setVisibleElements(["PLCParametersTab"], PLC_Tabs);
      $$("PLCtabViewControl").getTabbar().setValue("PLCParametersTab");
  });
  return <WebixComponent ui={PLCtabViewControl()} />;
});

export default class ViewPLC extends React.Component {
  constructor(props) {
    super(props);
    this.title = "second title";
    this.state = { title: "state title", dt: [], deviceId: 0 };
    this.updateDevices = props.updateDevices;
  }

  render() {
    return (
      <div>
        <PLCTabViewObserver />
        {/* <WebixComponent ui={PLCtabViewControl()} /> */}
        <ViewPLCcontrol id="PLCControlTab" />
        <ViewPLCvariables id="PLCParametersTab"/>     
        
      </div>
    );
  }
}
