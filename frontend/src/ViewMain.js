import React, { useEffect } from "react";
import ViewDevices from "./ViewDevices";
import ViewPLC from "./ViewPLC";
import ViewGraphicTrends from "./ViewGraphicTrends";
import ViewCPlotWeb from "./ViewCPlotWeb";
import { Context, setVisibleElements, setStyleByID } from "./Context";
import { PLCProvider } from './PLCContext';

const pageArr = ["devices", "PLC", "idGraphicTrends", "idCPlotWeb"];

export function showPage(id) {
  setVisibleElements([id], pageArr);
}

export default class ViewMain extends React.Component {
  render() {
    return (
   
      <div>
           <PLCProvider>
        <div id="devices">
          <ViewDevices />
        </div>
        <div id="PLC">
          <ViewPLC />
        </div>
        </PLCProvider>
        <div id="idGraphicTrends">
          <ViewGraphicTrends />
        </div>
        <div id="idCPlotWeb">
          <ViewCPlotWeb />
        </div>
      </div>
   
    );
  }
}
