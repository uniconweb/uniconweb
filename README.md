# react-demo

https://mobx.js.org/getting-started

# Configure

If you need custom frontend configuration store it in frontend/src/config.local.json.
Otherwise configuration from config.default.json will be used.
Configuration from config.local.json will override configuration from frontend/src/config.default.json.

example configuration:
```
{
  "ip":"192.168.50.255",
  "requestTimeOut": 2000
}
```

# Installation

install nodejs for windows https://nodejs.org/en/download/
for Linux/Debian

```
sudo curl -fsSL https://deb.nodesource.com/setup_16.x | sudo bash -
sudo apt-get install -y nodejs
```

```
npm install -g yarn
npm install -g copy-files-from-to
npm i / yarn install
```

НО! тут используется проприетарная библиотека webix, поэтому

1. нужно проассоциировать scope @xbs с репозиторием webix (https://npm.webix.com)
2. залогиниться (попросить @ashvabsky добавить вас в список пользователей webix) npm config set @xbs:registry https://npm.webix.com npm login --registry=https://npm.webix.com --scope=@xbs // теоретически должно быть достаточно команды npm login, // но, почему-то не работает
3. эти команды больше не нужны, так как для логина в репозитории требующие авторизации можно использовать .npmrc. Этот файл создан лежит в директории frontend и содержит токен (с использованием учетных данных пользователя hrisaor), который обеспечивает авторизацию.

# Запуск с использованием докера

собрать имидж

```
docker-compose build
```

запустить

```
docker-compose up
```

# Описание API

### Сохранение конфигурации

| \*          | \*          |
| ----------- | ----------- |
| URL         | /api/config |
| HTTP Method | POST        |

Тело запроса

```
{
    "id": "qwe2",
    "config": {
        "test1": "asd",
        "test2": "zxc"
    }
}
```

### Сохранение конфигурации

| \*          | \*          |
| ----------- | ----------- |
| URL         | /api/config |
| HTTP Method | GET         |

Параметры запроса

```
/api/config?id=qwe2
```

# Scichart licensing
Загрузить проект с ресурса
```
https://github.com/ABTSoftware/SciChart.JS.Examples.git
```
Проект называется SciChart.JS.Examples/Sandbox/demo-dotnet-server-licensing/

- Открыть solution в vscode
- В файле DotnetServerLicensing.csproj Заменить имя сборки на ScichartServerLicensing:
```
<AssemblyName>ScichartServerLicensing</AssemblyName>
```  
Установить ключ сервера, который присылает техподдержка
В файле Startup.cs:
```
services.AddLicenseServer("9D4PfBekUxNA3mN763xn+BrdAXudLnL84j+svFwPoJcb8f8t4xB+x/JpFUPQPVeat1SZ296rM2jN+xaMtct+CqYukdmIoG3roFZg0dtT9rQ3Wu9hZ9sM0WURUTzwck+78iotW3XGNRYvkAa9w6tPsfSxtozzyBC0G3JIxN2VttCcDFSruixUl14TDB/a81D93xDNRy3t0PuDO93D7sDGA8dnJGb6EWlpLrlX6EW0Hu23VOlh4YVc81/tDlk73NLWpgGHFAHIE3BGqIhIwPk0q6ea7RXz2p+2zMhpcKCvgBN16PjR9etPaqPHcsRW98R0gwFUzLQz1/4C2E6VnLOnGoJEzkBFD3Adc6wRfgNbQbyeP82eh8Vnc+B58S25L9roTTOVzNtGJf07vSh7ex/bLTiXLQeAUG7DSH1wgZ7T7ZIQpkw2DPdyCHoZ0JWbEv07G3B7oWrj58dRYt3tSrw6lwOGhxgI+QFumusGt7U5yNJAlVorQiSnqOgNTDrdUv+ux/wM9z/ze/KxN1TwhTUR2OJR5d9x++vPtjO+kA/rA8qz8pnMWBbMmkywDZXUsaKTU6rJZx8TthqtL2JLL+MKc7lluMZbkxwkPXqOn5hTytRH0i/ptRX6WXOO4fPkByPcqZHuIjGSxsqxCd54OsTI5IR/3lkxRfA=");
```

В uniconweb установить ключ клиента, который присылает техподдержка
```
 const LICENSE_KEY = "gu+2fCx0keJAMYee7rONF7fm5eVx4W4gKfRGACDXju3mwhDCLm+LwgbY1gVGz6YAro8vpyt/ymjhookolQkv7JfvaeVNI0OhH1hPYPSfX/wPC2uTamVa2LxChz6A/U9CkB7yAJanm7O9KfvymReEyoI0yle8ie01K+fqHH/a2Y7zCOH7Q0y4LncQbLMzYaPsN7OSJEgvpUIbjDXv/O05lLlnl8xYYEOBgANUu5NrJQw2JMUOtqz5/8xzrEZ2P4PRxox3bw6OKksnk1dtIo4bzS+4AWGkLCRsJH45YF1WJErLr+UcvlciVZcA3fEsVvi64Yg8ti4+Ul/wfMOBHY/0SLA8qF5xSjO64nj487lOgK9u2z2ojImMv7H3lX+z29yV8O/17JU/ClEtR/aH0oe5H0mh30A+sg7Ca3ujOAMmhW5el0nGTOuyU70CoE0t4MdvBMLim/08Jn0JDnP0FAU0/ehwefqqFzmIyXdBMtrM8Pg84RwiD//q6AlI0Tl08ahHH+b1wH8629sr3sBjH5V62MAvQcuSWBw0f4uyh7ZgCN8bpjaGz2hDy1u1r5JYSQjV+AJzVjmtrj0F+yL2Rq1WfzrUBvnvzmS01+g48BW+FRxmky38Bu0ixNo0ACyaIbH12eFou3zlw9eNTh3wUfInpmEFUHxp2w4g";
```

SciChartSurface.setRuntimeLicenseKey(LICENSE_KEY);

Запустить сервер лицензий:
```
npm install 
npm run build
dotnet build  
dotnet run
```
В дальнейшем для запуска достаточно только:
```
dotnet run
```