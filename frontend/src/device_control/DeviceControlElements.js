import 'webix/webix.css';
import { $$ } from "@xbs/webix-pro";
import * as webix from "@xbs/webix-pro";
import { Context } from '../Context';
import { ValueFormatEnum } from "../data_model/param.mjs";
import  {ColorEnum} from "./DeviceControlProperties";

let _elemNum = 0;

export function addDragControl(view) {
  console.log("addDragControl",view);
  webix.DragControl.addDrag(view.$view, {
    $dragMove:function(source, target, obj){
      console.log("dragMove",source);
      return false;
     
    } ,
    $dragDestroy:function(){
      return false;
    },
    $dragPos:(pos) => { 
      var context =  webix.DragControl.getContext();
      let control = webix.$$(context.source[0]);

      control.config.left = alignToGrid(parseInt(pos.x + context.x_offset));
      pos.x = control.config.left - webix.DragControl.left;
      control.config.top = alignToGrid(parseInt(pos.y+context.y_offset));
      
      pos.y = control.config.top - webix.DragControl.top;
      if (pos.y < 0 ) {pos.y = 0; };
      if (pos.x < 0 ) {pos.x = 0; };
      if (pos.y > (view.$view.clientHeight - control.config.height - 65) ) {pos.y = $$("controlLayoutForm").$view.clientHeight - control.config.height - 65 };
      if (pos.x > (view.$view.clientWidth - control.config.width - 10) ) {pos.x = $$("controlLayoutForm").$view.clientWidth - control.config.width - 10 };
    },
    $dragCreate:function(source, ev){
      const el = webix.$$(ev);
      if (el && el !== view){
          var pos = webix.html.pos(ev);
          var context = webix.DragControl.getContext();
          context.source = [el.config.id];
          context.from = view;
          context.y_offset = el.config.top - pos.y;
          context.x_offset = el.config.left - pos.x;
          return el.$view;
      }
      return false;
    }
  })
}

function alignToGrid(x) {
  return Math.round(x / 10) * 10;
}

export function render(elem) {
  elem.data["width"] = Number(elem.data["width"])+1;
  elem.resize();
  elem.render();
  elem.data["width"] = Number(elem.data["width"])-1;
  elem.resize();
  elem.render();
}

export function updateElementPropertyValue(elem, prop, value, oldValue) {
  if (value && elem.data[prop] && elem.data[prop] === value) 
    return;

  elem.data[prop] = value;

  if (prop === "labelWidth") {
    elem.data["width"] = Number(elem.data["width"]) + Number(value) - Number(oldValue);
//  props.updateValue("width", elem.data["width"]);
  }  

  if (prop === "selectColor") {
    elem.data["currentColor"] = value;
  }

  elem.data["label"] = elementLabel(elem);

  updateElemStyle(elem);

  elem.render();
}

function elementLabel(elem) {
  let res = "";
  if (elem.data["baseLabel"]) {
    let ed = "";
    let edNum = "";
    if (elem.data.view === "slider" ) {
      edNum = $$(elem).data.value ? " — " + $$(elem).data.value : "";
    }

    if (elem.data["unit"] === 1 && elem.data.modelData?.valueUnit !== "") {
      ed = " (" + elem.data.modelData?.valueUnit + ")";
    }
    res = elem.data["baseLabel"] + edNum + ed;
  }
  
  return res;
}

export function parseConfig(confObj, layoutForm, currentDevice) {
  if (!confObj) return;

  if (!("elements" in confObj)) {
    console.log("Error config file");
    return;
  }

  _elemNum = 0;
  let elements = confObj["elements"];
  for (let elemObj of elements) {
    let view = elemObj["view"];
    let elemName = elemObj["name"];
    let paramID = elemObj["param_ID"];
    let param = undefined;

    _elemNum++;
    let newElem = createElement(elemName, view["top"], view["left"], view["width"], _elemNum)
    let id = addElementToView(layoutForm, newElem)

    if (!$$(id)) return;

    if (paramID) {
      let device = currentDevice ? currentDevice : Context.model.device(paramID.deviceId);
      param = device.param(paramID.moduleId, paramID.paramId);
    }

    if (param) {
      assignParam(id, param)
    }

    for (let key of elemObj.props["properties"]) {
      if (key in elemObj.props && key in $$(id).data) {
        $$(id).data[key] = elemObj.props[key];
      }
    }
   
    updateElemStyle($$(id));
    $$(id).render();
  }

}

export function assignParam(elemId, param) {
  let elem = $$(elemId);
  const paramName = Context.isPLCMode() ? param.fullName() : param.displayName();
  const selectedDevice = Context.model.device(param.ID.deviceId);

  elem.data["modelData"] = param;
  elem.data["param_name"] = paramName;
  elem.data["device_name"] = selectedDevice.displayName(); 

  elem.data["baseLabel"]= paramName ;
  elem.data["readonly"] = param.isReadOnly();

  elem.data["unit"] = false;

  let properties = elem.data.properties;
  properties.push("param_name");
  properties.push("unit");
  properties.push("device_name");

  let optionText = [];
  let options = [];
  for (let key in param.valueTexts) {
    optionText.push(param.valueTexts[key]);
    options.push({id:key, value:param.valueTexts[key]})
  }

  const elemName = elem.config.view;
  switch (elemName) {
    case "label":
      break;
    case "text":
      const useValueFormat = param.valueFormat === ValueFormatEnum.Bin || param.valueFormat === ValueFormatEnum.Hex;
      const label = (param.valueFormat === ValueFormatEnum.Bin) ? "bin value" : (param.valueFormat === ValueFormatEnum.Hex) ? "hex value" : "";
      elem.data["use_value_format"] = {value: useValueFormat, label: label};

      if (useValueFormat) {
        properties.push("use_value_format");
      };

      elem.data["min"] = "";
      elem.data["max"] = "";

      elem.data["selectColor"] = param.isReadOnly() ? ColorEnum.Silver : ColorEnum.Turquoise;
      elem.data["currentColor"] = elem.data["selectColor"];
      
      properties.push("min","max","selectColor", "currentColor");

      break;
    case "slider":
      elem.data["min"] = "1";
      elem.data["max"] = "100";
//    elem.data["min"] = $$(id).data.modelData.min ;
//    elem.data["max"] = $$(id).data.modelData.max ;
      elem.data["step"] = "1";
      properties.push("min","max","step");
      break;
    case "button":
      let btnPropName = (valueFormat) => {
        if (valueFormat === ValueFormatEnum.Text) {
          return "set_value_combo";
        }
        return "set_value";
      } 
  
      let prop = btnPropName(param.valueFormat);
      elem.data[prop] = {value :param.lastValue().value};
      
      if (options.length > 0) {
        elem.data[prop].options = options;
      }
      properties.push(prop);

      break;
    case "combo":
      elem.data["option_list"] = optionText.join();
      elem.define("options", options);
      properties.push("option_list");

      break;

    case "switch":
      elem.data["bit_num"] = 1;
//    elem.data["selectColor"] = ColorEnum.Silver;
//    elem.data["currentColor"] = elem.data["selectColor"];

      properties.push("bit_num");
      
      break;      
    default:
      return undefined;
  }

  elem.data.properties = properties;
  elem.data.value = undefined;
  elem.data["label"] = elementLabel(elem);
  // render(elem);
  updateElemStyle(elem);
}

export function addElementToView(view, elem) {
  if (!view.addView) return;
  let id = view.addView(elem)

  if(!id) {
    console.log("Error! Coudn't add new element, elem = ${elem}");
  }
 
  let control = $$(id);
  control.$view.classList.add("addElementToViewStyle_"+elem.view);

  render(control);

  return id;
}

export function collectConfigData(elements) {

//let elements = srcForm.getChildViews();
  let arr = []
  elements.forEach(element => {

    let d = {};
    d["view"] = {}; // представление комопненты (координаты, размер, ...) 
    d["props"] = {}; // св-ва компопненты
    d["name"] = element.config.view; // тип компоненты
    d["param_ID"] = element.config.modelData?.ID; // привязанный объект класса параметр из модели

    let view = d["view"]
    view["id"] = element.config.id;
    view["label"] = element.config.label;
    view["left"] = element.config.left;
    view["top"] = element.config.top;
    view["width"] = element.config.width;

    let props = d["props"]
    props["properties"] = element.config.properties;

    element.config.properties.forEach((prop, val) => {
      props[prop] = element.config[prop];
    });

    arr.push(d); 
  });

  return arr;
}

export function createElement(name, top, left, width, elemNum) {
  let elemStruct = {top:top, left:left, width:width, streamable:false, readonly:true}
  let properties = ["baseLabel", "label", "width", "height"];
  elemStruct["view"] = name;

  switch (name) {
    case "label":
      elemStruct["label"] = "label";
      elemStruct["baseLabel"] = "label";
      
      elemStruct["align"] = "left";
      elemStruct["autowidth"] = false;
      elemStruct["font_size"] = "100%";
      elemStruct["font_color"] = "black";

      webix.html.addStyle(`.customLabelClass .webix_el_box {font-size: ${elemStruct["font_size"]}; color: ${elemStruct["font_color"]};}`);
      
//    elemStruct["css"] = {"font-style": "italic", "font-size": "x-small", "color" : "grey"};
      elemStruct["css"] = "customLabelClass" + "_" + elemNum;
      
//      elemStruct["inputWidth"] = width;
 //     elemStruct["width"] = width;
      properties.push("autowidth", "font_size", "font_color");
      break;
    case "text":
      elemStruct["label"] = "parameter" //"<span class='myCss'>My Label</span>";
      elemStruct["baseLabel"] = "parameter" //"<span class='myCss'>My Label</span>";
      elemStruct["placeholder"] = "value...";
      elemStruct["font_size"] = "x-large";
      elemStruct["font_color"] = "green";

      elemStruct["height"] = 60;
      elemStruct["readonly"] = true;
      elemStruct["labelPosition"] = "top";
      elemStruct["streamable"] = true;
      elemStruct["css"] =  "customTextClass"+ "_" + elemNum;
      elemStruct["currentColor"] = ColorEnum.No_Color; // no color

      properties.push("font_size", "font_color");
      webix.html.addStyle(`${elemStruct["css"]} {border: 0px solid #b6e2ec !important; background: ${ColorEnum.color(elemStruct["currentColor"])};}`);


      break;
    case "slider":
      elemStruct["label"] = "";
      elemStruct["min"] = 1;
      elemStruct["max"] = 100;
      elemStruct["width"] = 410;
      elemStruct["height"] = 70;
      elemStruct["labelPosition"] = "top";
      elemStruct["labelWidth"] = 60;
      elemStruct["step"] = 1;
      elemStruct["streamable"] = true;

      // elemStruct["title"] = webix.template("#value#");
      break;
    case "button":
      elemStruct["label"] = "button";
      elemStruct["baseLabel"] = "button";
      elemStruct["streamable"] = false;
      elemStruct["height"] = 38;

      break;
    case "switch":
      elemStruct["label"] = "bit value";
      elemStruct["labelWidth"] = 80;  
      elemStruct["streamable"] = true;
      elemStruct["height"] = 38;
      elemStruct["width"] = 150;
      elemStruct["labelRight"] = ""; 
      elemStruct["onLabel"] = ""; 
      elemStruct["offLabel"] = ""; 

      elemStruct["css"] =  "color_switch"+ "_" + elemNum + " .webix_switch_box.webix_switch_on";
      elemStruct["selectColor"] = ColorEnum.No_Color; // no color

      properties.push("labelWidth", "labelRight", "onLabel", "offLabel", "selectColor");  
      webix.html.addStyle(`${elemStruct["css"]} {background-color: ${ColorEnum.color(elemStruct["currentColor"])};}`);

      break;
      case "combo":
      elemStruct["label"] = "parameter";
      elemStruct["baseLabel"] = "parameter";
      elemStruct["labelPosition"] = "top";
      elemStruct["height"] = 60;
      elemStruct["width"] = 200;
      elemStruct["streamable"] = true;
      elemStruct["options"] = ["one", "two", "three"];

      properties.push("labelWidth");
      break;
      
    default:
      return undefined;
  }
  
  elemStruct["properties"] = properties;

  return elemStruct;
}

async function updateElemValues(elements) {
  if (!elements || elements.length == 0) return;

  for (let element of elements) {
    let param =  element.config.modelData;
    const stremable = (element.config["streamable"] == true)
    if (param && stremable === false) {
      const value = await param.currentValue();      
      if (value != null && value != undefined)
      updateElemValue(element, value.value);
    }
  }  
}

export function updateElemValue(elem, value) {
  let param =  elem.config.modelData;
  if (!param) return;

  if (value === undefined) {
    value = param.lastValue().value;
  }

  let useFormat = "use_value_format" in elem.config ? elem.config["use_value_format"].value : false;
  if (elem.config.view == "switch") {
    useFormat = true;
  }

  const bitNum = "bit_num" in elem.config ? elem.config["bit_num"] : undefined;
  
  const displayValue = elem.config.view == "combo" ? value : param.displayValue(value, useFormat, bitNum);

  if (elem.data['value'] === displayValue) 
    return;

  elem.data['value'] = displayValue;
  elem.data['label'] = elementLabel(elem);

  let prevColor = elem.data["currentColor"];
  if (elem.data.view === "text") {
    if (elem.data["min"] !== "" && Number(value) <= elem.data["min"]) {
      elem.data["currentColor"] = ColorEnum.Orange;
    } else if (elem.data["max"] !== "" && Number(value) >= elem.data["max"]) {
      elem.data["currentColor"] = ColorEnum.Red;
    } else {
      elem.data["currentColor"] = elem.data["selectColor"];
    }
  }

  let needUpdate = (elem.data["currentColor"] != prevColor);

  if (needUpdate) {
    updateElemStyle(elem);
  }

  elem.render();
}

function updateElemStyle(elem) {
  if (elem.data["css"] ) {
//  webix.html.removeCss(elem.getNode(), cssName);
    let style = "";
    if (elem.config.view === "label") {
      style = `.${elem.data["css"]} .webix_el_box {font-size: ${elem.data["font_size"]}; color: ${elem.data["font_color"]};}`;
    } else if (elem.config.view === "text") {
      style = `.${elem.data["css"]} {border: 0px solid #b6e2ec !important; background: ${ColorEnum.color(elem.data["currentColor"])} !important;}`
    } else if (elem.config.view === "switch") {
      style = `.${elem.data["css"]} {background-color:${ColorEnum.color(elem.data["selectColor"])} !important;}`
    }

    if (style !== "") {
      webix.html.addStyle(style);
    }
  }

   render(elem);  
}

export async function receiveParams(params, elements, frequency) {

  let param_readers = [];
  for (let param of params) {
    if (!param.isStreamming()) {
      const stream = Context.model.openParamStream(param, frequency);
      if (!stream) continue;

      const reader = param.stream.getReader();
      if (!reader) continue;
  
      let param_elems = [];
//    let elements = $$("controlLayoutForm").getChildViews();
      elements.forEach(element => {
        if (element.config.modelData?.ID.deviceId === param.ID.deviceId
          && element.config.modelData?.ID.moduleId === param.ID.moduleId 
          && element.config.modelData?.ID.paramId === param.ID.paramId) {
          param_elems.push(element);
        }
      });
  
      param_readers.push({reader, param_elems});
    };
  }

  Context.model.startParamStreams(frequency);  

  const updateValues = async () => {
    let doBreak = true;
    
    for (let p_r of param_readers) {

      const { done, value: values } = await p_r.reader.read();

      if (done) {
        return;
      }

      if (values && values.length > 0) {
        doBreak = false;
        let value = values[values.length - 1];
//      console.log("Received value, deviceId = " + value.deviceId + ", moduleId = " + value.moduleId + ", paramId = " + value.paramId);
        for (let elem of p_r.param_elems) {
          const allowSetValue = ('allow_set_value' in elem.data) ? elem.data['allow_set_value'] : true;
          if (allowSetValue) {
            await updateElemValue(elem, value.value);  
          }
        }
      }
    }

    if (!doBreak) {
      setTimeout(await updateValues());
    }
  };

  setTimeout(await updateValues());
}

export async function closeParamStreams(params) {

  if (Context?.currentDevice === undefined) {
    return; // not inited yet
  }
  await Context.model.forceCloseAllStreams(Context.currentDevice?.deviceId ?? 0)
}

export function onButtonClick(element) {

  let buttonPropName = (valueFormat) => {
    if (valueFormat === ValueFormatEnum.Text) {
      return "set_value_combo"; // the case when a button is bound to a text param (f.e. selected by combo)
    }
    return "set_value";
  }

  let param = element.data["modelData"];
  let value = element.data[buttonPropName(param.valueFormat)];
  param?.asyncSetValue(value.value); 

}

export function onSliderChange(element, value) {
  let param =  element.config.modelData;
  let useFormat = element.config["use_value_format"]?.value;
  const displayValue = param.displayValue(value, useFormat);
  element.data['value'] = displayValue;
  element.data['label'] = elementLabel(element);
  element.data['allow_set_value'] = false; 
  
  element.render();  
}

export async function onChangeValue(element, value, oldValue) {
  console.log("onChange to ", value, "from", oldValue);
  let param = element.data["modelData"];
  await param?.asyncSetValue(value);

  if ('allow_set_value' in element.data) {
    element.data['allow_set_value'] = true;
  }

  if (param.lastError < 0) {
    updateElemValue(element, oldValue);
    return;
  }

  updateElemValue(element, value);  
}

