import "webix/webix.css";
import WebixComponent, { scroll } from "./WebixComponent";
import React, { useEffect, useState } from "react";
import { $$ } from "@xbs/webix-pro";
import * as webix from "@xbs/webix-pro/webix.js";
import {
  Context,
  showPageAndHiddenElements,
  setVisibleElements,
} from "./Context";
import { observer } from "mobx-react";
import { restoreCurrentTab as dev_restoreCurrentTab } from "./ViewDevices";
import { restoreCurrentTab as plc_restoreCurrentTab } from "./ViewPLC";
import { showPage } from "./ViewMain";

const body1 = {
  margin: 10,
  padding: 0,
  type: "wide",
  view: "flexlayout",
  cols: [
    {
      view: "toggle",
      deviceID: "m231",
      label: '<span class="material-icons">' + "</span> 123",
    },
  ],
};

const accordionInit = [
  { header: "Graphic trends", id: "GraphicTrends", body: "123" },
  { header: "PLC", id: "PLC", body: "" },
  { header: "cPlotWeb", id: "cPlotWeb", body: body1 },
  { header: "Devices", id: "deviceInit", body: "" },
];

function setScroll(body) {
  return {
    view: "scrollview",
    scroll: "y", // vertical scrolling
    body: body,
    maxHeight: 360,
  };
}

function accordion1() {
  return {
    multi: false,
    rows: [
      { header: "panelA", id: "panelA", body: { template: "Default section" } },
      {
        header: "panelB",
        collapsed: true,
        id: "panelB",
        body: { id: "contentB", rows: [] },
      },
    ],
    on: {
      onAfterExpand: function (id) {
        var view = $$(id);
        console.log("id", id, view);
        if (id === "panelB" && !view.complete) {
          webix.ui([{ template: " Section B " }], $$("contentB"));
          view.complete = true;
        }
      },
    },
  };
}

function accordion() {
  //  let v = scroll(buttons(),"qwe1");
  //  v= buttons();
  return {
    view: "accordion",
    // width: 0,
    id: "accmain",
    type: {
      height: "auto",
    },
    multi: false,
    scroll: "y",
    collapsed: true,
    select: true,
    rows: [],
    on: {
      onChange: function (newValue, oldValue, config) {
        // console.log(newValue)
      },
      onAfterExpand: function (id) {
        let changeId = $$(id);
        let newHeight = 42;
        switch (id) {
          case "deviceInit":
            newHeight = 0;
            // Context.showPages(["ViewDevices"]);
            showPage("devices");
            showPageAndHiddenElements("");
            setVisibleElements(["devices"], ["PLC"]);
            dev_restoreCurrentTab();
            break;

          case "cPlotWeb":
            // Context.showPages(["ViewCPlotWeb","chartCPlotWeb"]);
            showPage("idCPlotWeb");
            break;

          case "PLC":
            //newHeight = 0;
            showPage("PLC");
            setVisibleElements(["PLC"], ["devices"]);
            showPageAndHiddenElements("");
            plc_restoreCurrentTab();
            break;

          case "GraphicTrends":
            // Context.showPages(["ViewGraphicTrends","chartTrends"]);
            showPage("idGraphicTrends");
            break;
          default:
            break;
        }
        changeId.config.height = newHeight;
        changeId.resize();
      },
    },
  };
}

const setActiveDevice = (devices, deviceID) => {
  console.log("setActiveDevice", devices);
  if (deviceID) {
    devices.cols.forEach(function (item, index, array) {
      if (item.deviceID === deviceID) {
        Context.states.indexDevice = devices.cols[index].deviceID;
        devices.cols[index].value = 1;
      } else {
        devices.cols[index].value = 0;
      }
    });
  } else {
    if (devices.cols.length > 0) {
      setActiveDevice(devices, devices.cols[0].deviceID);
    }
  }
};

export const updateLeftMenuBase = (devicesArr, selection) => {
  let options = [];
  let minWidthBut = "130";
  // if ((devicesArr.length % 3) == 0) minWidthBut = 95;
  let devices = {
    margin: 10,
    padding: 0,
    type: "wide",
    // view:"flexlayout",
    rows: [],
  };
  devicesArr.forEach(function (item, index, array) {
    let labelCss = item.enabled ? "" : "label_disabled";
    let wxiIcon = item.enabled ? "wxi-eye" : "wxi-eye-slash";
    devices.rows.push({
      view: "toggle",
      type: "icon",
      icon: wxiIcon,
      deviceID: item.id,
      // label: '<span class="material-icons">' + "</span> item.name"
      onLabel: '<span class="">' + item.displayName() + "</span> ",
      offLabel:
        "<span class=" + labelCss + ">" + item.displayName() + "</span> ",
      minWidth: minWidthBut,
      height: 80,
      // css: "webix_primary",
      value: Context.states.activeDeviceId === item ? 1 : 0,
      click: function (id, event) {
        Context.setCurrentDivice(item);
        let s1 = $$(id).getParentView();
        s1.getChildViews().forEach((element) => {
          element.setValue(0);
        });
      },
    });
  });
  // For default device
  // console.log("devices", devices, $$("deviceInit"));
  // setActiveDevice(devices,null);
  let scrollDev = setScroll(devices);
  webix.ui(scrollDev, $$(selection), 0);
  $$(selection).adjust();

  return devicesArr;
};

const MenuAccordion = observer(({}) => {
  useEffect(() => {
    updateLeftMenuBase(Context.devices, "deviceInit");
  }, [Context.devices, Context.updateModel, Context.states.currTabViewId]);

  return (
    // <WebixComponent ui={accordionTest()} data={accordionInit1} updateModel={ Context.updateModel  } />
    <WebixComponent
      ui={accordion()}
      data={accordionInit}
      updateModel={Context.updateModel}
    />
  );
});

export default MenuAccordion;
