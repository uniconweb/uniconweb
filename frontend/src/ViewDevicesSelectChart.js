import React , { useState } from "react";
import "webix/webix.css";
import ReactDOM from "react-dom";
import WebixComponent from "./WebixComponent";
// import { $$, template } from 'webix';
import { $$, template } from "@xbs/webix-pro";
import { Context, addCssClass } from "./Context";
import { stringify } from "webix";

let colorsArrTab = [];
let colors = [
 
  { id: "#ffff00", value: "<span class='color-icon' style='background-color: #ffff00;'></span>" },
  { id: "#ff0000", value: "<span class='color-icon' style='background-color: #ff0000;'></span> " },
  { id: "#0000ff", value: "<span class='color-icon' style='background-color: #0000ff;'></span> " },
  { id: "#ff00ff", value: "<span class='color-icon' style='background-color: #ff00ff;'></span>" },
  { id: "#800000", value: "<span class='color-icon' style='background-color: #800000;'></span>" },
  { id: "#008000", value: "<span class='color-icon' style='background-color: #008000;'></span>" },
  { id: "#000080", value: "<span class='color-icon' style='background-color: #000080;'></span>" },
  { id: "#808080", value: "<span class='color-icon' style='background-color: #808080;'></span>" },
  { id: "#ff8000", value: "<span class='color-icon' style='background-color: #ff8000;'></span>" },
  { id: "#800080", value: "<span class='color-icon' style='background-color: #800080;'></span>" },
  { id: "#01ff00", value: "<span class='color-icon' style='background-color: #01ff00;'></span>" },
  { id: "#01ffff", value: "<span class='color-icon' style='background-color: #01ffff;'></span>" },
 ];
let selectedParams = [];


const clearDataTable = () => {
  let dataTable = $$("showSelectChartWindowData");
  /*
  selectedParams.forEach(function(item, index, array) {
    let item2 = dataTable.getItem(item.row);
    item2.line = 0;
  });
*/

  dataTable.clearAll();
  selectedParams = [];
  colorsArrTab = [];
};

const ViewDevicesSelectChartUI = () => {

  let selectedMinY = -1000; 
  let selectedMaxY = 1000; 
  let timeX = 10000;

  
  let v = {
    view: "window",
    id: "showSelectChartWindow",
    height: 650,
    width: 800,
    left: 50,
    top: 50,
    editable:true,
    // css:"showSelectChartWindowData",
    move: true,
    modal: false,
    head: "Select oscilloscope channels",
    body: {
      rows: [
        {
          id: "showSelectChartWindowData",
          view: "datatable",
          editable:true,
          height: 500,
          width: 800,
      
          columns: [
            { id: "channel", header: "Channel", width: 120, css: "center" },
            {
              id: "name",
              header: ["Name", { content: "textFilter" }],
              fillspace: 1,
            },
            {
              id: "status",
              header: ["Show", { content: "masterCheckbox" }],
              width: 80,
              css: "center",
              template: "{common.checkbox()}",
            },
            {
              id: "line", 
              header: "Num Line",
              width: 200,
              editor: "richselect",
              options: colors,
              css: { "border-left": "1px solid #D3D3D3" },
              template: function(obj) {//id={id:1, channel:1, name: '@VA[1.1]', color:'#ffff80', status:1, ...} 
              let selectedColor = colors.find(item => item.id === obj.line)?.id;
              
              if (obj.line > 0) {    
                  return `<span class="color-icon" style="background-color: ${colorsArrTab[obj.line - 1]};"></span>`;
              } else if (selectedColor) {
                  let index = selectedParams.findIndex(item => item.var_id === obj.var_id);
                  if (index > -1 && obj.line ) {
                    selectedParams[index].color = selectedColor;
                    colorsArrTab.splice(index, 1, selectedColor);  
                                   
                  }
                  
                
                  //console.log(selectedParams);
                  //console.log(colorsArrTab);
              
                  return `<span class="color-icon" style="background-color: ${selectedColor};"></span>`;
              } else {
                  return `<span class="color-icon" style="background-color: ${colorsArrTab[12]};"></span>`;
              }
              },
            },
            { 
              id:"userName",	
              editor:"text", 
              value:"",
              header:"User name", 
              width:150, 
              fillspace:1,
              editable: true, 
              css: { "border-left": "1px solid #D3D3D3" },
              template: function (obj) {
//                console.log("obj: " + JSON.stringify(obj));
                let valueName = obj.userName ?? "";
                
                let index = selectedParams.findIndex(item => item.var_id === obj.var_id);
                if (index > -1) {
                  selectedParams[index].userName = valueName;
//                  console.log("userName: "  + selectedParams[index].userName);

                }

//                console.log("valueName: "+ valueName);
                
                return valueName;
              }
            },
            
          ],
          data: [],
          on: {
            onCheck: function (row, column, state) {
              // console.log("onCheck");
              let table1 = $$("showSelectChartWindowData");
              let item = table1.getItem(row);
              console.log("state = " + state);  

                if (state == 1) {
                  let index = selectedParams.findIndex(param => param.var_id === item.var_id);
                  if (index > -1) {
                    item.userName = selectedParams[index].userName;
                    item.color = colorsArrTab[index];
                  } else {
                    if (item.name !== "—") {
                      let showParam = {
                        name: item.name,
                        channel: item.channel,
                        var_id: item.var_id,
                        row: row,
                        color: item.color,
                        isDiscrete: item.isDiscrete,
                        userName: item.userName ?? ""
                      };

                      selectedParams.push(showParam);
                      //console.log(colorsArrTab);
                      colorsArrTab.push(item.color);
                      item.line = selectedParams.length;
                      console.log(selectedParams);
                  }
                }
              } 

              if (state == 0) {
                item.userName = ""; 
                item.line = 0;

                item.status = 0;                
                console.log("item = " + JSON.stringify(item));  

                let index = selectedParams.findIndex(param => param.var_id === item.var_id)
                if (index != -1) {
                  selectedParams.splice(index, 1)
                  colorsArrTab.splice(index, 1)
                }
              }

//            console.log("item = " + JSON.stringify(item));
                  //потом возможно закомментировать
              selectedParams.forEach(function (param, index, array) {
                let item_ = table1.getItem(param.row);
                item_.line = index + 1;
                table1.updateItem(param.row, item_);
              });

              table1.updateItem(row, item);
            },
            onBeforeEditStart: function(id) {//activate rows after checkbox
              const row = this.getItem(id.row);
              return !!row.status;
            },
            
          },
        },
        {
          
          height:40,
          width: 800,
          editable: true,
          cols:[ 
        {
          id: "minY",
          css: "myInput",
          view: "text",
          label: "minY",
          labelAlign: "right",
          labelWidth: 50,
          type: "number",
          placeholder: "-1000",
          inputAlign: "left",
          width: 220,          
          on: {
            onKeyPress: function(code, event) {
              if (event.key === "Tab") return;
              if (event.key < "0" || event.key > "9") {
                if (event.key !== "Backspace") {
                  if (event.key !== "-") {
                  event.preventDefault();
                  }
                }
              }
            }
          },
        },
        {
          id: "maxY",
          css: "myInput",
          view: "text",
          type: "number",
          label: "maxY",
          labelAlign: "right",
          labelWidth: 50,          
          placeholder: "1000",
          inputAlign: "left",
          width: 220,
          on: {
            onKeyPress: function(code, event) {
              if (event.key === "Tab") return; 
              if (event.key < "0" || event.key > "9") {
                if (event.key !== "Backspace") {
                  if (event.key !== "-") {
                  event.preventDefault();
                  }
                }
              }
            }
          },
        },
        
      ],

    },
        {
          
          height: 48,
          cols: [
            {
              label: "Cancel",
              view: "button",
              height: 0,
              click: function (id, event) {
                // console.log("webixButton");
                $$("showSelectChartWindow").hide();
                clearDataTable();
              },
            },
            {
              label: "Apply",
              view: "button",
              height: 0,
              click: function (id, event) {
                $$("showSelectChartWindowData").eachColumn(function(id, col) {
                  var filter = this.getFilter(id);
                  if (filter) {
                    if (filter.setValue) {
                      filter.setValue("")  
                    }
                        else { 
                          filter.value = "";  
                        }        
                  }
                });
                $$("showSelectChartWindowData").filterByAll();
                
            
                let chartToVisible = Context.getChartFromPool();
                if (chartToVisible) {
                  /*                    
                    let paramArr = [];
                    selectedParams.forEach(function(item, index, array) {
                        paramArr.push(item.name);
                    });
*/
                  let chartSettings = new Map();
                  if (Context.paramToCharts.has(Context.states.indexDevice)) {
                    chartSettings = Context.paramToCharts.get(
                      Context.states.indexDevice
                    );
                  }

                  let minY = selectedMinY;
                  let maxY = selectedMaxY;
    
                  if ($$("minY").getValue() !== "") {
                    minY = parseInt($$("minY").getValue(),10);
                    
                  }
    
                  if ($$("maxY").getValue() !== "") {
                    maxY = parseInt($$("maxY").getValue(),10);
                  
                  }
    
                  let paramsXY = { minY, maxY, timeX };
                  chartSettings.set(chartToVisible, { selectedParams, paramsXY });

                  $$("showSelectChartWindow").hide();
                  clearDataTable();
                  
                  Context.paramToCharts.set(Context.states.indexDevice, chartSettings);
                  Context.addChart(Context.states.indexDevice, chartToVisible);
                  Context.osc_state.display_resolution_ms = timeX
                  Context.osc_state.yAxisRange.maxY = maxY;
                  Context.osc_state.yAxisRange.minY = minY;
                  
                  let current_comp = $$("showSelectChartWindowData");
                  current_comp.config.apply()                  
                }

                selectedMinY = -1000; 
                selectedMaxY = 1000;
                
               
              },
            },
          ],
        },
      ],
    },
  };
  return v;
};

export default class ViewDevicesSelectChart extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return <WebixComponent ui={ViewDevicesSelectChartUI()} data={[]} />;
  }
}
