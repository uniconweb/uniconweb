import WebixComponent, { scroll } from "./WebixComponent";
import React, { useEffect, useState } from "react";
import { SysInterfacesEnum } from './data_model/device.mjs'
import { Context } from "./Context";
import { observer } from "mobx-react";

function label(caption, css) {
  return { label: caption, view: "label", css: css };
}

const DeviceInfoCaption = observer(({}) => {
  const [title, setTitle] = useState("");
  
  useEffect(() => {
    let mode = SysInterfacesEnum.toString(Context.model.m_currSysTypeId)

    let title = (mode === "") ? Context.infoCurrentDivice : mode + ": " + Context.infoCurrentDivice

    setTitle(title);

  }, [Context.infoCurrentDivice]);

  return <WebixComponent ui={label(title, "header_label")} data={title} />;
});

export default DeviceInfoCaption;
