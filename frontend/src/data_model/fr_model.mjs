import EventEmitter from 'events'
import cbor from 'cbor-web'

import { Device, SysModule, SysInterfacesEnum } from './device.mjs'

import { DeviceProvider } from "./services/fr_deviceprovider.mjs";
import { ParamProvider } from "./services/fr_paramprovider.mjs";
import {default as RequestHelper} from "./services/fr_requesthelper.mjs";

const STATUS_OK = 200;
const ERROR_RESPONSE = {
    status: 500, // any server error
    msg: ""
}
const CONNECTION_TIMEOUT_MSC = 7000;

export class StatusEnum {
    static Unknown = '';
    static Loading = 'DATA_LOADING';
    static Loaded = 'DATA_LOADED';
    static Inited = "MODEL_INITED";
    static ReInited = "MODEL_REINITED";
    static Changed = 'DATA_CHANGED';
    static Cancelled = 'DATA_CANCELLED';
    static UnChanged = 'DATA_UNCHANGED';
    static Enabled = 'ENABLED';
    static Disabled = 'DISABLED';
}

let checkstatusIntervalId = 0;
let _messageDataLength = 0

const STREAM_SERVER_PORT = 1237;
const RECEIVED_DATA_ERROR = "Received data error!";
const CAPTURED_PARAMS_MAX = 64;

export class Model extends EventEmitter {
    constructor(srvHost) {
        super();

        this.m_name = 'Unicon';
        this.m_currSysTypeId = 0
        this.m_devices = [];
        this.m_subsystems = [];
        this.m_devices_hash = ""; 
        this.m_oscs = [];
        this.m_trends = [];
        this.m_inited = false;
        this.m_enabled = false;
        this.m_firstInited = false;
        this.m_host = srvHost;

        this._capturedParams = [];

        this.deviceProvider = new DeviceProvider(RequestHelper);
        this.paramProvider = new ParamProvider(RequestHelper);
    }

    async init() {
   
        if (this.m_inited) {
            return;
        }

        RequestHelper.initConnection(this.m_host);

        setTimeout(async () => {
            if (!this.m_inited) {
                this.emit('system_status', StatusEnum.Cancelled);
            }
        }, CONNECTION_TIMEOUT_MSC);

        await RequestHelper.waitIsConnected();

        this.m_host = await RequestHelper.resolveSocketIP(this.m_host);
        this.initConnection(this.m_host);
    }

    async initConnection(hostIP) {

        let streamSocketUrl = "ws://" + hostIP + ":" + STREAM_SERVER_PORT;
        this.streamSocket = new WebSocket(streamSocketUrl);

        this.streamSocket.onopen = async (event) => {
            console.log(`Stream socket ${this.streamSocket.url} opened successfully.`);
            try {
                this.m_inited = true;

                if (!this.m_firstInited) {
                    this.m_firstInited = true;
                    this.emit('system_status', StatusEnum.Inited);
                } else {
                    this.emit('system_status', StatusEnum.ReInited);
                }    
            } catch(e) {
                console.error(e);
            }
        };

        this.streamSocket.onerror = (error) => {
            console.log('Stream error: ' + error.message);
        };

        this.streamSocket.onclose = (event) => {
            console.log('Stream closed, reason = ' + event.reason);

            if (this.m_inited) {
                this.disableAllDevices(this.m_currSysTypeId);

                this.emit('system_status', StatusEnum.Cancelled);
            }

            this.m_inited = false;
            this.streamSocket = null;

            setTimeout(async () => {
                await this.initConnection(hostIP);
            }, CONNECTION_TIMEOUT_MSC);

            return;
        };

        this.streamSocket.onmessage = async (message) => {

            var messageData;
            var data = message.data;
            if (data instanceof Blob) {
                var buffer = await data.arrayBuffer();
                messageData = cbor.decode(buffer);
            } else {
                messageData = JSON.parse(message.data);
            }

            if ('error' in messageData && messageData.error != 0) {
                console.warn(`Receiving data error = ${JSON.stringify(messageData.error)}, data = ${JSON.stringify(messageData)}`);
                return;
            }

            if (messageData.type == 'sys') {
                if (messageData.status == 1 && this.m_enabled) {
                    this.updateDeviceLinks();
                }
                if (messageData.status == 2 && this.m_enabled) {
                    console.log("Another client is connected, this client is disabled!"); 
                    this.m_enabled = false;
                    this.disableAllDevices(this.m_currSysTypeId);                    
                    this.emit('system_status', StatusEnum.Disabled);
                }

                return;
            } 
            
            if (!this.m_enabled){
                this.m_enabled = true;
                this.emit('system_status', StatusEnum.Enabled);                
                this.updateDeviceLinks();                
            }

            if (!this.loaded()) {
                console.warn("The model is not loaded on receive message!");
                return;
            }

            if (messageData.type == 'osc') {
                let deviceId = messageData.d_id;
                await this._streamOscValue(deviceId, messageData);
                return;
            }

            if (messageData.type == 'par') {
                let deviceId = messageData.d_id;
                let moduleId = messageData.m_id;
                let paramId = messageData.p_id;
                let value = messageData.val;
                let valueTime = messageData.time;

                let param = this._capturedParams.find((item) => {
                    return item.ID.deviceId === deviceId && item.ID.moduleId === moduleId && item.ID.paramId === paramId
                });
        
                if (!param) {
                    console.warn("Received uncaptured parameter, deviceId = " + deviceId + ", moduleId = " + moduleId + ", paramId = " + paramId);
                    return;
                }

                // console.log("Received parameter, deviceId = " + deviceId + ", moduleId = " + moduleId + ", paramId = " + paramId);

                _messageDataLength = message.data.length;
                await param.streamValue(value, valueTime, _messageDataLength);
            }
            return;
        };

        console.log(`Model inited`);
        this.m_inited = true;
        this.m_enabled = true;
    }

    _captureParam(param) {
        let index = this._capturedParams.indexOf(param)

        if (index === -1) {
            if (this._capturedParams.length > CAPTURED_PARAMS_MAX) {
                console.info("Clearing captured params cache");
                this._capturedParams = [];
            }

            this._capturedParams.push(param);
        }

        return;
    }

    async forceCloseAllStreams(deviceId) {
        if (!this.m_inited) return;
        if (this._capturedParams?.length === 0) return;

        try {
            let ID = {
                sysTypeId : this.m_currSysTypeId,
                deviceId: deviceId,
                moduleId: 0,
                paramId: 0
            }
    
            await this.paramProvider.reqParamValue(ID, "off");
            
        } catch (error) {
            console.log(error);
        }
        
        this._capturedParams.forEach((param) => {
            param.destroyStream();
        });

        this._capturedParams = [];         
    }

    async closeParamStream(param) {
        const index = this._capturedParams.indexOf(param);

        if (index > -1) { 
            this._capturedParams.splice(index, 1);
        }

        await param.closeValueStream();
    }

    openParamStream(param, frequency) {
        if (!param) {
            return;
        }

        this._captureParam(param);
        const stream = param.openValueStream(frequency);
        return stream;
    }

    startParamStreams(frequency) {
        let params = [];
        for (var i = 0; i < this._capturedParams.length; i++) {
            let p = this._capturedParams[i];
            params.push(p.ID)
        }
        
        this.paramProvider.reqParamsValue(this.m_currSysTypeId, params, { frequency: frequency });
    }

    async _streamOscValue(deviceId, valueData) {
        let device = this.device(deviceId);
        if (!device) {
            return;
        }

        let osc = device.getOsc();
        if (osc === undefined || osc === null || valueData == undefined) {
            console.warn("Unable to receive osc stream data. The osc is deactivated now");
            const oscId = deviceId;
            await this.deviceProvider.reqCloseOscStream(this.m_currSysTypeId, deviceId, oscId);
            return;
        }

       osc.streamData(valueData);
    }

    async load() {
        return new Promise(async (resolve, reject) => {
            try {

                if (!this.m_inited) {
                    let err = "The data model is not inited yet. Will be inited now."
                    console.warn(err);
                    this.init();
                    //                  reject({ status: 500, msg: err });
                }

                this.emit('system_status', StatusEnum.Loading);                

                this.clear();

                let subSystems = [];

                await RequestHelper.waitIsConnected();
                let isDemoMode = await RequestHelper.isDemoMode();
                this.m_currSysTypeId = !isDemoMode ? SysInterfacesEnum.UAVCAN : SysInterfacesEnum.DEMO;

                console.log("Current system type: " + SysInterfacesEnum.toString(this.m_currSysTypeId))

                let devices = await this.deviceProvider.reqDevices(this.m_currSysTypeId);

                for (var i = 0; i < devices.length; i++) {

                    let deviceInfo = devices[i];
                    let device = new Device();
                    await device.load(deviceInfo);
                    if (!device.isValid()) return null;
                    device.enabled = true;
            
                    if (!subSystems.includes(device.sysTypeId)) {
                        subSystems.push(device.sysTypeId);
                        await this.deviceProvider.reqSystemInit(device.sysTypeId);
                    }

                    this.m_devices.push(device)
                }

                this.m_subsystems = subSystems;

                console.log("loaded devices  = " + this.m_devices.length + " for system type = " + this.m_currSysTypeId);
                this.emit('system_status', StatusEnum.Loaded);

//              this.enablePeriodicCheck();
                setTimeout(async () => { await this.updateDeviceLinks(); }, 7000);
                resolve({ result: 'true', status: 200 });

            } catch (err) {
                console.log(err);
//              this.disablePeriodicCheck();
                this.emit('system_status', StatusEnum.Cancelled);
                reject(err);
            }
        });
    }

    async createDevice(sysTypeId, deviceId) 
    {
        let deviceInfoList = await this.deviceProvider.reqDevice(sysTypeId, deviceId);
        let deviceInfo = deviceInfoList[0];
        if (!deviceInfo) return null;

        let device = new Device();
        await device.load(deviceInfo);
        if (!device.isValid()) return null;

        device.enabled = true;
        return device;
    }

    loaded() {
        return this.m_devices.length > 0;
    }

    disableAllDevices(sysTypeId) {
        for (var i = 0; i < this.m_devices.length; i++) {
            let device = this.m_devices[i];
            if (device.sysTypeId === sysTypeId) {
                device.enabled = false;
            }
        }
    }

    async updateDeviceLinks() {
            let links = [];
            let currDeviceListId = [];
            let dataChanged = false;

            try {
                links = await this.deviceProvider.reqLinks(this.m_currSysTypeId);
            } catch (e) {
                console.log(e);
            }

            for (var i = 0; i < this.m_devices.length; i++) {

                let device = this.m_devices[i];
                if (links.includes(device.deviceId)) {
                    if (!device.enabled) {

                        device.enabled = true;
                        dataChanged = true;
                        // Here we should compare the device structure to a new device structure, because structure could be changed
                        // let device = await this.createDevice(this.m_currSysTypeId, links[i]);
                        // if (device) {
                        //    this.m_devices[i] = device; // new device is reconstructured     
                        //    device.enabled = true;                           
                        //    dataChanged = true;
                        // }
                        
                    }
                } else {
                    if (device.enabled) {
                        device.enabled = false;
                        dataChanged = true;
                    }
                }
                
                currDeviceListId.push(device.deviceId);
            }

            for (var i = 0; i < links.length; i++) {
                if (!currDeviceListId.includes(links[i])) {
                    let device = await this.createDevice(this.m_currSysTypeId, links[i]);
                    if (device) {
                        this.m_devices.push(device);
                        dataChanged = true;
                    }
                }
            }

            if (dataChanged) {
                console.log("device links changed" + ", links = " + links.length); 
                this.emit('system_status', StatusEnum.Changed);
            }
    }

/*    
    enablePeriodicCheck() {
        if (checkstatusIntervalId > 0) {
            return;
        }

        checkstatusIntervalId = setInterval(async () => {
            await this.updateDeviceLinks();

            let res = await this.deviceProvider.reqStatus(this.m_currSysTypeId);

            if (res.system_status != StatusEnum.Unknown) {
                this.emit('system_status', res.system_status);
            }


        }, 3000)
    }
*/
    disablePeriodicCheck() {
        if (checkstatusIntervalId === 0) {
            return;
        }

        clearInterval(checkstatusIntervalId);
        checkstatusIntervalId = 0;
    }

    clear() {
        this.disablePeriodicCheck();

        this.m_devices = [];
        this.m_trends = [];
        this._capturedParams = [];
    }

    device(id) {
        let result = this.m_devices.find(item => item.deviceId  === id);
        return result;
    }

    devices(sysInterface) {
        if (sysInterface == undefined) {
            return this.m_devices;
        }

        let result = this.m_devices.filter(item => item.interfaceName === sysInterface);
        return result;
    }

    sysInterfaces() {
        let result = this.m_devices.reduce((res, current) => {
            if (!(current.interfaceName in res)) {
                res.push(current.interfaceName);
            }
        }, []);

        return result;
    }
}

/*
module.exports = {
    Model,
    SysInterfacesEnum
};
*/