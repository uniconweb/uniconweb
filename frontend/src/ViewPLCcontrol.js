import React, { useRef, createRef, useState, useEffect } from "react";
import { observer } from "mobx-react";

import 'webix/webix.css';
import WebixComponent from './WebixComponent';
import { $$ } from "@xbs/webix-pro";
import * as webix from "@xbs/webix-pro";
import {svgs} from "./helpers/svgs";
import { Context } from './Context';
import { PLC_ConfigHelper } from "./data_model/plc_config.js";
import { Param, ValueFormatEnum } from "./data_model/param.mjs";
//import  {selectParameter, getSelectedParam}  from "./DeviceParamSelect";
import  {DeviceProperties, ColorEnum} from "./device_control/DeviceControlProperties";
import * as controller from "./device_control/DeviceControlElements";
import * as background from "./device_control/DeviceControlBackground"
import { usePLC } from "./PLCContext";

const PREFIX_ID = "plc_"
const TAB_NAME = "PLCControlTab"; // The name of the page tab
const CONFIG_KEY = "PLC_HMI";

let actions = {};

function importPLCFunctions(plcFunctions) {
  actions = plcFunctions;
}

let _editModeEnabled = false;
var _configHelper = new PLC_ConfigHelper();
let _currentElement = undefined;
let _elemNum = 0;
let dev_props = new DeviceProperties();

function ID(baseId) {
  return PREFIX_ID  + baseId;
}

function onSelectedParam(param) {
  controller.assignParam(_currentElement.config.id, param);
  dev_props.setCurrentElement(_currentElement.config.id, controller.updateElementPropertyValue);
  actions.addParameter(param);
}

function clear() {
  let form = $$(ID("controlLayoutForm"));
  if (!form) return;

  let elements = form.getChildViews();
  let len = elements.length;
  for (let i = 0; i < len; i++) {
    let elem = elements[0];
    form.removeView(elem);
  }

  dev_props.clearPropertiesForm();
}

async function loadConfig(configKey) {
  await _configHelper.load(configKey);

  let confObj = _configHelper.get(configKey);

  clear();
  parseConfig(confObj);
}

async function restoreConfig(configKey) {

  if (!configKey) return;

  let confObj = _configHelper.get(configKey);
  if (!confObj) {
    await _configHelper.load(configKey);
    confObj = _configHelper.get(configKey);    
  }

  if (!confObj) {
    console.error("The control configuration is not found!")
    return;
  }

  clear();
  parseConfig(confObj);
}

function parseConfig(confObj) {
  let layoutForm = $$(ID("controlLayoutForm"));

  controller.parseConfig(confObj, layoutForm);

  if (confObj && "background" in confObj) {
    background.loadBackground($$(ID("devicesDesignLayout")), confObj["background"]);
  }

  // layoutForm.focus()  
}

async function saveConfig(deviceKey, doPersistentSave) {
  let form = $$(ID("controlLayoutForm"));
  let elements = form.getChildViews();

  let confData = controller.collectConfigData(elements);
  
  let confObj = {};
  confObj["elements"] = confData;
  confObj["background"] = $$(ID("devicesDesignLayout")).config["background"];

  _configHelper.set(deviceKey, confObj);
  if (doPersistentSave) {
    await _configHelper.save(deviceKey);
    webix.message({type:"success", text:"The Control configuration is saved", });
  }
}

function deleteCurrentElem() {
  if (!_currentElement) return;
  
  let elem = _currentElement;
  let form = $$(ID("controlLayoutForm"));
  
  form.removeView(elem);

  let elements = form.getChildViews();
  let lastElem = elements.at(-1)
  if (lastElem) {
    setCurrentElement(lastElem.config.id);
  } else {
    dev_props.clearPropertiesForm();
  }
}

function setCurrentElement(id) {
  dev_props.setCurrentElement(id, controller.updateElementPropertyValue);

  selectElement(id);
}

function selectElement(id) {
  let elements = $$(ID("controlLayoutForm")).getChildViews();
  elements.forEach(element => {
    if (element.config.id !== id) {
      webix.html.removeCss(element.$view, "webix_formbuilder_selected");
    }
  });

  if (id) {
    webix.html.addCss($$(id).$view, "webix_formbuilder_selected", true); 
  }

  _currentElement = $$(id); 
}

function draggedElemName(draggedControlId) {
  switch (draggedControlId) {
    case ID("dragLabel"):
      return "label";
    case ID("dragText"):
      return "text";
    case ID("dragSlider"):
      return "slider";
    case ID("dragButton"):
      return "button";
    case ID("dragSwitcher"):
      return "switch";
    case ID("dragCombo"):
      return "combo";
    default:
      return "";
  }
}


function openParamStreams() {
  let elements = $$(ID("controlLayoutForm")).getChildViews();
  let params = [];
  elements.forEach(element => {
    if (element.config["streamable"] === true) {
      let param =  element.config.modelData;
      if (param && params.indexOf(param) === -1) {
        params.push(param);
      }
    } 
  });

  controller.receiveParams(params, elements, 4);
}

async function closeParamStreams() {
  /*  
    let elements = $$(ID("controlLayoutForm")).getChildViews();
    elements.forEach(async (element) =>  {
      let param =  element.config.modelData;
      if (param && param.isStreamming()) {
        await Context.model.closeParamStream(param);
      }
    });
   */
    await controller.closeParamStreams();
  
}
  
function setDragControl(enabled) {
  // webix.DragControl.addDrop("trashButton",{
  //   $drop:function(source, target, e){
  //     console.log("source",source,target);
  //   }});
    if (!enabled) {
    webix.DragControl.addDrop(ID("controlLayout"),{
      $drop:function(source, target, e){
        // console.log("unlink");
      }});
      return;
  }

  webix.DragControl.addDrop(ID("controlLayout"), {
    $drop:function(source, target, e) {
        const box = webix.html.offset( webix.$$(ID("controlLayoutForm"))._viewobj);
        const view = webix.$$(e);

        var pos = webix.html.pos(e);

        let elemName = draggedElemName(source.id);
        if (elemName === "") return;

        _elemNum++;
        let newElem = controller.createElement(elemName, pos.y-box.y, pos.x-box.x, 200, _elemNum)
        let elemId = controller.addElementToView(view, newElem)
        if (!$$(elemId)) return;
        
        attachElementEvents(elemId, _editModeEnabled)

        setTimeout(() => {
          setCurrentElement(elemId);
        }, 100);        
    }
  });

  webix.DragControl.addDrag(ID("dragText"));
  webix.DragControl.addDrag(ID("dragLabel"));
  webix.DragControl.addDrag(ID("dragSlider"));
  webix.DragControl.addDrag(ID("dragButton"));
  webix.DragControl.addDrag(ID("dragSwitcher"));
  webix.DragControl.addDrag(ID("dragCombo"));
  
  const view = $$(ID("controlLayoutForm"));
  controller.addDragControl(view);
}

function enableEditMode() {
  hideShow(true);
  setDragControl(true);
  _editModeEnabled = true;

  closeParamStreams();

  const view = $$(ID("controlLayoutForm"));

  dev_props.show();
  dev_props.clearPropertiesForm();
  webix.html.removeCss( view.$view, "webix_formbuilder_form_run");
  webix.html.addCss( view.$view, "webix_formbuilder_form"); 
//view.define("css", "webix_formbuilder_form");

  attachEvents(true);
 
}

function hideShow(flagShow) {
  

  $$(ID("mainToolbar"))._collection[0].cols.map((x) => {
    // console.log("x.id",x);
    if ((!(x.value === "Run" || x.value === "Edit")) && ((x.view ==="button")|| (x.view ==="icon") || (x.view ==="template"))) {
      if (flagShow) {
        $$(x.id).show();
      } else {
        $$(x.id).hide();
      }
    }
    
  });
}

function enableRunMode() {
  hideShow(false);
  setDragControl(false);
  _editModeEnabled = false;
   
  setTimeout(() => {
    openParamStreams()
  }, 10);

  dev_props.clearPropertiesForm();
  dev_props.hide();

  const view = $$(ID("controlLayoutForm"));
// webix.html.removeCss( view.$view, "webix_formbuilder_form"); // We cannot remove style becuase it is used in rows
  webix.html.addCss( view.$view, "webix_formbuilder_form_run"); 
//  view.define("css", "webix_formbuilder_form_run");
  
  attachEvents(false)
}

function attachEvents(isDragMode) {
  let elements = $$(ID("controlLayoutForm")).getChildViews();
  elements.forEach(element => {
    attachElementEvents(element.config.id, isDragMode)
  });

  if (isDragMode) {
    webix.attachEvent("onFocusChange", function(current_view, prev_view) {
      
      let elements = $$(ID("controlLayoutForm")).getChildViews();
      elements.forEach(element => {
        if (element.config.id !== current_view.config.id) {
          webix.html.removeCss(element.$view, "webix_formbuilder_selected");
        }
      });
    
      if (current_view === $$(ID("controlLayoutForm"))) {
        dev_props.clearPropertiesForm();
      }

      let current_top = current_view?.getTopParentView();
      let prev_top = prev_view?.getTopParentView();

      if (current_top !== prev_top && prev_top.config.id === "selectParam") {
        $$("selectParam").close();
      }

    });
  } else {
    webix.detachEvent("onFocusChange");
  }
}

function attachElementEvents(elementId, isEditMode) {
  let element = $$(elementId);

  if (isEditMode) {
    element.detachEvent("onItemClick");
    element.detachEvent("onChange");
    element.detachEvent("onSliderDrag");
  } else { // run mode
    element.detachEvent("onFocus");
    element.detachEvent("onItemClick");
    element.detachEvent("onSliderDrag");
    element.detachEvent("onChange");
  }

  if (isEditMode) {
    element.attachEvent("onFocus", function(current_view, prev_view) {
      setCurrentElement(current_view.config.id);
    })
  } 
  
  if (!isEditMode) {
    if (element.data["view"] === "button") {
      element.attachEvent("onItemClick", function() {controller.onButtonClick(element)})
    }
  
    if (element.data["view"] === "slider") {
      element.attachEvent("onSliderDrag",  function() {
        let newValue = element.getValue(); 
        controller.onSliderChange(element, newValue)})
    }

    element.attachEvent("onChange", function(newValue, oldValue, config) {
      controller.onChangeValue(element, newValue, oldValue)})
  }
}

function DesignLayout(params) {
  return {
    type:"space", 
    // minWidth: 822,
    // width: 822,
    // height: 600,
    // css:"webix_formbuilder_form",
    css:"scroll_form",
    id:ID("devicesDesignLayout"),
      
    rows:[
      {
        view:"abslayout",
        drag:true,
        css:"webix_formbuilder_form",
        // left : 1350,
        // width: 622,
        // height: 400,
        // resize: true,
        id:ID("controlLayoutForm"), 
        cells:[
          
          ],
      }
    ]
  }
}

function designMain(params) {
  return {
    view:"toolbar", 
    id: ID("mainToolbar"),
    type:"space", 
    // "height": 850,
    drag:true,
    rows:[
        {cols:[
          { view:"template", css:"marging_right_10", width: 120, 
            template: `<div id=${ID("dragLabel")} >`+`<span class="drag_icons"> <img src="${svgs["label"]}"/> </span>`+'<div class="drag_icons_text">Label</div></div>'
          },
          { view:"template", css:"marging_right_10", width: 120, 
          template: `<div id=${ID("dragButton")} >`+`<span class="drag_icons"> <img src="${svgs["button"]}"/> </span>`+'<div class="drag_icons_text">Button</div></div>'
          },
          { view:"template", css:"marging_right_10", width: 120, 
          template: `<div id=${ID("dragSwitcher")} >`+`<span class="drag_icons"> <img src="${svgs["switch"]}"/> </span>`+'<div class="drag_icons_text">Bit value</div></div>'
          },
          { view:"template", css:"marging_right_10", width: 120, 
          template: `<div id=${ID("dragCombo")} >`+`<span class="drag_icons"> <img src="${svgs["combo"]}"/> </span>`+'<div class="drag_icons_text">Combo</div></div>'
          },
          { view:"template", css:"marging_right_10", width: 120, 
          template: `<div id=${ID("dragSlider")} >`+`<span class="drag_icons"> <img src="${svgs["slider"]}"/> </span>`+'<div class="drag_icons_text">Slider</div></div>'
          },
          { view:"template", css:"marging_right_10", width: 120, 
          template: `<div id=${ID("dragText")} >` + `<span class="drag_icons"> <img src="${svgs["text"]}"/> </span>`+'<div class="drag_icons_text">Value</div></div>'
          },
          { view:"icon", id:ID("trashButton"),  icon: "wxi-trash", hotkey: "ctrl+delete", click: (e) => {
            deleteCurrentElem();
          } },
         
          { view:"button",width: 150, value:"Set background",click: (e) => {
            background.showLoadBackground($$(ID("devicesDesignLayout")), CONFIG_KEY);
          }},
          {},
          { view:"button",  id: ID("EditBtn"), width: 100, value:"Edit",click: (e) => {
            enableEditMode(true);
          }},
          { view:"button",id: ID("RunBtn"),  width: 100, value:"Run", css:"marging_right_20", click: (e) => {
//          saveConfig(Context.currentDevice.name, true);            
            enableRunMode(false);
          }},
          { view:"button", width: 100, value:"Save",click: (e) => {
            saveConfig(CONFIG_KEY, true);
          }},
          { view:"button", width: 100, value:"Load", css:"marging_right_20", click: (e) => {
              webix.confirm({title: "Loading the configuration", text:"All changes will be erased"}).then(function(result){
                loadConfig(CONFIG_KEY);
            }).fail(function(){
            });            
            
          }},
          // { view:"button", width: 100, value:"Delete",click: (e) => {
          //   deleteCurrentElem();
          // }},
          
          {}
        ]},
        
    ]
  }
}


function ControlLayout(params) {
    return {
      css: "devicesControlFrameTab",
      // height: 850,
      cols: [
        DesignLayout(),
        dev_props.designProperties({prefix_ID: params.prefix_ID}, onSelectedParam),
      ]
    }
}

function usePrevious(value) {
  const ref = useRef();
  useEffect(() => {
    ref.current = value; //assign the value of ref to the argument
  },[value]); //this code will run when the value of 'value' changes
  return ref.current; //in the end, return the current ref value.
}

const ViewPLCcontrol = observer((props) => {
  const plcFunctions = usePLC();
  useEffect(() => {
    _editModeEnabled = false;
    
    webix.ui({
      view:"contextmenu",
      id:ID("cmenu"),
      master: TAB_NAME,
      data:["Delete",{ $template:"Separator" }],
      on:{
        onItemClick:function(id){
          deleteCurrentElem()
          // var context = this.getContext();
          // var list = context.obj;
          // var listId = context.id;
          // webix.message("List item: <i>"+list.getItem(listId).title+"</i> <br/>Context menu item: <i>"+this.getItem(id).value+"</i>");
        }
      }
    });

//  restoreConfig(CONFIG_KEY);

    if (_editModeEnabled) {
      enableEditMode();
    } else {
      enableRunMode();
    }


  },[]);

  useEffect(() => {
    importPLCFunctions(plcFunctions);
    const isCurrCntrlTab = (Context.states.currTabViewId === TAB_NAME)

    if (isCurrCntrlTab) {
/*      
      setTimeout(() => {
        updateElemValues()
      }, 100);
*/
      restoreConfig(CONFIG_KEY);

      if (_editModeEnabled) {
        enableEditMode();
      } else {
        enableRunMode();
      }
      webix.html.denySelect();

      
    } else {
      closeParamStreams();
      webix.html.allowSelect();
    }
  }, [Context.states.currTabViewId]);


  return (
    <div id={props.id}>
      <WebixComponent   ui={designMain(props)} />
      <div id={ID("controlLayout")}>
        <WebixComponent   ui={ControlLayout({prefix_ID: PREFIX_ID})} />
      </div>
    </div>
    
  );
});

export default ViewPLCcontrol;