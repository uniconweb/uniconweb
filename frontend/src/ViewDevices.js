import "webix/webix.css";
import WebixComponent, { scroll, resizeWebixComponent } from "./WebixComponent";
import { $$ } from "@xbs/webix-pro";

import * as webix from "@xbs/webix-pro/webix.js";
import React, { useEffect } from "react";

import ViewDevicesInfo from "./ViewDevicesInfo";
import ViewDevicesControl from "./ViewDevicesControl";
import ViewDevicesParameters from "./ViewDevicesParameters";
import ViewDevicesOscilloscope from "./ViewDevicesOscilloscope";
import { observer } from "mobx-react";
import { Context, setVisibleElements, setStyleByID } from "./Context";
import {PLCProvider} from "./PLCContext";

const cellsArr = [
  "devicesParametersTab",
  "devicesControlTab",
  "devicesОscilloscopeTab",
  "devicesInfoTab",
];

export function setCurrentTab(id) {
  setVisibleElements([id], cellsArr);
  console.log("setCurrentTab",id);
  resizeWebixComponent();
  setTimeout(() => {
    Context.states.currTabViewId = id;
  });
}

export function restoreCurrentTab() {
  let currentTab = $$("tabViewControl").getTabbar().getValue();
  if (currentTab) {
    setVisibleElements([currentTab], cellsArr);
    resizeWebixComponent();
    Context.states.currTabViewId = currentTab;    
  }
}

function tabViewControl() {
  return {
    view: "tabview",
    id: "tabViewControl",
    // type:{
    //   height:"auto"
    // },
    cells: [
      {
        id: "settings",
        header: "Settings",
        select: true,
        body: {
          id: "devicesParametersTab",
          select: true,
        },
      },
      {
        id: "control",
        header: "Control",
        body: {
          id: "devicesControlTab",
        },
      },      
      {
        id: "oscilloscope",
        header: "HiResScope",
        body: {
          id: "devicesОscilloscopeTab",
        },
      },
      {
        id: "info",
        header: "Info",
        body: {
          id: "devicesInfoTab",
        },
      },
    ],
    tabbar: {
      // id: "tabbarParam",
      on: {
        onAfterTabClick: function (id, ev) {
          setCurrentTab(id);
        },
      },
    },
  };
}

const TabViewObserver = observer(({}) => {
  useEffect(() => {
    console.log("Render TabViewObserver");
    // console.log(Context.states.indexDevice);
    setVisibleElements(["devicesParametersTab"], cellsArr);
    $$("tabViewControl").getTabbar().setValue("devicesParametersTab");
  });
  return <WebixComponent ui={tabViewControl()} />;
});

export default class ViewDevices extends React.Component {
  constructor(props) {
    super(props);
    this.title = "first title";
    this.state = { title: "state title", dt: [], deviceId: 0 };
    this.updateDevices = props.updateDevices;
  }

  render() {
    return (
      <div>
        <TabViewObserver />
        {/* <WebixComponent ui={tabViewControl()} /> */}
        <ViewDevicesInfo id="devicesInfoTab" height="90vh" />
        <ViewDevicesControl id="devicesControlTab" />
          <ViewDevicesParameters id="devicesParametersTab" />
        <ViewDevicesOscilloscope id="devicesОscilloscopeTab" />
      </div>
    );
  }
}
