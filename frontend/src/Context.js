// import {$$} from 'webix';
import { $$ } from "@xbs/webix-pro";
import { makeObservable, observable, action, toJS } from "mobx";
import { PLC_Tabs } from "./ViewPLC";

export const setStyleByID = (id, cssproperties, val) => {
  let element = document.getElementById(id);
  if (element) {
    element.style[cssproperties] = val;
  }
};
export const getStyleByID = (id, cssproperties) => {
  let element = document.getElementById(id);
  if (element) {
    return element.style[cssproperties];
  }

  return "";
};

export const removeCssClass = (id, cssStyle) => {
  let element = document.getElementById(id);
  if (element) {
    element.classList.remove(cssStyle);
    // element.classList.add(cssStyle);
  }
};

export const addCssClass = (id, cssStyle) => {
  // console.log("addCssClass "+id+ " "+cssStyle);
  let element = document.getElementById(id);
  if (element) {
    console.log("addCssClass " + id + " " + cssStyle + " ok");
    element.classList.add(cssStyle);
  }
};

export function setVisibleElements(
  visibleElementIDArr,
  hiddenElementsArr = [],
) {
  // console.log("visibleElementIDArr",visibleElementIDArr);
    hiddenElementsArr.forEach((id) => {
      setNonVisibleAndHidden(id);
    });
    visibleElementIDArr.forEach((id) => {
      setVisible(id);
    });
  }


export function setNonVisibleAndHidden(id) {
  setStyleByID(id, "display", "none");
  setStyleByID(id, "visibility", "hidden");
}

export function setVisible(id) {
  setStyleByID(id, "display", "block");
  setStyleByID(id, "visibility", "visible");
}

export function showPageAndHiddenElements(page) {
  setNonVisibleAndHidden("ViewPLC");
  // setNonVisibleAndHidden("ViewDevices");
  setNonVisibleAndHidden("ViewGraphicTrends");
  setNonVisibleAndHidden("ViewCPlotWeb");
  setNonVisibleAndHidden("devicesInfoTab");
}

export const Context = {
    chartkey:
  //"u8YE2iWTHGXbOQ3imgrRiSCMGJZOtRPpkUxA1s7O7p8D99g8NEf+HZb0D/w4dLIF6Z0lvC5tzXCPXdijXbZxNjSgMp8Kq31lqWs37sFfvFkeD7Li/2m7tf4EPWI1+5LMdwKOCJ85747ofPaob/hyP/e8mLBJiL7knb6M6BX3qXZoIYREtXpdZZg5SMwUSKe1JARnDX9fgaLKyq6yivM2y9FPj8wvQXD7D7zz8T6Oe3E/OrlpyVJiChb8V5sca6y0VcQay8eI75LbZvdeyZXPw4AkbIflX5Ha1MdwE4BRcR40rNESBmin/U2+nWmX3AWWkeLUB+rOmpkf6WpNmJA2Oibq8uumazX4Irwu+BKsgNs8pBk2SrhFOTnh8MNMdCyck/CJb0TgrwG5mjv3ev7BJ7tDqGr7xixo6xeEQSm7wOx+1AWw+vGc7cPUv5+ZOzyI6ZZPmvvaWaWzjIUHR1k7op0MTWr6NAwlCX5gK0yhlyAt4Rt7hn7USnY8Fm/6E5k3067qSJFkuwTOzIC6/8dM48Afkn+Ixw8VqY5/S8ivBOUmS+a7qZHaUDBy6Ld1QXfxs0xrU0hmDMC67U8G/gck5XtohJfPFNLzK3gkLTNO+D55TGsjl9CNMMF6qoazgd7HwqCx5afNZzDgx/uaH/r2Fbp5XLamjUPQALjPejQTNn+TH8ge",
 // "WkmBfkCtsjVG2/hWF6NYdB58/eFtzIPd/TapVv02ZaTLSR5ABc12uEGadcBpxsl6Eqk72kav/imT+7UGh8Xe9yCvZ4CTsuyaVZxUgpJLY9Two3mGQGOvwUAhiE8/5XuMi6u1tLsusQpr9Rcb3U9IjzPLVeaItgLS+acVMei8vXAqacoHNepWZRaGvvaNtL6pGQRZ6aGiBqrS2mO9/My49Ba0NZNza++05ITAKlhqvLIn6njsv/LtvcBcf1BhjUMt9dMaVRczZlvMDYVDowr8utJYClOVs6l9TNik354275ONWQhqYC5RsjSLExucrr93MWQCy9urmpmfh149NHL7NdXUreZlaFh9hZkbiFV/iNHUfnH03chLWfHxvw/d5qmJ77LPZaxIckhTlfR1zq6omcKsCRp5BsiZv8zFyf0nX9llm9MxttT9YX2VawrLHzif6FhsumcZBZ0+1voO0MJLXGYWIR6gIgznnq4LDQODhzsZZrK1lEEbUM29DkPjruneNgyKQkzaCm+/CnRynWqtt031Uw3/KCCDb/BLrql5VlYYBTY3N0vRYRekuMulXeJl",
  "PcDRCF32nN1Lxf3yinT5HdA88vkvFR99h63kYZi3KQTQPyWRY4bMk4MBc9S0y8/9M9zmMZpj5x1+u+RGO4vpozgcTbdxOrHI3vbLTmhTndvDldXVQ559wmMrG7Kw08GDbMkIG5wsRSP8UhiWjhbo6xmFCdbkShjQY8GPQJWf7AHEstYfNXjbr7gmpKboLE+FjjsVNdTdGxjsmbRguNhFG3hog8/ooNKzHOgEZUqZAQ+vVMaLZlOPIyu6HH+/Q2ftOj5ml/n4iAchIWMleRcgz4ELEzELcBk8aptOwS8T4kSsq0CxAbWCHZecXQlmejVFrbq/PF4727KxG9/yAbRSIy/vFNhsGgyfQNW3cWErD4pJI0IYd61eJ/skt1D+3KTK4cL0J6UEh0ayCNSgVhlXYCYNjndqWMlBYHR1bzCaDQHZvz4kmkIUwS3iXF0M+NDWDe/HwW8IDtNNzM4y3EU2Iu8utyL4WGOSWcRouNHfGGaMsCHaJYOM+pEoXFKN4dgZ+k1KyZrCe/jVdyP5cV79B+5HYqharohoXDHFhNawW0qBzV0bwSNel1AqJr8dCG6FX7lPPENOvu8S+NV+iw5PQ0mPy0re/+/Oe4NqDEE6zAFLafXcknIEWC9zSYAYnqyTM1Yuvg+YnJA8dbT1dgQqAxUzDrTXX5FMeguPLUMIQ4Lu",
    model: {
      m_devices_hash: "",
      m_devices: [],
      device(deviceId) {
        return this.m_devices.find(device => device.deviceId === deviceId);
      }
    },
  status: "Loading...",
  updateModel: 0,
  count: 0,
  title: "t1",
  elements: {},
  devices: [],
  states: { indexDevice: -100, currTabViewId: "", activeDeviceId: null },
  osc_state: { trig_time:0, trig_reason:0, display_resolution_ms:10000, yAxisRange: { minY: -1000, maxY: 1000 }, step: null},
  isHalted: false,
  isHistory: false,  
  infoCurrentDivice: "", //Название текущего девайса
  currentDevice: {},
  chartActions: [],

  oscilloscopeChartList: ["chart1", "chart2", "chart3"], // List of available charts for oscilloscoping
  paramToCharts: new Map(), // { deviceId: {"chart1":{}, "chart2":{}, "chart3":{}} }
  chartsLength: 0, // TODO: Need to fix. It brings to errors when add/remove charts
  deviceChartList: new Map(),
    /*[ // лист для создания списка графиков по устройствам
        {deviceId:0, charts:["chart0"]}
    ], */
  chartStates: new Map(),// для хранения состояния масштаба



  getChartFromPool: function () {
    let visibleCharts = this.currDeviceCharts();

    let difference = this.oscilloscopeChartList.filter(
      (x) => !visibleCharts.includes(x)
    );
    let chart = difference.shift();
    return chart;
  },

  setStatus: function (title) {
    this.status = title;
    console.log("setStatus=" + title);
  },

  setDevices: function (deviceArr) {
    this.devices = deviceArr;
  },

  setCurrentDivice: function (deviceItem) {

    if (!deviceItem) {
      this.infoCurrentDivice = "No selected devices";
      this.states.indexDevice = -1;
      this.currentDevice = null;
      this.states.activeDeviceId = null;
      return;
    }

    let deviceInd = -1;
    this.devices.forEach((element, index) => {
      if (element.deviceId == deviceItem?.deviceId) {
        deviceInd = index;
      }
    });

    let desc = deviceItem.displayName(); //deviceItem.desc !== "" ? deviceItem.desc : deviceItem.displayName();
    this.infoCurrentDivice = desc// + ", Channel: " + deviceItem.interfaceName;
    this.states.indexDevice = deviceInd;
    this.currentDevice = deviceItem;
    this.states.activeDeviceId = deviceItem;
  },
  

  getDeviceByInd: function (ind) {
  let deviceArray = this.devices;
  
  let deviceId = deviceArray.length > ind && ind >= 0 ? deviceArray[ind].deviceId : 0;
  return this.model.device(deviceId);

},
  

  deviceCharts: function (deviceInd) {
    let res = [];
    if (deviceInd === undefined) {
      return res;
    }

    if (this.deviceChartList.has(deviceInd)) {
      res = this.deviceChartList.get(deviceInd);
    }

    return res;
  },

  currDeviceCharts: function () {
    return Context.deviceCharts(Context.states.indexDevice);
  },

  chartParams: function (chart) {
    let deviceInd = this.states.indexDevice;
    let params = this.paramToCharts.get(deviceInd);

    let res = params?.get(chart);
    return res;
  },

  saveZoomState: function (chartID, state) {
    this.chartStates.set(chartID, state);
  },

  getZoomState: function (chartID) {
    return this.chartStates.get(chartID);
  },


  addChart: function (deviceIndex, chartId) {
    if (!this.deviceChartList.has(deviceIndex)) {
      this.deviceChartList.set(deviceIndex, []);
    }

    let charts = this.deviceChartList.get(deviceIndex);
    if (chartId) {
      charts.push(chartId);
    }
    this.chartsLength = charts.length;
  },

  popChart: function (deviceIndex) {
    let charts = [];
    if (this.deviceChartList.has(deviceIndex)) {
      charts = this.deviceChartList.get(deviceIndex);
    }

    if (charts.length === 0) return;

    charts.pop();
    this.chartsLength = charts.length;
  },

  clearCharts: function (deviceIndex) {
    if (this.deviceChartList.has(deviceIndex)) {
      this.deviceChartList.set(deviceIndex, []);
    }
    this.chartsLength = 0;
  },

  removeChart: function(deviceIndex, chartId) {
    if (this.deviceChartList.has(deviceIndex)) {
        let charts = this.deviceChartList.get(deviceIndex);
        const chartIndex = charts.indexOf(chartId);
        if (chartIndex !== -1) {
            charts.splice(chartIndex, 1); 
            this.deviceChartList.set(deviceIndex, charts); 
            //console.log(`Chart ${chartId} deleted. New length - ${charts.length}`);
        }
    }
   
    let totalCharts = 0;
    for (let charts of this.deviceChartList.values()) {
        totalCharts += charts.length;
    }
    this.chartsLength = totalCharts; 
    //console.log("Count charts updated- " + this.chartsLength);
},
  isPLCMode: function() {
    return PLC_Tabs.includes(this.states.currTabViewId);
  },

  resize: function (event) {
    console.log("resize");
    //let elements = ["$scrollview1"];
    let elements = [
      "accmain",
      "scrollacc",
      "tabViewControl",
      "PLCtabViewControl",
      "devicesParametersTab",
      "PLCDevicesParametersTab"
    ];

    elements.forEach(function (item, index, array) {
      //if(item == "controlview") $$(item).resize();
      if ($$(item)) {
        if ($$(item)["adjust"]) {
          $$(item).adjust();
        }
      }
    });
  },
  showPages: function (visibeElements) {
    setVisibleElements(visibeElements, this.pages);
    document.documentElement.style.setProperty("--chartcount", 1);
    // Этот код будет полезен для стека графиков
    // this.pages.forEach(id => {
    //     setStyleByID(id,"display","none");
    //   });
    //   visibeElements.forEach(id => {
    //     document.documentElement.style.setProperty('--chartcount', 1);
    //     setStyleByID(id,"display","");
    //     setStyleByID(id,"visibility","visible");
    //   });
  },
};
