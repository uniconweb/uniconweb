const serverApiRoutes = require('./services/api/serverApi.js');
const express = require("express");
const addConfigStoreLoadAPI = require("./services/api/configStoreLoad");
const PORT = process.env.PORT || 3000;
const path = require("path");
const app = express();
const fetch = require("node-fetch");
const Busboy = require("busboy");
const fs = require("fs");

const scichartLicURL = 'http://localhost:5000' //'http://10.9.0.1:5000'
let isDemoMode = false;

app.listen(PORT, () => {
  console.log(`Server listening on ${PORT}`);
});

app.get("/api/server/demo", async (req, res) => {
  let ans = {isDemoMode};
  isDemoMode = false; // do not need it anymore.
  return res.status(200).send(ans);
});

app.use("/api/server", serverApiRoutes);

// Have Node serve the files for our built React app
app.use(express.static(path.resolve(__dirname, "../frontend/build")));

// **************************************************************
// *** CONFIG STORE/LOAD API
// **************************************************************
const CONFIG_STORE_LOAD_API_PATH = "/api/config";
addConfigStoreLoadAPI(app, CONFIG_STORE_LOAD_API_PATH);

// Scichart license query redirection to dot net core license server
app.get("/api/scichart", async (req, res) => {
  
  let ans = '';
  try {
  
   let url = scichartLicURL + req.originalUrl;
    console.log("fetch license from:" + url);

    let response =  await fetch(url);
    ans = await response.text();
  }
  catch (e) {
    ans = 'no valid license'
  }

  console.log("answer:" + ans);
  return res.status(200).send(ans);
});

// todo move to BackgroundLoadAPI -------------------------------------------------------
const STORAGE_DIR = process.env.STORAGE_DIR ?? "../storage";
app.get("/background/load", function(req, res) {
  const saveFileName = "background_" + req.query.key
  console.log("read file from " + path.join(__dirname, STORAGE_DIR, saveFileName));
  fs.readFile(path.join(__dirname, STORAGE_DIR, saveFileName), function(err, data) {
      if (err) {
          res.sendStatus(404);
      } else {
          // modify the data here, then send it
          res.send(data);
      }
  });
});

app.post("/background/upload",(req,res)=>{
  const bb = Busboy({ headers: req.headers });
  var saveTo = ""; 
  var fileName = ""; 
 
  bb.on("file", (name, file, info) => {
    const { filename, encoding, mimeType } = info;
    console.log(
      `File [${name}]: filename: %j, encoding: %j, mimeType: %j`,
      filename,
      encoding,
      mimeType
    );

    const saveFileName = "background_" + req.query.key
    saveTo = path.join(__dirname, STORAGE_DIR, saveFileName);
    console.log("Save to:" + saveTo);

    file.pipe(fs.createWriteStream(saveTo));
/*
    file.on('data', (data) => {
      console.log(`File [${name}] got ${data.length} bytes`);
    }).on('close', () => {
      console.log(`File [${name}] done`);
    });
*/
  });

  bb.on('field', (name, val, info) => {
    console.log(`Field [${name}]: value: %j`, val);
    fileName = val;
  });

  bb.on('close', () => {
    console.log('Done loading file!');
    res.send({
      status: "server", 
      value: fileName   
    });
    res.end();
  });

  req.pipe(bb);  
  // ...
});

// -----------------------------------------------------------------------------
// All other GET requests not handled before will return our React app

app.get("*", async (req, res) => { 

  if (req.originalUrl === "/demo") {
    console.log(`Demo mode ON`);
    isDemoMode = true;    
    return res.sendFile(path.resolve(__dirname, "../frontend/build", "index.html"));
  }

  // Для всех других маршрутов сбрасываем isDemoMode
  res.sendFile(path.resolve(__dirname, "../frontend/build", "index.html"));
});
