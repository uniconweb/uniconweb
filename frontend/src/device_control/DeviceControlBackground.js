
import 'webix/webix.css';
import { $$ } from "@xbs/webix-pro";
import * as webix from "@xbs/webix-pro";

const BACKGROUND_LOAD_URL = "/background/load";
const BACKGROUND_UPLOAD_URL = "/background/upload";

function loadForm(destView, backgroundKey) {
 return  {
    view:"form", 
    elements:[
        {
          view:"list",  id:"list1", type:"uploader",
          autoheight:true, borderless:true
        },      
        { view:"uploader", value:"Load background", link:"list1", multiple:false, accept:"image/png, image/gif, image/jpeg", upload: BACKGROUND_UPLOAD_URL + "?key=" + backgroundKey},
        { view:"button", value:"Clear background", css:"webix_primary" , click:function() { clearBackgroundImage(destView) }},
        {cols:[
            { view:"button", value:"Set", css:"webix_primary" , click:function() { setBackgroundImage(destView, backgroundKey) }},
            { view:"button", value:"Cancel", click:function(){ 
              $$('winShowLoadBackground').close();} 
            }
        ]}
    ]
}
}

export function showLoadBackground(destView, backgroundKey) {
  webix.ui({
    view:"window",
    id: "winShowLoadBackground",
    modal:true,
    height:250,
    width:350, 
    left:450, top:50,
    head:{
      view:"toolbar", cols:[
        { width:4 },
        { view:"label", label: "Set background image" },
        { view:"button", label: 'Close Me', width: 100, align: 'right', click:function(){ $$('winShowLoadBackground').close(); }}
      ]
    },
    body:loadForm(destView, backgroundKey)
  }).show();
}

export function loadBackground(view, backgroundKey) {
  if (!backgroundKey || backgroundKey === "") 
    return;

  const urlPath = BACKGROUND_LOAD_URL + "?key=" + backgroundKey;
  view.config["background"] = backgroundKey;    
  view.$view.style.background = 'url(' + urlPath+ ')';
  view.$view.style.backgroundRepeat = 'no-repeat';

  return;
}

function clearBackgroundImage(view) {
  view.config["background"] = "";
  view.$view.style.background = "";
  $$('winShowLoadBackground').close();
}

function setBackgroundImage(view, backgroundKey) {
  view.config["background"] = backgroundKey;
  const urlPath = BACKGROUND_LOAD_URL + "?key=" + backgroundKey + "&ver=" + Math.random();
  view.$view.style.background = 'url(' + urlPath+ ')';
  view.$view.style.backgroundRepeat = 'no-repeat';
//$$("devicesDesignLayout").$view.render();
  $$('winShowLoadBackground').close();
}