import React, { useRef, useState, useEffect } from "react";
import { Context, setStyleByID, addCssClass } from "../Context";
import Chart, { SyncCharts } from "./ChartMulti";
import { observer } from "mobx-react";

var chart_state = new Map(); // A current state for a specific chart 
chart_state.set("chart1", {eof: false})

async function loadOscData(chart) {
  // todo: вынести во вьюмодель
  let deviceInd = Context.states.indexDevice;
  if (!chart || deviceInd === undefined) return;

  let chartParams = Context.chartParams(chart);
  let device = Context.getDeviceByInd(deviceInd);
  let osc = device?.getOsc();

  if (!osc || !chartParams) return;

  Context.osc_state.trig_time = osc.trig_time
  Context.osc_state.reason = osc.reason;

  let channels = oscChannels(osc, chartParams);

  await loadDrawData(chart, channels); // load from buffer
  await loadMissedData(osc, chart, channels);
}

async function loadMissedData(osc, chart, channels) {
  let missedChannels = [];
  
  channels.forEach(function (ch, index, array) {
    if (!ch?.buffer) {
      missedChannels.push(ch);
      return;
    }
  });

  if (missedChannels.length > 0) {

    await osc.getData(missedChannels); // load missed channels
    setTimeout(async () => {
      loadDrawData(chart, missedChannels)
    }, 1000);
  }
}

async function startOscData(chart) {
  // todo: вынести во вьюмодель

  let deviceInd = Context.states.indexDevice;
  if (!chart || deviceInd === undefined) return;

  let chartParams = Context.chartParams(chart);
  let device = Context.getDeviceByInd(deviceInd);
  if (!device.enabled) {
    return;
  }

  let osc = device.getOsc();
  if (osc === null) {
    osc = await device.requestNewOsc();
  }

  if (!osc || !chartParams) return;

  Context.osc_state.trig_time = osc.trig_time
  Context.osc_state.reason = osc.reason;

  let channels = oscChannels(osc, chartParams);
  const { minY, maxY, timeX} = chartParams.paramsXY;

  osc.setDisplayResolution(timeX);
  osc.openDataStream(channels);

  await startDrawData(chart, channels);
  
  return;
}

async function Adjust() {
// Aligne charts to the end of data
  
  let charts = Context.currDeviceCharts();

  let actions = Context.chartActions;
  charts.forEach(function (chart, index, array) {
    actions[chart].Adjust(); 
  });
}

async function loadDrawData(chart, channelItems) {
  let actions = Context.chartActions[chart];
  if (!actions) return;

  let eof_counter = 0;
  chart_state.set(chart, {eof: false})

  channelItems.forEach(function (ch, index, array) {
    if (!ch?.buffer) {
      return;
    }

//  todo: let values = osc.channelData(ch.var_id); 
    for (let values of ch.buffer.values) {
      let xValues = values.map((valObj) => {
        return valObj.time;
      });
      let yValues = values.map((valObj) => {
        return valObj.val;
      });

      actions.addVarPointMegaRange(xValues, yValues, index + 1);
    }
    ch.buffer.lastPos = ch.buffer.eof === true ? 0 : ch.buffer.values.length;      

    if (ch.buffer.eof === true) {
      eof_counter++;

      if (eof_counter === channelItems.length) { 
        chart_state.get(chart).eof = true;
        actions.zoomExtents();
      }
    }
  });
}

function oscChannels(osc, params) {
  let channels = [];
  if (typeof params !== 'object') {
    return;
  }

  params.selectedParams.forEach(function (param, index, array) {
    let chItem = osc.channel(param.var_id);
    channels.push(chItem);
  });

  return channels;
}

export async function closeOsc(device) {
  let osc = device?.getOsc();

  if (osc != null) {
    osc.closeDataStream();
  }
}

export function stopDrawOsc(device) {
  let osc = device?.osc;
  if (osc != null) {
    osc.stopDataStream();
  }
}

export async function loadHistoryOscData(deviceIndex, step) {
  let device = Context.getDeviceByInd(deviceIndex);  
  let osc = device?.getOsc();

  if (osc != null && osc.isReceiving()) {
    await osc.closeDataStream();
  }

  osc = await device.requestNewOsc(step);

  Context.osc_state.trig_time = osc.trig_time
  Context.osc_state.reason = osc.reason;

  let chartChannels = [];
  let visibleCharts = Context.deviceCharts(deviceIndex);
  let params = Context.paramToCharts.get(deviceIndex);
  if (!params) return;

  for (let chart of visibleCharts) {
    let chartParams = params.get(chart);
    let channels = oscChannels(osc, chartParams);
    chartChannels.push(channels);
  }

  let allChannels = [];
  for (let i = 0; i < chartChannels.length; ++i) {
    allChannels.push(...chartChannels[i]);
  }

  if (allChannels.length === 0) {
    console.log("There is no any selected channel!");
    return;
  }

  await osc.openDataStream(allChannels, step);

  let i = 0;
  let actions = Context.chartActions;  
  for (let chart of visibleCharts) {
    actions[chart].clearChart();
    chart_state.get(chart).eof = true;
    await startDrawData(chart, chartChannels[i]);
    i++;
  }

  Adjust(); // Aligne charts to the end of data
}

export async function startNewOsc(deviceIndex) {
  let device = Context.getDeviceByInd(deviceIndex);  
  let osc = device?.getOsc();

  if (osc != null && osc.isReceiving()) {
    await osc.closeDataStream();
  }

  osc = await device.requestNewOsc();

  Context.osc_state.trig_time = osc.trig_time
  Context.osc_state.reason = osc.reason;

  let chartChannels = [];
  let visibleCharts = Context.deviceCharts(deviceIndex);
  let params = Context.paramToCharts.get(deviceIndex);
  if (!params) return;

  for (let chart of visibleCharts) {
    let chartParams = params.get(chart);
    let channels = oscChannels(osc, chartParams);
    chartChannels.push(channels);
  }

  let allChannels = [];
  for (let i = 0; i < chartChannels.length; ++i) {
    allChannels.push(...chartChannels[i]);
  }

  if (allChannels.length === 0) {
    console.log("There is no any selected channel!");
    return;
  }

  osc.openDataStream(allChannels);

  let i = 0;
  let actions = Context.chartActions;  
  for (let chart of visibleCharts) {
    actions[chart].clearChart();
    chart_state.get(chart).eof = true;
    startDrawData(chart, chartChannels[i]);
    i++;
  }

  Adjust(); // Aligne charts to the end of data
}

export async function restartOscData(indexDevice) {   
  // todo вынести во вьюмодель

  let device = Context.getDeviceByInd(indexDevice);
  if (!device) {
    return;
  }  

  let osc = device.getOsc();
  if (!osc) {
    console.warn("There is no device osc!");    
    return;
  }

  if (osc?.isStreaming() === true) {
    return;
  }

  Context.osc_state.trig_time = osc.trig_time
  Context.osc_state.reason = osc.reason;

  let chartChannels = [];
  let visibleCharts = Context.deviceCharts(indexDevice);
  let params = Context.paramToCharts.get(indexDevice);
  if (!params) return;

  for (let chart of visibleCharts) {
    let chartParams = params.get(chart);
    let channels = oscChannels(osc, chartParams);
    chartChannels.push(channels);
  }

  let allChannels = [];
  for (let i = 0; i < chartChannels.length; ++i) {
    allChannels.push(...chartChannels[i]);
  }

  if (allChannels.length == 0) {
    console.log("There is no any selected channel!");
    return;
  }

  osc.openDataStream(allChannels);

  let i = 0;
  for (let chart of visibleCharts) {
    startDrawData(chart, chartChannels[i]);
    i++;
  }

  Adjust(); // Aligne charts to the end of data
}

async function startDrawData(chart, channelItems) {
  if (chart === "") return;

  let eof_counter = 0;
  channelItems.forEach(async function (ch, index, array) {

    const reader = ch?.getReader();

    if (!reader) return;

    const drawData = async (reader) => {

      let actions = Context.chartActions;

      const { done, value: data } = await reader?.read();

      if (done) {
        // actions[chart].zoomExtents();
        // setTimeout(async () => {
        //   SyncCharts();
        // }, 0);
        return;
      }

      if (data) {
        if (chart_state.get(chart).eof === true) {
          actions[chart].clearChart();
          chart_state.get(chart).eof = false;

          Context.osc_state.trig_time= data.trig_time;
          Context.osc_state.trig_reason = data.reason;
          console.log("OscilloscopeCharts: "+ JSON.stringify(Context.osc_state));
        }

        let values = data.values;
        let xValues = values.map((valObj) => {
          return valObj.time;
        });
        let yValues = values.map((valObj) => {
          return valObj.val;
        });
        try {
          if (Context.isHistory) {
            actions[chart].addVarPointMegaRange(xValues, yValues, index + 1)
          } else {
            actions[chart].addVarPointRange(xValues, yValues, index + 1);
          }
        } catch {
          return; // it is happened when chart is hidden
        }


        if (data.eof) {
          eof_counter++;

          if (eof_counter === channelItems.length) {  // if all channels of the chart is eof
            chart_state.get(chart).eof = true;
            eof_counter = 0;       

            // overview on eof
//          if (Context.isHistory) {
              // setTimeout(async () => {
              //   SyncCharts();
              // }, 0);
//          }
          }
        }
      }

      setTimeout(await drawData(reader));
    };

    setTimeout(await drawData(reader));
  });
}

//create your forceUpdate hook
function useForceUpdate() {
  const [value, setValue] = React.useState(0); // integer state
  console.log("forse update");
  return () => setValue((value) => value + 1); // update the state to force render
}

function usePrevious(value) {
  const ref = useRef();
  useEffect(() => {
    ref.current = value; //assign the value of ref to the argument
  },[value]); //this code will run when the value of 'value' changes
  return ref.current; //in the end, return the current ref value.
}

const OscilloscopeChartsObserver = observer(({}) => {
  const [deviceInd, setDeviceInd] = useState(0);
  const prevDeviceInd = usePrevious(deviceInd);
  
  const handleTabSwitch = async () => {
    let actions = Context.chartActions;
    console.log("Render oscilloscope for the device = " + Context.states.indexDevice + ", visibility = " + Context.states.currTabViewId);
    let isHidden = Context.states.currTabViewId !== "devicesОscilloscopeTab";
    if (isHidden || Context.isHalted) {
      await stopDrawOsc(Context.currentDevice);
    } else {
      if (prevDeviceInd !== Context.states.indexDevice) {
        let dev = Context.getDeviceByInd(prevDeviceInd);
        if (dev) {
          await stopDrawOsc(dev);
        }
      }
      setTimeout(() => {
        Context.currDeviceCharts().forEach(chartID => {
          actions[chartID].restorePreviousZoomState();
        });
      }, 500);
      //setTimeout(restorePreviousZoomState, 1000);
    }
  };


  useEffect(() => {
    handleTabSwitch();
    setDeviceInd(Context.states.indexDevice);
  }, [
    Context.chartsLength,
    Context.states.indexDevice,
    Context.states.currTabViewId,
  ]);

  let isVisible = Context.states.currTabViewId === "devicesОscilloscopeTab";
  return (
    <OscilloscopeCharts
      id="oscilloscope_charts"
      deviceInd={Context.states.indexDevice}
      visibleCharts={Context.currDeviceCharts()}
      chartsLength={Context.chartsLength}
      chartActions={Context.chartActions}
      params={Context.paramToCharts}
      visibility={isVisible}
    />
  );
});

function OscilloscopeCharts(props) {
  /*    
    const _showCharts = () => {

      let chartActions = props.chartActions;

      console.log("Render oscilloscope for the device = " + props.deviceInd + ", charts = " + props.visibleCharts.length + ", visibility = " + props.visibility);

      for (let chartId of props.visibleCharts) {

//      let visibility = props.visibility ? 'visible' : 'hidden';
//      chartActions[chartId].setVisibility(visibility);
//      document.documentElement.style.setProperty('--chartcount', props.chartsLength);
      };
    };
*/    
    let params = props.params?.get(props.deviceInd);
    const startOscDataFunc = !Context.isHalted ? startOscData : null;

    React.useEffect(()  => {

      setTimeout(() => {
          if ( props.visibility === true) {
            let params = props.params?.get(props.deviceInd);
            SyncCharts(params);
            if (!Context.isHalted) {
              Adjust(); // Aligne charts 
            }
          }
        }, 1000);
    });


  let oscilloscopes = props.visibleCharts.map((item) => (
    <Chart
      id={item}
      key={item}
      title="&nbsp;"
      loadData={loadOscData}
      startData={startOscDataFunc}
      params={params?.get(item)}
      chartCount={props.chartsLength}
      isVisibile={props.visibility}
    />
  ));

  return (
    <div id="container" className="wrapper">
      {oscilloscopes}
    </div>
  );
}

export default OscilloscopeChartsObserver;
