const WebSocket = require('ws');
const express = require("express");
const router = express.Router();

const DATA_SERVER_IP =  "localhost"; // assume that the data server is on the same machine
const DATA_SERVER_PORT = 1235;
const CONNECTION_TIMEOUT_MSC = 5000;

 async function getServerIP() {
  return new Promise((resolve, reject) => {
    let socketUrl = "ws://" + DATA_SERVER_IP + ":" + DATA_SERVER_PORT;
    let socket = new WebSocket(socketUrl);
    let resolved = false; 

    const timeoutId = setTimeout(() => {
      if (!resolved) {
        console.log('Connection timeout');
        socket.close();
      }
    }, CONNECTION_TIMEOUT_MSC);

    socket.on('open', () => {
      console.log('Connection established');
      let cmd = { cmd: "GET_IP" };
      try {
        socket.send(JSON.stringify(cmd));
      } catch (error) {
        console.log('Send error:', error.message);
        clearTimeout(timeoutId); // clear timer
        reject("Send error");
      }
    });

    socket.on('error', error => {
      clearTimeout(timeoutId);
      let msg = 'Connection error, check if the server is on and after restart your browser:' + error.message;
      console.log(msg);
      reject(msg);      
    }); 

    socket.on('message', message => {
      try {
        const response = JSON.parse(message);
        console.log('Received message:' + message);
        if (!resolved && response.ip) {
          resolved = true; // Устанавливаем флаг разрешения промиса
          clearTimeout(timeoutId); // Очищаем таймер
          resolve(response.ip);
          socket.close(); 
        } else {
          reject("No IP in the response");
        }
      } catch (error) {
        console.log('Error parsing message:', error);
        reject("Error parsing message");
      }
    });
  });
}

const isValidIP = ip_Str => {
  if (ip_Str === "localhost") {
    return true;
  }
  // get the parts
  const parts = ip_Str.split('.');
  // test for 4 parts if not return false
  if (parts.length !== 4)
    return false;
  // filter out any leading zero or not between 0-255 part 
  // return true if all parts passed the filtered OR false if any part not passed the filter
  return (parts.filter( part => (part === Number(part).toString()) && (Number(part) >= 0 && Number(part) <= 255) ).length === 4);
}

async function handleGetServerIP(req, res) {
    try {
      console.log(`Request received for /api/IP from getServerIP`);

      let resultIP = req.socket.localAddress || req.connection.localAddress;
      console.log(`Try to receive IP from request string: ${resultIP}.`);

      if (resultIP.startsWith('::ffff')) {
        resultIP = resultIP.split(':').pop();
      } else if (resultIP.startsWith('::1')) {
        resultIP = "localhost";
      }

      if (!isValidIP(resultIP)) {

        console.log(`Try to receive IP from socket server`);
        
        resultIP = await getServerIP();
      }
      
      if (isValidIP(resultIP)) {
        console.log(`Responding with IP: ${resultIP}`);
        res.send({ ip: resultIP });
      } else {
        res.send({ ip: "" });
      }
    } catch (e) {
      res.send({ ip: "" });
    }
 }

  router.get("/IP", handleGetServerIP);
  module.exports = router;