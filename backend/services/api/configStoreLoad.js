const express = require("express");
const fs = require("fs");

const DOCKER_DIR = process.env.STORAGE_DIR ?? "./storage";
const DATA_FILE_NAME = `${DOCKER_DIR}/data.json`;

const configStoreLoadGet = (req, res) => {
  if (req.query.id) {
    if (req.configs.hasOwnProperty(req.query.id))
      return res.status(200).send(req.configs[req.query.id]);
    else return res.status(404).send({ message: "Config not found." });
  } else if (!req.configs)
    return res.status(404).send({ message: "Config not found." });
  return res.status(200).send(req.configs);
};

const configStoreLoadPost = (req, res) => {
  if (req.body.config && req.body.id) {
    req.configs[req.body.id] = req.body.config;

    fs.writeFile(
      DATA_FILE_NAME,
      JSON.stringify(req.configs, null, 1),
      (err, response) => {
        if (err){
          return res
            .status(500)
            .send({
              message: `Error writing file. Filename=${DATA_FILE_NAME}`,
            });
          }
        return res
          .status(200)
          .send({ message: `Config with id="${req.body.id}" created.` });
      }
    );
  } else return res.status(400).send({ message: "Bad request." });
};

const addConfigStoreLoadAPI = (app, apiPath) => {
  app.use(apiPath, express.json());
  app.use(apiPath, express.urlencoded({ extended: true }));

  app.use((req, res, next) => {
    fs.readFile(DATA_FILE_NAME, (err, data) => {
      req.configs = {};
      if (!err) {
        try {
          req.configs = JSON.parse(data);
        }
        catch(e) {
          console.log("Error parsing storage file");
        }
      }
      next();
    });
  });

  //app.route(apiPath).get(configStoreLoadGet).post(configStoreLoadPost);
  app.post(apiPath, configStoreLoadPost);
  app.get(apiPath, configStoreLoadGet);
};

module.exports = addConfigStoreLoadAPI;
