import { DeviceProvider } from "./services/fr_deviceprovider.mjs";
import {default as RequestHelper} from "./services/fr_requesthelper.mjs";

const ARG_ERROR = "Invalid function argument!";

export default class Oscilloscope {
    constructor() {
        this.id = 0
        this.sysTypeId = 0
        this.deviceId = 0
        this.name = ''
        this.desc = ''
        this.resolution_us = 0;
        this.lastError = 0;
        this.trig_time = new Date().getTime();
        this.reason = 1;
        this.analogChannels = [];
        this.discreteChannels = [];
        this.capturedChannels = [];

        this.deviceProvider = new DeviceProvider(RequestHelper);

        // Переменные для измерения производительности
        this._msgCount = 0;
    }

    deserialize(oscHeader, sysTypeId, deviceId) {
        this.id = oscHeader.id;
        this.sysTypeId = sysTypeId;
        this.deviceId = deviceId;
        this.name = oscHeader.name;
        this.desc = oscHeader.desc;
        this.resolution_us = oscHeader.resolution_us;  
        this.display_resolution_ms = oscHeader.display_resolution_ms;     
        this.analogChannels = oscHeader.analog_channels;
        this.discreteChannels = oscHeader.discrete_channels;
        this.capturedChannels = [];
        this.trig_time = oscHeader["trig_time"];
        this.reason = oscHeader["reason"];
    }

    openDataStream(channels, step) {

        if (!Array.isArray(channels) || channels.length === 0) {
            channels = this.analogChannels;
        }

        for (let ch of channels) {
            this.captureChannel(ch);
        }

        // if (this.capturedChannels.length === 0) {
        //     for (let ch of this.analogChannels) {
        //         this.captureChannel(ch);
        //     }
            
        //     for (let ch of this.discreteChannels) {
        //         this.captureChannel(ch);
        //     }

        // }

        for (let ch of channels) {
            if (ch.stream && ch.controller) {
                continue;
            }

            ch.stream = new ReadableStream({
                start(controller) {
                    ch.controller = controller;
                }
            });

            ch.getReader = (() => {
                try {
                    return ch.stream?.getReader();
                } catch {
                    return null;
                } 
            });
        }

        this._msgCount = 0;

        let osc_vars = [];
        for (let chObj of this.capturedChannels) {
            osc_vars.push(chObj.var_id);
        }

        try {
            this.deviceProvider.reqOpenOscStream(this.sysTypeId, this.deviceId, this.id, osc_vars, step);
        } catch (error) {
            this.lastError = error;
            console.error(error);
        }
    }

    async getData(channels) {

        if (!Array.isArray(channels) || channels.length === 0) {
            channels = this.analogChannels;
        }

        for (let ch of channels) {
            this.captureChannel(ch);
        }

        let osc_vars = [];
        for (let chObj of channels) {
            osc_vars.push(chObj.var_id);
        }

        await this.deviceProvider.reqOscData(this.sysTypeId, this.deviceId, this.id, osc_vars);
    }

    captureChannel(ch) {
        if (!ch || ch.var_id === undefined) {
            console.error(ARG_ERROR);
            return;
        }

        let chObj = this.capturedChannels.find(item => item.var_id === ch.var_id);
        if (!chObj) {
            chObj = ch;
            chObj.buffer = this.createBufferObj();
            chObj._timeLabel = new Date().getTime()
            console.time(`The stream elapsed time(${chObj._timeLabel}), var name = ${chObj["name"]}`);
            this.capturedChannels.push(chObj);
        }
    }

    createBufferObj() {
        let buffer = {};
        buffer = {};
        buffer.values = [];
        buffer.lastPos = 0;                
        buffer.eof = false;
        return buffer;
    }

    async updateHeader() {
        try {
            let oscHeader = await this.deviceProvider.reqOsc(this.sysTypeId, this.deviceId, this.id);
            if (this.id !== oscHeader.id) {
                throw ("requested osc id is incorrect!")
            }

            this.name = oscHeader["name"];
            this.desc = oscHeader["desc"];
            this.trig_time = oscHeader["trig_time"];
            this.reason = oscHeader["reason"];
            this.resolution_us = oscHeader["resolution_us"];  
            this.display_resolution_ms = oscHeader["display_resolution_ms"];

        } catch (error) {
            this.lastError = error;
            console.error(error);
        }

        return;
    }

    async setDisplayResolution(displayResolution) {
        if (this.display_resolution_ms === Number(displayResolution)) {
            return;
        }

        try {
            this.display_resolution_ms = Number(displayResolution);
            await this.deviceProvider.setOscSettings(this.sysTypeId, this.deviceId, {"displayResolution": this.display_resolution_ms});
        } catch (error) {
            this.lastError = error;
            console.error(error);
        }

        return;
    }

    isReceiving() {
        if (this.capturedChannels === undefined) {
            return false;
        }

        if (this.capturedChannels.length === 0) {
            return false;
        }

        return true;
    }

    isStreaming() {
        if (this.capturedChannels === undefined) 
            return false;

        if (this.capturedChannels.length > 0) {
            for (let chObj of this.capturedChannels) {
                if (chObj.stream !== null) {
                    return true;
                }
            }
        }   
    
        return false;
    }

    async closeDataStream() {
        if (this.capturedChannels === undefined) {
            return;
        }

        for (let chObj of this.capturedChannels) {
            this._finishChannel(chObj);
        }

        this.capturedChannels = [];
        try {
            await this.deviceProvider.reqCloseOscStream(this.sysTypeId, this.deviceId, this.id);
        } catch (error) {
            this.lastError = error;
            console.error(error);
        }
    }

    stopDataStream() {
        if (this.capturedChannels === undefined) return;

        for (let chObj of this.capturedChannels) {
            chObj.controller?.close();
            chObj.stream = null;
            chObj.controller = null;
        }
    }    

    streamData(socketData) {
        if (this.capturedChannels === undefined || this.capturedChannels.length === 0) {
            return;
        }

        for (let varId of socketData.vars) {
            let chObj = this.capturedChannels.find(item => item.var_id === varId)
            if (!chObj) {
                continue;
            }
            if (chObj.buffer.values.length === 0 || chObj.buffer.eof) {
//              console.log(`Start the channel data receiving, var name = ${chObj["name"]}`);
                chObj._byteCount = 0;
                chObj._receivedValues = 0;
                chObj._timeLabel = new Date().getTime()
//              console.time(`The stream elapsed time(${chObj._timeLabel}), var name = ${chObj["name"]}`);
            }

            let dataObj = this.parse(socketData, chObj);
            if (dataObj.res === -1) {
                continue;
            }

            if (chObj.buffer.eof) {
                chObj.buffer.values = [];
                chObj.buffer.eof = false;
                chObj.buffer.lastPos = 0;
            }

            if (dataObj.res > 0) {
                chObj.buffer.values.push(dataObj.values);
                chObj.buffer.eof = dataObj.eof;
                chObj.buffer.trig_time = dataObj.trig_time;
                chObj.buffer.reason = dataObj.reason;

                if (chObj.controller) {
                    let lastValLength = chObj.buffer.values.length - chObj.buffer.lastPos;
                    chObj.buffer.lastPos = chObj.buffer.values.length;
                    if (lastValLength === 1) { // buffer is actual, there is not missed data
                        chObj.controller.enqueue(dataObj);
                    } else if (lastValLength > 1) {
                        let data = dataObj;
                        data.values = [];

                        let valuesArr = chObj.buffer.values.slice(-lastValLength) // put all missed data
                        for (let values of valuesArr) {
                            data.values = [...data.values, ...values];
                        }
    
                        chObj.controller.enqueue(data); // add missed data
                    }
                }
            } 

            if (chObj.buffer.eof === true) {
//              console.log(`Finished the osc data receiving, var name = ${chObj["name"]}, received values = ${chObj._receivedValues}, bytes = ${chObj._byteCount}`);
//              console.timeEnd(`The stream elapsed time(${chObj._timeLabel}), var name = ${chObj["name"]}`);
            }
        }
    }

    _finishChannel(chObj) {
        
//      let channel = this.channel(chObj.var_id);

        chObj.controller?.close();
        chObj.stream = null;
        chObj.controller = null;

    }

    channelData(varId) {
        return this.channel(varId).buffer;
    }

    parse(socketData, chObj) {

        let resObj = {
            res: 1,
            eof: false,
            values:[],
            trig_time: 0,
            reason: 0
        }

        if (socketData.values === undefined) {
            resObj.res = -1;
            return resObj;
        }

        let ind = socketData.vars.indexOf(chObj.var_id)
        let chValues = socketData.values[ind];

        if (!chValues || chValues.length == 0) {
            resObj.res = -1;
            return resObj;
        }

        let receivedValues = chValues.length;
        if (chObj._receivedValues === undefined || chObj._byteCount === undefined) {
            chObj._byteCount = 0;
            chObj._receivedValues = 0;
        }

        chObj._byteCount += receivedValues * JSON.stringify(chValues[0]).length;
        chObj._receivedValues += receivedValues;
        this._msgCount++;
        if (ind === 0) {
 //         console.log(`Received values count: ${receivedValues} for varId: ${chVarId}`);
        }

        let resolution_us = this.resolution_us;
        let time_us = socketData.time - chValues.length * (this.resolution_us);
//      let trig_time_mks = this.trig_time * 1000;

        let values = [];
        for (let newVal of chValues) {
            let time_ms = time_us * 0.001;

            let lastVal = values.length > 0 ? values[values.length - 1].val : undefined;
            let prevLastVal = values.length > 1 ? values[values.length - 2].val : undefined;


            if (chObj.isDiscrete === 0) {
            // For optimization only. Remove unnecessary values when they are not changed
                if (lastVal === prevLastVal && lastVal === newVal) {
                    values[values.length - 1].time = time_ms;
                    time_us += resolution_us;
                    continue;
                } 
            }

            if (chObj.isDiscrete === 1) {
                // For discrete variables, it is necessary to add intermediate points to create so-called "humps"
                if (newVal > lastVal) {
                    let prevTime = values[values.length - 1].time; 
                    let valObj = {
                        'val' : newVal, 
                        'time' : prevTime
                    };
                    values.push(valObj);
                } else if (lastVal > newVal) {
                    let valObj = {
                        'val' : lastVal, 
                        'time' : time_ms
                    };
                    values.push(valObj);
                }
            }


            let valObj = {
                'val' : newVal, 
                'time' : time_ms 
            };
            values.push(valObj);

            time_us += resolution_us;
        }

        resObj.res = 1;
        resObj.eof = (socketData.eof == 1);
        resObj.sof = (socketData.sof == 1);
        resObj.trig_time = socketData.trig_time;
        resObj.reason = socketData.reason;
        resObj.values = values

        if (socketData.vars.length == 1) { // logging when one channel only
            console.log('Receiving channel data crunk:' + ' var = ' + chObj["name"] + ', ch num = ' + chObj["ch_num"] + ', values count = ' +  chValues.length );
        }
        return resObj;
    }

    channel(varId) {
        let result = this.analogChannels.find(item => item.var_id === varId)
        if (!result) {
            result = this.discreteChannels.find(item => item.var_id === varId)
        }
        return result;
    }
}