export class ConfigHelper {
    constructor() {    
        this.configData = new Map(); // <Key, conf_obj>
    }

    async get(keyId) {
        if (!this.configData.has(keyId)) {
            let data = await this.load(keyId);
            this.configData.set(keyId, data);
        }

        if (!this.configData.has(keyId)) {
            return {};
        }

        return this.configData.get(keyId);
    }

    async save(keyId, confObj) {
        this.configData.set(keyId, confObj);

        let url = `/api/config?id=${keyId}`;

        let confObjToSave = {
            id: keyId,
            config: confObj
        }

        let response = await fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify(confObjToSave)
        });
          
        let result = await response.json();

    }

    async load(keyId) {

        let url = `/api/config?id=${keyId}`;
        let response = await fetch(url);
        let result = {}

        if (response.ok) { // если HTTP-статус в диапазоне 200-299
          // получаем тело ответа (см. про этот метод ниже)
          
          try {
            result = await response.json();
          } catch {
            let msg = "Ошибка при обращению к серверу, HTTP:  " + response.status;
            console.error(msg)            
            result = {};
          }

          return result;
        } else {
            if (response.status == 404) { // config not found
                return {};
            } else {
                let msg = "Ошибка при обращению к серверу, HTTP: " + response.status;
                console.error(msg)
            }
        }
        
        return result;
    }
}
