import "webix/webix.css";
import WebixComponent, { scroll } from "./WebixComponent";
import React, { useEffect, useState } from "react";
import { Context } from "./Context";
import { observer } from "mobx-react";
import { $$ } from "@xbs/webix-pro";

function label(caption, css, width) {
  return { label: caption, view: "label", css: css };
}

const GetCurrentTime = () => {
  let d = new Date();
  return (
    d.getHours().toString().padStart(2, 0) +
    ":" +
    d.getMinutes().toString().padStart(2, 0) +
    ":" +
    d.getSeconds().toString().padStart(2, 0)
  );
};

const footerlayout = {
  cols: [
    {
      id: "footerLoadingStatus",
      label: "caption1",
      view: "label",
      css: "footer_label",
    },
    { label: "", view: "label", css: "footer_label" },
    { label: "", view: "label", css: "footer_label" },
    {
      id: "footerTimer",
      label: "",
      view: "label",
      css: "footer_label_right",
    },
  ],
};

const Footer = observer(({}) => {
  const [timer, setMinutes] = useState(GetCurrentTime());
  useEffect(() => {
    let timerObject = $$("footerTimer");
    let footerLoadingStatus = $$("footerLoadingStatus");
    // Context.setStatus("qwe");
    footerLoadingStatus.setValue(Context.status);
    // !!! Вернуть таймер, временно отключён
    // let myInterval = setInterval(() => {
    //        setMinutes(GetCurrentTime());
    //        timerObject.setValue(GetCurrentTime());
    //     }, 1000)
    // return ()=> {
    //     clearInterval(myInterval);
    //   };
  }, [Context.status]);

  return (
    <div className="footer">
      {/* <WebixComponent ui={label("Load","footer_label",0)} data={Context.status} />
      <WebixComponent ui={label("Data","footer_label",0)} data={"Data"} />
      <WebixComponent ui={label("state","footer_label",0)} data={"state"} />
      <WebixComponent ui={label(timer,"footer_label_right",0)} data={timer} /> */}
      <WebixComponent ui={footerlayout} />
    </div>
  );
});

export default Footer;
