
const configDefault = require("./config.default.json");
let configLocal;

try {
    configLocal = require("./config.local.json");
} catch (e) {
    configLocal = {};
}

const Config = {...configDefault, ...configLocal};

export default Config;