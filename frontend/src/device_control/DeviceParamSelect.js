import "webix/webix.css";
import { $$ } from "@xbs/webix-pro";
import { Context } from "../Context";
import { ValueFormatEnum } from "../data_model/param.mjs";
import * as webix from "@xbs/webix-pro/webix.js";

let _param = null;
let selectedDevice = null;

function filterParam(paramItem, elementData) {
  if (!paramItem || !elementData) {
    console.error("Invalid parameter item or element data.");
    return false;
  }
  let res = false;
  let correctViewList = [];
  switch (Number(paramItem.valueFormat)) {
    case ValueFormatEnum.Bin:
      correctViewList = ["button", "text", "switch"];
      break;
    case ValueFormatEnum.Int:
      correctViewList = [ "button", "slider", "text"];
      break;
    case ValueFormatEnum.Float:
      correctViewList = [ "button", "text"];
      break;
    case ValueFormatEnum.Hex:
      correctViewList = [ "button", "text"];
      break;
    case ValueFormatEnum.Text:
      correctViewList = [ "button", "text", "combo"];
      break;

    default:
      break;
  }
  res = !elementData.view || correctViewList.includes(elementData.view);
  if (!res) return false;

  let strongWritableControls = [];
  strongWritableControls = ["button", "combo"];

  if (strongWritableControls.includes(elementData.view)) {
    res = paramItem.rw === "W";
  }

  return res;
}

async function GetDeviceParamsArr(elementData, device) {
  let dataResult = [];
  let j = 1;

  if (!device) {
    console.error(
      "No current device set in Context when calling GetDeviceParamsArr"
    );
    return [];
  }

  device.modules.forEach(function (moduleInfo, index, array) {
    moduleInfo.params.forEach(function (param, index, array) {
      if (filterParam(param, elementData)) {
        dataResult.push({
          module: moduleInfo.displayName(),
          title: param.displayName(),
          dimention: param.valueUnit,
          rw: param.rw,
          rank: j,
          id: j,
          label: param.name,
          idParam: param.var_id,
          modelData: param,
        });
      }
      j++;
    });
  });

  return dataResult;
}

export function selectParameter(currElem_id, onSelectParam) {
  if ($$("selectParam")) {
    $$("selectParam").close();
    webix.ui({}).destructor("selectParam");
  }

  let selectedDevicesMode = Context.isPLCMode();
  let currentElement = $$(currElem_id);
  if (!currentElement || !currentElement.data) {
    console.error("Current element or its data is not properly configured.");
    return;
  }

  let bodyConfig = {
    view: "datatable",
    columns: [
      { id: "module", header: "Module", width: 200, sort: "string" },
      {
        id: "title",
        header: [{ content: "textFilter", placeholder: "Param" }],
        width: 300,
        sort: "string",
      },
      { id: "dimention", header: "dim", width: 50, sort: "string" },
      { id: "rw", header: "R/W", width: 50, sort: "string" },
    ],
    headerRowHeight: 41,
    select: "row",
    height: 600,
    scroll: true,
    resize: true,
    escHide: true,
    autowidth: true,
    data: [],
    on: {
      onItemDblClick: function (id, event, node) {
        let currentRow = this.getItem(id);
        if (!currentRow || !currentRow.modelData) {
          console.error("Parameter data is missing or incomplete");
          return;
        }
        _param = this.data.pull[id].modelData;
        onSelectParam(_param);
        $$("selectParam").close();
      },
    },
  };

  if (!selectedDevicesMode) {
    // DEVICES mode: load data immediately
    let data = GetDeviceParamsArr(currentElement.data, Context.currentDevice);
    bodyConfig.data = data;
  }

  const OnSelectedDevice = (device) => {
    if (!device) {
      console.error("No device selected or device is undefined.");
      return;
    }

    let deviceData = GetDeviceParamsArr(currentElement.data, device);
    bodyConfig.data = deviceData;
    let dataTable = $$("selectParam").getBody().getChildViews()[0];
    if (dataTable) {
      dataTable.clearAll();
      dataTable.parse(deviceData);
    } else {
      console.error("DataTable element not found in selectParam.");
    }
  };

  const devicesPanel = {
    view: "scrollview", // scroll for devices
    width: 300,
    body: {
      rows: [
        {
          template: "<div class='header-template' >Devices</div>",
          type: "header",
        },
        {
          rows: Context.devices.map((device) => ({
            view: "toggle",
            type: "icon",
            icon: "wxi-eye",
            label: device.name,
            deviceID: device.id,
            onLabel: `<span class="">${device.displayName()}</span>`,
            offLabel: `<span class='${
              device.enabled ? "" : "label_disabled"
            }'>${device.displayName()}</span>`,
            minWidth: 130,
            height: 80,
            value: device === Context.devices[0] ? 1 : 0,
            click: function (id, event) {
              let s1 = $$(id).getParentView();
              s1._cells.forEach((element) => {
                element.setValue(0); // Сброс других переключателей
              });

              OnSelectedDevice(device);
              console.log("selectedDevice " + device.displayName());
            },
          })),
        },
      ],
    },
  };

  const windowConfig = {
    view: "window",
    id: "selectParam",
    position: "center",
    move: true,
    modal: true,
    close: true,
    head: "Select parameter",
    body: selectedDevicesMode ? { cols: [bodyConfig, devicesPanel] } : bodyConfig,
    on: {
      onShow: function () {
        if (selectedDevicesMode && Context.devices.length > 0) {
          let defaultDevice = Context.devices[0];
          OnSelectedDevice(defaultDevice);
        }
      },
    },
  };

  webix.ui(windowConfig).show();
}

export function getSelectedParam() {
  return _param;
}

export default GetDeviceParamsArr;
