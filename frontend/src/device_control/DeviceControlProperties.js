import 'webix/webix.css';
import { $$ } from "@xbs/webix-pro";
import  {selectParameter}  from "./DeviceParamSelect";

export class ColorEnum {
  static Undefined = 0;
  static No_Color = 0;
  static Silver = 1;
  static Turquoise = 2;
  static Red = 3;
  static Green = 4;
  static White = 5;
  static Orange = 6;

  static colors = [
    {id:ColorEnum.Silver, value:"Silver"},
    {id:ColorEnum.Turquoise, value:"Lightblue"},
    {id:ColorEnum.Red, value:"Salmon"},
    {id:ColorEnum.Green, value:"ForestGreen"},
    {id:ColorEnum.White, value:"White"},
    {id:ColorEnum.Orange, value:"Orange"},
    {id:ColorEnum.No_Color, value:"Transparent"}
  ];

  static color(id) {
    for (let cl of this.colors) {
      if (cl.id === Number(id)) return cl.value;
    }
  }
}

export class DeviceProperties {
  constructor() {
    this.prefix_ID = ""
    this.currentElement = undefined;
  }

  ID(baseId) {
    return `${this.prefix_ID}${baseId}`;
  }
  
  updateValue(prop, value) {
      $$(this.ID("formProperties")).elements["prop"].setValue(value);
  }  

  setCurrentElement(srcElemId, onChangedValue) {
    let form = $$(this.ID("formProperties"));
    this.currentElement = $$(srcElemId);
    form["currentElement"] = $$(srcElemId);
    
    $$(this.ID("selectParamBtn")).show();

    for (let i in form.elements){
      form.elements[i].disable();
      form.elements[i].hide();
    }

    for (let prop of $$(srcElemId).data.properties) {
      let elem = form.elements[prop];
      if (!elem) continue;
    
      let propValue = $$(srcElemId).data[prop];

      let value;
      let visible = true;
      if (typeof propValue === 'object') {
        if ("value" in propValue) {
          value = propValue.value;
        }
        if ("options" in propValue) {
          elem.define("options", propValue.options);
        }
        if ("label" in propValue) {
          elem.data["label"] = propValue.label;
        }
        if ("visible" in propValue) {
          visible = propValue.visible;
        }
      } else {
        value = propValue;
      }

      if (!visible) continue;

      elem.setValue(value);
      elem.enable(); 
      elem.show();

      if (!elem.hasEvent("onChange")) {
        elem.attachEvent("onChange", function(newV, oldV, config) {
          let currElem = form["currentElement"]
          let propName = this.config.name;
          let propValue = currElem.data[propName];
          if (typeof propValue === 'object' && "value" in propValue) {
            propValue.value = newV;
          } else {
            propValue = newV;
          }

          onChangedValue(currElem, propName, propValue, oldV);
        });
      }
    };
  }

  clearPropertiesForm() {
    let form = $$(this.ID("formProperties"));
    const selectParamBtn_id = this.ID("selectParamBtn");

    for (let i in form.elements){
        form.elements[i].disable();
        form.elements[i].hide();
    }

    $$(selectParamBtn_id).hide();
    this.currentElement = undefined;
  }

  show()
  {
    $$(this.ID("designProperties")).enable();
    $$(this.ID("designProperties")).show();
  }

  hide()
  {
    $$(this.ID("designProperties")).disable();
    $$(this.ID("designProperties")).hide();
  }

  designProperties(params, onSelectedParam) {
    this.prefix_ID =  params.prefix_ID;
    return {
      view:"scrollview", 
      width:340, 
      disabled:false,
      // css:"webix_formbuilder_properties",
      id:  this.ID("designProperties"),
      body:{
        css:"webix_formbuilder_properties_form",
        rows:[
          {
            view:"form", borderless:true, 
            id:  this.ID("formProperties"),
            localId:"form",
            // scroll:true,
            // height: 180,
            autoheight:true,
            elementsConfig:{
              labelAlign:"right",
              labelWidth:90
            },
            rows:[
              { view:"button",  id:this.ID("selectParamBtn"), value:"Select parameter",click: (e) => {
                selectParameter(this.currentElement.config.id, onSelectedParam);
              }
            },
  /*            
              { cols:[
                { template:"Parameter", type:"section" }
              ]},
  */         
              { view:"fieldset", label:"Parameter", body:{
              rows:[
               
              { label: "Device: ", view: "text", name: "device_name", readonly: true, css: { "background": "#CCD7E6 !important", "border-color": "#CCD7E6 !important" } },
              { label:"Name", view:"text", name:"param_name", readonly:true, css:{"background":"#CCD7E6 !important","border-color": "#CCD7E6 !important"}},
              { label:"unit", view:"checkbox", name:"unit",value:0},
              { label:"value format", view:"checkbox", name:"use_value_format", readonly: true, value:0},
              { label:"Set value", view:"text", name:"set_value"},
              { label:"Set value", view:"combo", name:"set_value_combo", options:["One", "Two", "Three"], optional:{button:true}},
              { label:"Options", view:"text", name:"option_list", native:"list"},
              { label:"Min", id:this.ID("min"), view:"text", name:"min", native:"number", optional:{
                counter:true, slider:true, rangeslider:true
              }},
              { label:"Max", id:this.ID("max"), view:"text", name:"max", native:"number", optional:{
                counter:true, slider:true, rangeslider:true
              }},
              { label:"Step", id:this.ID("step"), view:"text", name:"step", native:"number", optional:{
                counter:true, slider:true, rangeslider:true
              }},
              { label:"Bit num", id:this.ID("bit_num"), view:"text", name:"bit_num", native:"number", optional:{
                counter:true, slider:true, rangeslider:true
              }}
              ]}},

              { height: 10},
  /*            
              { cols:[
                { template:"View", type:"section" }
              ]},
  */
              { view:"fieldset", label:"View", body:{
                rows:[
                  { label:"Label", id:this.ID("elLabel"), view:"text", name:"baseLabel"},
                  { label:"Label width", view:"text", name:"labelWidth", native:"number"},
                  { label:"Font size", view:"text", name:"font_size"},
                  { label:"Font color", view: "text", name:"font_color" },
                  { label:"Auto width", view:"checkbox", name:"autowidth", value:0},
                  { label:"Width", view:"text", name:"width", native:"number"},
                  { label:"Height", view:"text", name:"height", native:"number"},
                  { label:"Color", view: "combo", type:"select", name:"selectColor", value:"red", options:ColorEnum.colors, optional:{text:true} },            
                  // { label:"Position", view:"richselect", name:"labelPosition", options:["top", "left"]},
                  { label:"Right text", view:"text", name:"labelRight"},
                  { label:"On label", view:"text", name:"onLabel"},
                  { label:"Off label", view:"text", name:"offLabel"},
                ]}
              },
              { height: 10},            
            ]
          }
        ]

      }
    };
  }
}

