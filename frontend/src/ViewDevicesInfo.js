import React, { useRef, createRef, useState, useEffect } from "react";
import "webix/webix.css";
import * as webix from "@xbs/webix-pro/webix.js";
import { observer } from "mobx-react";
import { Context } from "./Context";
import WebixComponent from "./WebixComponent";
import { $$ } from "@xbs/webix-pro";

const textArr = [];
textArr.push(
  "By using thyristors (SCRs) in a phase angle control mode, reduced voltage control can be achieved. Phase control makes it possible to gradually increase the motor terminal voltage from an initial set point up to the system supply voltage level. The related starting current and the starting torque can be optimally adjusted to the motor/load conditions."
);
textArr.push(
  "Control Module- MVCP is the “brain” of the soft starter. It consists of the mBoard that includes: • Main CPU PCB. • HMI board: can be either placed in the Control Module box or at the cabinet door. • Fireboard PCB. • Powersupply. • Input/outputinterfaceterminals. • Optional PCBs (when ordered). </br>The Control Module for HRVS-DN-PowerStart is identical for all ratings and suitable for mounting in the L.V. compartment of the cabinet which should be fully segregated from the M.V. compartment. </br>Interposing relays should be connected to all HRVS-DN-PowerStart auxiliary contacts, three relays must be incorporated: Immediate, End of Acceleration and Fault."
);
textArr.push(
  "Motor will start only if SOFT STOP (terminal 21) and STOP (terminal 22) terminals are connected to Control Input voltage."
);
textArr.push(
  "Control Input voltage (START, SOFT STOP, STOP, terminal inputs 20,21,22) can be the same as Control Supply (terminals 41, 42) or voltage from a different source."
);
textArr.push("Text 5");
textArr.push("Text 6");

function getInfoText(index) {
  return textArr[index];
}

function getInfo(props) {
  return {
    id: "infoParamLabelTest",
    rows: [
      {
        height: 240,
        cols: [
          {
            id: "infoParamPict",
            data: { src: "static/pictures/pic0.jpg" },
            template: function (obj) {
              return '<img class="infoParamPict" src="' + obj.src + '"/>';
            },
          },

          { label: "Label", view: "label", id: "infoParamLabel" },
        ],
      },
      {
        label: "Info",
        value: "",
        view: "richtext",
      },
    ],
  };
}

const ViewDevicesInfo = observer((props) => {
  useEffect(() => {
    //  console.log("ViewDevicesInfo");
    //  console.log("indexDevice", Context.states.indexDevice);
    let elem = $$("infoParamLabel");
    // console.log(elem);
    if (elem) {
      let currentInfo = getInfoText(Context.states.indexDevice);
      elem.setValue(currentInfo);
    }
    let pict = $$("infoParamPict");
    if (pict) {
      pict.setValues({
        src: "static/pictures/pic" + Context.states.indexDevice + ".jpg",
      });
    }
  }, [Context.infoCurrentDivice]);

  return (
    <WebixComponent id={props.id} ui={getInfo()} height="var(--infoArea)" />
  );
});

export default ViewDevicesInfo;
