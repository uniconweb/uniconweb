import { ConfigHelper } from "./services/confighelper.js";

export class PLC_ConfigHelper {
    constructor() {    
        this.configData = new Map(); // <Key, conf_obj>
        this.configHelper = new ConfigHelper()
    }

    get(devId) {
        if (!this.configData.has(devId)) return null;

        return this.configData.get(devId);
    }

    set(devId, confObj) {
        this.configData.set(devId, confObj);
    }

    async save(devId) {
        // save current device plc configuration
        // пока так, а потом усложнить: сохранять по всем измененным устройствам

        let data = await this.configHelper.get(devId);

        data.plc = this.configData.get(devId)
        await this.configHelper.save(devId, data); 
    }

    async load(devId) {
/*        
        if (this.configData.has(devId)) {
            return; 
        }
*/
        let data = await this.configHelper.get(devId);
        if (data && "plc" in data) {
            this.configData.set(devId, data.plc);
        }
    }
}

export class PLC_Element {
    constructor() {
        this.view = { // представление комопненты (координаты, размер, ...) 
            id: 0,
            top: 0,
            left: 0,
            width: 0,
            height: 0,
            label: "",
            labelWidth: 0,
            tips: "",
            useUnit: "",
            placeholder: ""
        };

        this.props = { // св-ва компопненты
            properties: [],
            min : 0,
            max: 0,
            step: 0,
            param_name: "",
            unit: "",
            paramId: 0,
            sendValue: null
        }

        this.param = null; // привязанный объект класса параметр из модели
        this.name = ''; // тип компоненты
    }

    serialise() {
        return JSON.stringify(this);
    }

    deserialise(strJson) {
        return JSON.parse(strJson);
    }
}