import React, { useState, useEffect, createRef } from "react";
import { observer } from "mobx-react";
import { Provider } from "mobx-react";
import * as ReactDOMClient from "react-dom/client";
import { Context } from "./Context";
import { Setup } from "./Setup";
import { $$ } from "@xbs/webix-pro";
import * as webix from "@xbs/webix-pro";
import WebixComponent from "./WebixComponent";
import Footer from "./Footer";
import ResizeSensor from "resize-sensor";
import MenuAccordion from "./MenuAccordion";
import "./App.css";
import ViewMain from "./ViewMain";
import DeviceInfoCaption from "./DeviceInfoCaption";
import { makeAutoObservable, makeObservable, observable, action } from "mobx";
import { height } from "@mui/system";

webix.message.expire = 3000; // messages expire in 3 seconds
webix.message.position = "top";

function resizeWebixComponent() {
  let webixitems = document.getElementsByClassName("webixitem");
  let itemArray = Array.from(webixitems);
  itemArray.map((item) => {
    if ($$(item.children[0])) {
      // console.log("adjust",item.children[0]);
      // $$(item.children[0]).adjust() ;
      $$(item.children[0].getAttribute("view_id")).adjust();
    }
  });
}

window.addEventListener(
  "resize",
  function (event) {
    resizeWebixComponent();
  },
  true
);

makeObservable(Context, {
  status: observable,
  setStatus: action,
  devices: observable,
  setDevices: action,
  infoCurrentDivice: observable,
  setCurrentDivice: action,
  deviceChartList: observable,
  chartsLength: observable,
  states: observable,
  osc_state: observable,
  isHalted: observable,
  isHistory: observable
});

// makeAutoObservable(Context);

// const CaptionObserver = observer(({  }) => {
//   Context.model.label = "";
//   useEffect(() => {
//     // console.log("Render CaptionObserver");
//     // console.log(Context.states.indexDevice);
//     let deviceItem = Context.model.device(Context.states.indexDevice);
//     if (deviceItem ) {
//       Context.model.label =  deviceItem.desc;
//     }
//   })
//   return (
//     <WebixComponent ui={{ "label": Context.model.label , "width":0, "view": "label", "css":"deviceLabel", "id":"descriptionDevice"}} />
//   );
// });

const label = {
  multi: false,
  rows: [
    { header: "panelA", id: "panelA", body: { template: "Default section" } },
    {
      header: "panelB",
      collapsed: true,
      id: "panelB",
      body: { id: "contentB", rows: [] },
    },
  ],
  on: {
    onAfterExpand: function (id) {
      var view = $$(id);
      if (id == "panelB" && !view.complete) {
        webix.ui([{ template: " Section B " }], $$("contentB"));
        view.complete = true;
      }
    },
  },
};

const labelSapce = {
  view: "toolbar",
  cols: [
    { view: "button", value: "but1" },
    {},
    { view: "button", value: "but2" },
  ],
};

const baseTeamplate = {
  rows: [
    {
      cols: [
        {
          id: "leftColumn",
          template: " ",
          width: 250,
        },
        {
          view: "resizer",
          // id:"resizer",
        },
        {
          rows: [
            {
              id: "headerTab",
              template: "",
              height: 38,
              // marginTop: 10,
              borderless: false,
            },
            // {
            //   id:"tabMenu",
            //   height: 44,
            //   template:"tabMenu"
            // },
            {
              id: "mainInfo",
              template: "mainInfo",
            },
          ],

          // width:150
        },
      ],
    },
    {
      id: "footer",
      template: "footer",
      height: 32,
    },
  ],
};

const b1 = {
  view: "button",
  id: "b1",
  value: "but1",
  click: (e) => {
    console.log(e);
    $$("b2").setValue("123");
    Context.setStatus("qwe1");
    let item = $$("deviceInit");
    console.log(item);
  },
};

const b2 = {
  view: "button",
  id: "b2",
  value: "but2",
  click: (e) => {
    console.log(e);
    $$("b1").setValue("123");
    // Context.status = "aaa";
    Context.setStatus("qwe2");
    console.log("Context.model.devices", Context.model.devices);
    Context.setDevices(Context.model.devices());
    const view = $$("abs1");
    webix.DragControl.addDrag(view.$view, {
      $dragDestroy: function () {
        return false;
      },
      $dragPos: (pos) => {
        var context = webix.DragControl.getContext();
        let control = webix.$$(context.source[0]);

        control.config.left = pos.x + context.x_offset;
        pos.x = control.config.left - webix.DragControl.left;
        control.config.top = pos.y + context.y_offset;
        pos.y = control.config.top - webix.DragControl.top;
      },
      $dragCreate: function (source, ev) {
        const el = webix.$$(ev);
        if (el && el !== view) {
          var pos = webix.html.pos(ev);
          var context = webix.DragControl.getContext();

          context.source = [el.config.id];
          context.from = view;
          context.y_offset = el.config.top - pos.y;
          context.x_offset = el.config.left - pos.x;

          return el.$view;
        }
        return false;
      },
    });
  },
};

const TeamplateLeftMenu = ({ id }) => {
  console.log("render TeamplateLeftMenu");
  useEffect(() => {
    console.log("useEffect TeamplateLeftMenu");
    // console.log(id);
    var element = document.getElementById(id);
    new ResizeSensor(element, function () {
      resizeWebixComponent();
    });
  });

  return (
    <div id={id}>
      <div className="headerlogo" />
      <MenuAccordion />
    </div>
  );
};

function App() {
  useEffect(() => {
    console.log("render App");
    let container = $$("leftColumn");
    console.log(container.$view);
    const root = ReactDOMClient.createRoot(container.$view);
    root.render(<TeamplateLeftMenu tab="home" id="leftMenuIDforResize12" />); // этот компонент обязательно должен иметь ID

    let container2 = $$("headerTab");
    const root2 = ReactDOMClient.createRoot(container2.$view);
    root2.render(<DeviceInfoCaption />);

    // let containerTabMenu = $$("tabMenu");
    // const rootTabMenu = ReactDOMClient.createRoot(containerTabMenu.$view);
    // rootTabMenu.render( <TabMenu/>);

    let containerMainInfo = $$("mainInfo");
    const rootMainInfo = ReactDOMClient.createRoot(containerMainInfo.$view);
    rootMainInfo.render(<ViewMain />);

    let containerFooter = $$("footer");
    const rootFooter = ReactDOMClient.createRoot(containerFooter.$view);
    rootFooter.render(<Footer />);
  });
  return (
    <div className="App">
      <WebixComponent ui={baseTeamplate} />
    </div>
  );
}

export default App;
