import Oscilloscope from './oscilloscope.mjs'
import { Param } from './param.mjs'

import { DeviceProvider } from "./services/fr_deviceprovider.mjs";
import { ParamProvider } from "./services/fr_paramprovider.mjs";
import {default as RequestHelper} from "./services/fr_requesthelper.mjs";

export class SysInterfacesEnum {
    static UNKNOWN = 0;
    static DEMO = 1;
    static UAVCAN = 2;
    static CANOPEN = 3;
    static MODBUS = 4;
    static CONNEX_MVCP = 5;

    static toString(arg) {
        switch (arg) {
            case SysInterfacesEnum.UNKNOWN: return '';
            case SysInterfacesEnum.DEMO: return 'DEMO';
            case SysInterfacesEnum.UAVCAN: return 'UAVCAN';
            case SysInterfacesEnum.CANOPEN: return 'CANOPEN';
            case SysInterfacesEnum.MODBUS: return 'MODBUS';
            case SysInterfacesEnum.CONNEX_MVCP: return 'CONNEX_MVCP';
        }
    }
}

export class Device {
    constructor() {
        this.sysTypeId = 0
        this.deviceId = 0
        this.name = '' // имя для класса однотипных устройств(DEVICE_NAME + HW_REV + SW_REV): DCDC00010002
        this.desc = ''
        this.image = 0
        this.info = ''
        this.osc = null
        this.interfaceName = 'Can';
        this.modules = []
        this.params = []
        this.enabled = true;

        this.deviceProvider = new DeviceProvider(RequestHelper);
        this.paramProvider = new ParamProvider(RequestHelper);
    }

    isValid() {
        if (this.modules.length == 0 || this.sysTypeId == 0 || this.name == '') {
            return false;
        }

        return true;
    }

    displayName() {
        if (!this.isValid()) 
            return "";

        if (this.deviceId === 0) 
            return this.name;
        else
            return this.name + " [" + Number(this.deviceId).toString(10).padStart(2, '0') + "]"     
    }
    
    module(moduleId) {
        let res = this.modules.find(item => item.id == moduleId);
        return res;
    }

    param(moduleId, paramId) {
        let res = this.params.find(item => item.moduleId == moduleId && item.id == paramId);
        return res;
    }
    paramByUid(uid) {
        let res = this.params.find(item => item.uid == uid);
        return res;
    }

    getOsc() {
        if (this.osc) {
            return this.osc;
        }
        
        return null;
    }

    async requestNewOsc(historyStep) {
        let oscHeader = await this.deviceProvider.reqOsc(this.sysTypeId, this.deviceId, undefined, historyStep);
        
        if (this.osc) {
            this.osc.stopDataStream()
        }

        this.osc = new Oscilloscope();
        this.osc.deserialize(oscHeader, this.sysTypeId, this.deviceId);

        return this.osc;
    }

    deserialize(dataObj) {
        this.name = dataObj.name;
        this.sysTypeId = dataObj.sys_type_id;
        this.deviceId = dataObj.id;
        this.desc = dataObj.desc;
        this.interface = dataObj.sys_type_id;
        this.interfaceName = SysInterfacesEnum.toString(dataObj.sys_type_id);
    }

    async load(deviceInfo) 
    {
        if (!deviceInfo) return;

        this.deserialize(deviceInfo);

        for (var ii = 0; ii < deviceInfo.modules.length; ii++) {
            let moduleInfo = deviceInfo.modules[ii];
            let paramInfoList = moduleInfo["params"]; 
            let module = this._createModuleObj(this.sysTypeId, this.deviceId, moduleInfo);

            for (var iii = 0; iii < paramInfoList.length; iii++) {
                let paramObj = paramInfoList[iii];
                let param = new Param();
                param.deserialize(paramObj, this.sysTypeId);
        
                // if (!param.isValid()) continue; // todo: uncomment when method isValid will be realised
        
                module.params.push(param);
                this.params.push(param);
            }

            this.modules.push(module);
        }

        this.enabled = true;
        return;
    }

    _createModuleObj(sysTypeId, deviceId, moduleInfo) {
        let res = new SysModule();
        res.deviceId = deviceId;
        res.sysTypeId = sysTypeId;
        res.deserialize(moduleInfo);

        return res;
    }
}

export class SysModule {
    constructor() {
        this.id = 0;
        this.sysTypeId = 0;
        this.deviceId = 0;
        this.name = '';
        this.desc = '';
        this.params = [];
    }

    deserialize(moduleInfo) {
        this.id = moduleInfo.id;
        this.name = moduleInfo.name;
        this.desc = moduleInfo.desc;
        return;
    }

    displayName() {
        if (this.deviceId === 0) 
            return this.name;
        else
            return this.name + " [" + Number(this.id).toString(10).padStart(2, '0') + "]"     
    }

}
