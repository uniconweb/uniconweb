import * as React from "react";
import { SciChartSurface } from "scichart/Charting/Visuals/SciChartSurface";
import { NumericAxis } from "scichart/Charting/Visuals/Axis/NumericAxis";
import { FastLineRenderableSeries } from "scichart/Charting/Visuals/RenderableSeries/FastLineRenderableSeries";
import { XyDataSeries } from "scichart/Charting/Model/XyDataSeries";
import { ZoomPanModifier } from "scichart/Charting/ChartModifiers/ZoomPanModifier";
import { RubberBandXyZoomModifier } from "scichart/Charting/ChartModifiers/RubberBandXyZoomModifier";
import { MouseWheelZoomModifier } from "scichart/Charting/ChartModifiers/MouseWheelZoomModifier";
import { XAxisDragModifier } from "scichart/Charting/ChartModifiers/XAxisDragModifier";
import { YAxisDragModifier } from "scichart/Charting/ChartModifiers/YAxisDragModifier";
import { EDragMode } from "scichart/types/DragMode";
import { ZoomExtentsModifier } from "scichart/Charting/ChartModifiers/ZoomExtentsModifier";
import { ChartModifierBase2D } from "scichart/Charting/ChartModifiers/ChartModifierBase2D";
import { SeriesSelectionModifier } from "scichart/Charting/ChartModifiers/SeriesSelectionModifier";
import { RightAlignedOuterVerticallyStackedAxisLayoutStrategy } from "scichart/Charting/LayoutManager/RightAlignedOuterVerticallyStackedAxisLayoutStrategy";
import { EAxisAlignment } from "scichart/types/AxisAlignment";

import { EllipsePointMarker } from "scichart";

import { ENumericFormat } from "scichart/types/NumericFormat";
import { EAutoRange } from "scichart/types/AutoRange";
import { SciChartLegend } from "scichart/Charting/Visuals/Legend/SciChartLegend";
import { LegendModifier } from "scichart/Charting/ChartModifiers/LegendModifier";
import {
  ELegendOrientation,
  ELegendPlacement,
} from "scichart/Charting/Visuals/Legend/SciChartLegendBase";
import { EResamplingMode } from "scichart/Charting/Numerics/Resamplers/ResamplingMode";

import { NumberRange } from "scichart/Core/NumberRange";
import { WaveAnimation } from "scichart/Charting/Visuals/RenderableSeries/Animations/WaveAnimation";
import { EZoomState } from "scichart/types/ZoomState";
import { EXyDirection } from "scichart/types/XyDirection";
import { EExecuteOn } from "scichart/types/ExecuteOn";
import { easing } from "scichart/Core/Animations/EasingFunctions";

// import { EllipsePointMarker } from "scichart/Charting/Visuals/PointMarkers/EllipsePointMarker";
import { RolloverModifier } from "scichart/Charting/ChartModifiers/RolloverModifier";
import { TSciChart } from "scichart/types/TSciChart";
import { SciChartOverview } from "scichart/Charting/Visuals/SciChartOverview";
import { SciChartVerticalGroup } from "scichart/Charting/LayoutManager/SciChartVerticalGroup";
// import { IXyDataSeriesOptions} from "scichart/Charting/Model/XyDataSeries";
import { useContext } from "react";
import { Context, setStyleByID } from "../Context";
import { BsXCircle } from "react-icons/bs";
import { BsArrowDownCircle } from "react-icons/bs";
import { BsArrowUpCircle } from "react-icons/bs";
import { closeOsc, stopDrawOsc } from "./oscilloscopecharts";

// import image from "./javascript-line-chart.jpg";
const LICENSE_KEY = Context.chartkey;
let visiblePointsX = 10000; // msec for x axis
let visiblePointsY = 2000;
const intervalAddPoint = 40;
const MAX_CHART_HEIGHT = "1300px";
const suffixChartID = "scichart-root";
const suffixChartOverviewID = "scichart-overview";
let chartSurfaces = new Map();

// const colorsArrDefaults = ["#f6bf02","#0aa547","#eb4646", "blue", "#368BC1", "#eeeeee", "#ff6600", "#9b2dce", "#228B22", "#ff0000","orange","#be0000", "white"];
const colorsTitleArr = [
  "black",
  "black",
  "black",
  "black",
  "black",
  "black",
  "white",
  "white",
  "white",
  "white",
  "white",
  "white",
];

let verticalGroup = new SciChartVerticalGroup();

class MyRubberBandZoomModifier extends RubberBandXyZoomModifier {
  // ChartModifierBase2D
  modifierMouseDown(args) {
    //  console.log("args.button = " + args.button);
    //  console.log("args.ctrlKey = " + args.ctrlKey);
    if (args.ctrlKey == true) {
      var pointTo = args.mousePoint;
      pointTo.x = args.mousePoint.x + 1000;
      //    this.performZoom(args.mousePoint, pointTo)
      var xAxis = this.parentSurface.getXAxisById(this.xAxisId);
      if (xAxis) {
        xAxis.zoomBy(-0.25, -0.25);
      }
    }

    if (args.shiftKey == true) {
      var xAxis = this.parentSurface.getXAxisById(this.xAxisId);
      if (xAxis) {
        xAxis.zoomBy(0.25, 0.25);
      }
    }

    args.handled = false;
    super.modifierMouseDown(args);
  }
}

const initSciChart = async (chartID, chartSettings, isMulty) => {
  const divElementId = chartID + "_" + suffixChartID;
  const divOverviewId = chartID + "_" + suffixChartOverviewID;
  var overviewElem = null;
  var toBeContinued = false;
  let _isYfreezed = false;
  

  SciChartSurface.setServerLicenseEndpoint("api/scichart");
  SciChartSurface.setRuntimeLicenseKey(LICENSE_KEY);

  const { sciChartSurface, wasmContext } = await SciChartSurface.create(
    divElementId
  );

  sciChartSurface.layoutManager.rightOuterAxesLayoutStrategy =
    new RightAlignedOuterVerticallyStackedAxisLayoutStrategy();
  const { selectedParams, paramsXY } = chartSettings;
  let { minY, maxY, timeX} = paramsXY;
  visiblePointsY = maxY - minY;
  visiblePointsX = timeX;

  const xAxis = new NumericAxis(wasmContext, {
    autoRange: EAutoRange.Once,
    axisAlignment: EAxisAlignment.Bottom,
    drawLabels: true,
    drawMajorGridLines: true,
    drawMinorGridLines: true,
    zoomExtentsToInitialRange: false,
    visibleRange: new NumberRange(0, visiblePointsX),
    maxAutoTicks: 20,
    isSorted: true,
    containsNaN: false,
  });

  sciChartSurface.xAxes.add(xAxis);

  // Optional: Add some interactivity modifiers to enable zooming and panning
  const cursor = new RolloverModifier({ modifierGroup: "first" });
  cursor.isEnabled = false;

  const lm = new LegendModifier({
    placement: isMulty ? ELegendPlacement.BottomLeft : ELegendPlacement.TopLeft,
    orientation: ELegendOrientation.Vertical,
    showLegend: true,
    showCheckboxes: true,
    showSeriesMarkers: true,
  });

  let xyDirection = isMulty
    ? EXyDirection.XDirection
    : EXyDirection.XyDirection;

  sciChartSurface.chartModifiers.add(
    //    new RubberBandXyZoomModifier({ xyDirection: EXyDirection.XyDirection, executeOn: EExecuteOn.MouseLeftButton }),
    new MouseWheelZoomModifier({ xyDirection: xyDirection }),
    new XAxisDragModifier({ dragMode: EDragMode.Panning }),
    new YAxisDragModifier({ dragMode: EDragMode.Scaling }),
    new ZoomExtentsModifier({
      isAnimated: true,
      animationDuration: 400,
      easingFunction: easing.outExpo,
    }),
    new ZoomPanModifier({
      executeOn: EExecuteOn.MouseRightButton,
      xyDirection: xyDirection,
    }),
    new MyRubberBandZoomModifier({
      xyDirection: xyDirection,
      executeOn: EExecuteOn.MouseLeftButton,
      receiveHandledEvents: true,
    }),
    new SeriesSelectionModifier(),
    lm,
    cursor
  );

  let yInitMin = [];
  let yInitMax = [];
  const ID_Y_AXIS = "yAxis";

  if (chartSettings) {
    let namesArr = [];
    let colorsArr = [];

    let i = 0;
    let discrParamsHeight = 30; //Math.min(10, 100/params.length) - 1;
    for (let param of selectedParams) {
      const displayName = param.userName || param.name;
      namesArr.push(displayName);
      colorsArr.push(param.color);

      i++;
      yInitMin[i] = param.isDiscrete ? 0 : minY;
      yInitMax[i] = param.isDiscrete ? 1 : maxY;

      if (i > 1 && !isMulty) continue;

      const yAxis = new NumericAxis(wasmContext, {
        id: ID_Y_AXIS + i,
        axisAlignment: EAxisAlignment.Right,
        autoRange: EAutoRange.Once,
        // visibleRange: new NumberRange(yInitMin[i], yInitMax[i]),
        // zoomExtentsToInitialRange : true,
        drawMinorGridLines: true,
        maxAutoTicks: 5,
        axisBorder: { borderTop: 0, borderBottom: 0 },
      });

      if (isMulty) {
        yAxis.labelProvider.formatLabel = (dataValue: number) =>
          param.isDiscrete ? dataValue.toFixed(0) : dataValue.toFixed(3);
        yAxis.stackedAxisLength = param.isDiscrete ? discrParamsHeight : "100%";
      }

      sciChartSurface.yAxes.add(yAxis);
      if (isMulty) {
        adjustMultyYAxeHeight(
          sciChartSurface,
          yAxis.id,
          yInitMin[i],
          yInitMax[i]
        );
      }
    }

    await parametriseSciChart(
      sciChartSurface,
      wasmContext,
      namesArr,
      colorsArr,
      isMulty
    );

  }

  if (!isMulty) {
    verticalGroup.addSurfaceToGroup(sciChartSurface); // Warning! This line of code break proper vert axis layouting

    setTimeout(() => {
      adjustSingleYAxeHeight(sciChartSurface, minY, maxY);
    }, 1000);
  }


  chartSurfaces.set(chartID, sciChartSurface);

  const makeYFreezed = (value) => {
    _isYfreezed = value;
  };

  const clearChart = (lineNum) => {
    let arrayLines = sciChartSurface.renderableSeries.items;
    if (lineNum === undefined) {
      for (let line of arrayLines) {
        line.dataSeries.clear();
      }
    } else {
      arrayLines[lineNum - 1].dataSeries.clear();
    }

    xAxis.visibleRange = new NumberRange(0, visiblePointsX);
  
  };


  const switchPointMarker = (value) => {
    let arrayLines = sciChartSurface.renderableSeries.items;
    const commonOptions = { width: 7, height: 7, strokeThickness: 1 };
    const invisibleOptions = { width: 0, height: 0, strokeThickness: 0 };
    for (let line of arrayLines) {
      try {
        if (value === true) {
          line.pointMarker = new EllipsePointMarker(wasmContext, {
            ...commonOptions,
            fill: line.color,
          });
        } else {
          if (line.pointMarker) {
            line.pointMarker = new EllipsePointMarker(wasmContext, {
              ...invisibleOptions,
              fill: line.color,
            });
          }
        }
      } catch (error) {
        console.error("Error toggling point marker:", error);
      }
    }
  };

  const addVarPoint = (x, y, line) => {
    let arrayLines = sciChartSurface.renderableSeries.items;
    arrayLines[line - 1].dataSeries.append(x, y);
    xAxis.visibleRange = new NumberRange(x - visiblePointsX, x);
    
  };

  const addVarPointRange = (xValues = [], yValues = [], line) => {
    let arrInd = line - 1;
    let arrayLines = sciChartSurface.renderableSeries.items;
    arrayLines[arrInd].dataSeries.appendRange(xValues, yValues);

    if (sciChartSurface.zoomState === EZoomState.UserZooming) {
      //    sciChartSurface.setZoomState(null);
      //    return;
    }

    let xAxe = sciChartSurface.xAxes.items[0];
    let startX = xValues[0];
    let endX = xValues[xValues.length - 1];
    //  let visiblePoints = endX;
    /*    
    if (xAxe.visibleRange.max < endX) {
      xAxe.visibleRange = new NumberRange(endX - visiblePoints, endX);
    }
*/

    if (xAxe.visibleRange.max < startX && toBeContinued === false) {
      return;
    }

    if (xAxe.visibleRange.min > startX || xAxe.visibleRange.max < endX) {
      if (startX + visiblePointsX > endX) {
        xAxe.visibleRange = new NumberRange(startX, startX + visiblePointsX);
      } else {
        xAxe.visibleRange = new NumberRange(
          endX - visiblePointsX / 2,
          endX + visiblePointsX / 2
        );
      }
    }

    const getMax = (numbers) => numbers.reduce((a, b) => Math.max(a, b));
    const getMin = (numbers) => numbers.reduce((a, b) => Math.min(a, b));

    let yAxe = isMulty
      ? sciChartSurface.yAxes.items[arrInd]
      : sciChartSurface.yAxes.items[0];

      var curMin = getMin(yValues),
          curMax = getMax(yValues);

    let zoomYMin = calcYMin(curMin, yAxe.visibleRange.min);
    let zoomYMax = calcYMax(curMax, yAxe.visibleRange.max);

    let yMin = yAxe.visibleRange.min;
    let yMax = yAxe.visibleRange.max;
  
    if (_isYfreezed === true) {
      const fixedRange = yMax - yMin;

      if (curMax > yMax) {
          yMax = curMax;
          yMin = yMax - fixedRange;
      } else if (curMin < yMin) {
          yMin = curMin;
          yMax = yMin + fixedRange;
      }
      yAxe.visibleRange = new NumberRange(yMin, yMax);
    } else {
      yMin = zoomYMin;
      yMax = zoomYMax;
    }

    yAxe.visibleRange = new NumberRange(yMin, yMax);
    yAxe.zoomExtentsRange = new NumberRange(zoomYMin, zoomYMax);

    visiblePointsY = yAxe.visibleRange.max - yAxe.visibleRange.min;
    toBeContinued = false;
 
    /*      
      if (!yAxe.visibleRange || yAxe.visibleRange.min > yMin[line] || yAxe.visibleRange.max < yMax[line]) {
        yAxe.visibleRange = new NumberRange(yMin[line], yMax[line]); 
      }
      if (!yAxe.zoomExtentsRange || yAxe.zoomExtentsRange.min > yMin[line] || yAxe.zoomExtentsRange.max < yMax[line]) {
        yAxe.zoomExtentsRange = new NumberRange(yMin[line], yMax[line]); 
      }
*/
  };

  const addVarPointMegaRange = (xValues = [], yValues = [], line) => {
    let arrInd = line - 1;
    let arrayLines = sciChartSurface.renderableSeries.items;
    arrayLines[arrInd].dataSeries.appendRange(xValues, yValues);

    let xAxe = sciChartSurface.xAxes.items[0];
    let startX = xValues[0];
    let endX = xValues[xValues.length - 1];

    if (xAxe.visibleRange.min > startX || xAxe.visibleRange.max < endX) {
      if (startX + visiblePointsX > endX) {
        xAxe.visibleRange = new NumberRange(startX, startX + visiblePointsX);
      } else {
        xAxe.visibleRange = new NumberRange(
          endX - visiblePointsX / 2,
          endX + visiblePointsX / 2
        );
      }
    }

    const getMax = (numbers) => numbers.reduce((a, b) => Math.max(a, b));
    const getMin = (numbers) => numbers.reduce((a, b) => Math.min(a, b));

    let yAxe = isMulty
      ? sciChartSurface.yAxes.items[arrInd]
      : sciChartSurface.yAxes.items[0];

    var curMin = getMin(yValues),
        curMax = getMax(yValues);

    let yMin = calcYMin(curMin, yAxe.visibleRange.min);
    let yMax = calcYMax(curMax, yAxe.visibleRange.max);
    
    yAxe.visibleRange = new NumberRange(yMin, yMax);
    yAxe.zoomExtentsRange = new NumberRange(yMin, yMax);
    visiblePointsY = yMax - yMin;
  };

  const calcYMax = (curMax, visibleYMax) => {
    let yMax = visibleYMax;
    
    let g = curMax < 0 ? -0.1 : 0.1;
    if (yMax < curMax + curMax * g) {
      g = curMax < 0 ? -0.5 : 0.5;
      yMax = curMax + g * curMax;
    }

    return yMax;    
  }

  const calcYMin = (curMin, visibleYMin) => {

    let yMin = visibleYMin;

    let g = curMin < 0 ? -0.1 : 0.12;
    if (yMin > curMin - g * curMin /* && curMin/yMin < 2*/) {
      g = curMin < 0 ? -0.5 : 0.5;
      yMin = curMin - g * curMin;
    }

    return yMin;
  }

  const zoomExtents = () => {
    sciChartSurface.zoomExtents();
   
  };

  const Scale = (valueX, valueY) => {
    if (valueX) {
      let xAxe = sciChartSurface.xAxes.items[0];
      visiblePointsX = xAxe.visibleRange.max - xAxe.visibleRange.min;
      let centerX = xAxe.visibleRange.min + visiblePointsX/2;
      // let factor = (valueX - visiblePointsX) / visiblePointsX;
      // xAxe.zoomBy(0, factor);
      xAxe.visibleRange = new NumberRange(
        centerX - valueX / 2,
        centerX + valueX / 2
      );
      visiblePointsX = xAxe.visibleRange.max - xAxe.visibleRange.min;
    }
    
    if (valueY) {
      let yAxe = sciChartSurface.yAxes.items[0];
      let visiblePointsY = yAxe.visibleRange.max - yAxe.visibleRange.min;
      let centerY = yAxe.visibleRange.min + visiblePointsY/2;

      // let factor = (valueY - visiblePointsY) / visiblePointsY;
      // yAxe.zoomBy(0, factor);

      yAxe.visibleRange = new NumberRange(
        centerY - valueY / 2,
        centerY + valueY / 2
      );
    }
  };

  const Adjust = () => {
    toBeContinued = true;
    const xAxe = sciChartSurface.xAxes.items[0];
    let xMax = 0;
    for (let line of sciChartSurface.renderableSeries.items) {
      if (line.dataSeries.xRange.max > xMax) {
        xMax = line.dataSeries.xRange.max;
      }
    }

    if (xMax > visiblePointsX) {
      xAxe.visibleRange = new NumberRange(
        xMax - visiblePointsX / 2,
        xMax + visiblePointsX / 2
      );
    }
  };

  const SwitchCursor = (value) => {
    cursor.isEnabled = value;
  };

  const SwitchOverview = async (isVisible) => {
    if (isVisible) {
      sciChartSurface.zoomExtents();
    } else {
      Adjust();
    }

    /*    
    var visibility = isVisible ? "visible" : "hidden";
    //  document.getElementById(divOverviewId).style["visibility"] = visibility;

    if (isVisible) {
      document.documentElement.style.setProperty("--overview-size", "70px");

      if (!overviewElem) {
        setTimeout(async () => {
          const ID_Y_AXIS = "yAxis";
          overviewElem = await SciChartOverview.create(
            sciChartSurface,
            divOverviewId,
            {
              secondaryAxisId: ID_Y_AXIS + "1",
              //  transformRenderableSeries: customTransformFunction,
            }
          );
        }, 100);
      }
    } else {
      document.documentElement.style.setProperty("--overview-size", "1px"); // Must be 1px, otherwise chart mouse control is broken
    }
*/
  };
  const saveCurrentZoomState = () => {
    const xVisibleRange = sciChartSurface.xAxes.items[0].visibleRange;
    console.log("xVisibleRange:", xVisibleRange);
    visiblePointsX = xVisibleRange.max - xVisibleRange.min;
    const yVisibleRange = sciChartSurface.yAxes.items.map(yAxis => yAxis.visibleRange);
    visiblePointsY = yVisibleRange[0].max - yVisibleRange[0].min;
    const state = {
      xMin: xVisibleRange.min,
      xMax: xVisibleRange.max,
      yVisibleRange: yVisibleRange.map(range => ({ min: range.min, max: range.max }))
    };
    console.log(`Saving state for chart ${chartID}:`, state);
    Context.saveZoomState(chartID, state);
    Context.osc_state.display_resolution_ms = visiblePointsX;
    paramsXY.timeX = visiblePointsX;
    Context.osc_state.yAxisRange = {
      minY: yVisibleRange[0].min,
      maxY: yVisibleRange[0].max
    };
    makeYFreezed(true);
    return state;
  };

  const restorePreviousZoomState = () => {
    let charts = Context.currDeviceCharts();
    charts.forEach((chartID) => {
      let state = Context.getZoomState(chartID);
      if (state) {
        console.log(`Restoring previous zoom state for chart ${chartID}: `, state);
        updateYAxis(state.yVisibleRange[0].min, state.yVisibleRange[0].max);
        updateXAxis(state.xMin, state.xMax);
        makeYFreezed(true);
      }
    });
  };

  const updateYAxis = (minY, maxY) => {
    const yAxis = sciChartSurface.yAxes.items[0];
    yAxis.visibleRange = new NumberRange(minY, maxY);
//  yAxis.zoomExtentsRange = new NumberRange(minY, maxY);
   
  };
  const updateXAxis = (minX, maxX) => {

    const xAxis = sciChartSurface.xAxes.items[0];
    xAxis.visibleRange = new NumberRange(minX, maxX);
  //xAxis.zoomExtentsRange = new NumberRange(minX, maxX);
  };
  
  return {
    wasmContext,
    sciChartSurface,
    controls: {
      addVarPoint,
      addVarPointRange,
      addVarPointMegaRange,
      zoomExtents,
      clearChart,
      Scale,
      Adjust,
      SwitchCursor,
      switchPointMarker,
      SwitchOverview,
      saveCurrentZoomState, 
      restorePreviousZoomState,
    },
  };
};

function adjustMultyYAxeHeight(sciChartSurface, axisId, minY, maxY) {
  setTimeout(() => {
    let yAxis = sciChartSurface.yAxes.getById(axisId);
    if (yAxis) {
      yAxis.visibleRange = new NumberRange(minY, maxY);
    }
  }, 1000);
}

function adjustSingleYAxeHeight(sciChartSurface, minY, maxY) {
  let yAxis = sciChartSurface.yAxes.get(0);
  let isData = false;

  for (let line of sciChartSurface.renderableSeries.items) {
    if (line.dataSeries.xRange.max > 0) {
      isData = true;
      break;
    }
  }
  if (isData === false) {
    yAxis.visibleRange = new NumberRange(minY, maxY);
  }
}

async function parametriseSciChart(
  sciChartSurface /*: SciChartSurface*/,
  wasmContext /*: TSciChart*/,
  namesArr = [],
  colorsArr,
  isMulty
) {
  const seriesArr = [];

  for (let k = 0; k < namesArr.length; k++) {
    seriesArr.push({
      color: colorsArr[k],
      name: namesArr[k],
      colorText: colorsTitleArr[k],
    });
  }

  if (sciChartSurface.renderableSeries) {
    sciChartSurface.renderableSeries.clear();
  }

  const commonOptions = { width: 7, height: 7, strokeThickness: 1 };
  const arrayLines = [];

  for (let m = 0; m < seriesArr.length; m++) {
    const axis_id = "yAxis" + (isMulty ? String(m + 1) : String(1));
    arrayLines.push(
      new FastLineRenderableSeries(wasmContext, {
        yAxisId: axis_id,
        stroke: seriesArr[m].color,
        strokeThickness: 2,
        dataSeries: new XyDataSeries(wasmContext, {
          dataSeriesName: seriesArr[m].name,
        }),
        animation: new WaveAnimation({
          zeroLine: -1,
          pointDurationFraction: 0.5,
          duration: 100,
        }),
        resamplingMode: EResamplingMode.None,
        onSelectedChanged: (sourceSeries) => {
          sourceSeries.strokeThickness = sourceSeries.isSelected ? 4 : 2;
        },
        onHoveredChanged: (sourceSeries, isHovered) => {
          sourceSeries.stroke = isHovered ? "white" : seriesArr[m].color;
        },
      })
    );

    arrayLines[m].rolloverModifierProps.tooltipTitle = seriesArr[m].name;
    arrayLines[m].rolloverModifierProps.tooltipLabelX = "X";
    arrayLines[m].rolloverModifierProps.tooltipLabelY = "Y";
    arrayLines[m].rolloverModifierProps.markerColor = seriesArr[m].color;
    arrayLines[m].rolloverModifierProps.tooltipColor = seriesArr[m].color;
    arrayLines[m].rolloverModifierProps.tooltipTextColor =
      seriesArr[m].colorText;

    await sciChartSurface.renderableSeries.add(arrayLines[m]);

    //  let yAxe = sciChartSurface.getYAxisById(axis_id)
  }
}

export default function Chart(props) {
  const [loaded, setLoaded] = React.useState(false);
  const [inited, setInited] = React.useState(false);
  const [sciChartSurface, setSciChartSurface] = React.useState(null);
  const [wasmContext, setWasmContext] = React.useState(null);
  const [chartHeight, setChartHeight] = React.useState(MAX_CHART_HEIGHT);
  const [visibility, _setVisibility] = React.useState("visible");
  const [controls, setControls] = React.useState({
    startDemo: () => {},
    stopDemo: () => {},
    addVarPoint: () => {},
    addVarPointRange: () => {},
    clearChart: () => {},
    Scale: (visiblePointsX, visiblePointsY) => {},
    SwitchCursor: (value) => {},
    switchPointMarker: (value) => {},
    SwitchOverview: (value) => {},
    setVisibility: (value) => {},
    saveCurrentZoomState: () => {}, 
  });

  const removeChart = () => {
    if (sciChartSurface) {
      sciChartSurface.delete(true);
    }

    let indexDevice = Context.states.indexDevice;
    stopDrawOsc(Context.currentDevice);
    Context.removeChart(indexDevice, props.id);
    
  };

  const closeButton = (
    <div
      style={{
        position: "absolute",
        top: 10,
        right: 80,
        zIndex: 100,
        cursor: "pointer",
        transform: "scale(2)",
      }}
      onClick={removeChart}
    >
      <BsXCircle size={17} />
    </div>
  );

  const toggleChartDisplay = () => {
    setChartHeight(
      chartHeight !== MAX_CHART_HEIGHT ? MAX_CHART_HEIGHT : "50px"
    );
  };

  const collapseButton = (
    <div
      style={{
        position: "absolute",
        top: 10,
        right: 120,
        zIndex: 100,
        cursor: "pointer",
        transform: "scale(2)",
      }}
      onClick={toggleChartDisplay}
    >
      {chartHeight === MAX_CHART_HEIGHT ? (
        <BsArrowDownCircle size={17} />
      ) : (
        <BsArrowUpCircle size={17} />
      )}
    </div>
  );

  const chartContainerStyle = {
    maxHeight: chartHeight,
    transition: "max-height 0.5s",
  };

  const setVisibility = (value) => {
    _setVisibility(value);

  };

  const _switchChartElement = (visibility = "visible") => {
    let elemId = props.id;
    setStyleByID(elemId, "visibility", visibility);
  };

  React.useEffect(() => {
    _switchChartElement(visibility);
  }, [visibility]);

  React.useEffect(() => {
    (async () => {
      if (sciChartSurface?.renderableSeries) {
        for (let line of sciChartSurface.renderableSeries.items) {
          line.dataSeries.clear();
        }
        sciChartSurface.renderableSeries.clear();
      }

      sciChartSurface?.delete();
      setSciChartSurface(null);

      setInited(false);
      setLoaded(false);
      /*
      const newControls = { setVisibility };
      setControls(newControls);
      Context.chartActions[props.id] = newControls;
*/
      if (!props.isVisibile) return;

      let isMultyMode = props.params?.selectedParams?.some((param) => param.isDiscrete) ?? false;

      setTimeout(async () => {
        const res = await initSciChart(props.id, props.params, isMultyMode); // отложенная опреацию создания необходима, так как диаграммы требует фиксированной высоты div блока, которая должна успеть рассчитаться
        if (!res) return;

        setInited(true);

        const newControls = Object.assign({}, res.controls, { setVisibility });
        setControls(newControls);
        Context.chartActions[props.id] = newControls;
        setSciChartSurface(res.sciChartSurface);
        setWasmContext(res.wasmContext);
     }, 10);
   })();

    return () => {
      // Delete sciChartSurface on unmount component to prevent memory leak
      verticalGroup = new SciChartVerticalGroup();
      chartSurfaces = new Map();
      sciChartSurface?.delete();
    };
  }, [props.params, props.chartCount, props.isVisibile]);

  
  React.useEffect(() => {
    if (!sciChartSurface || !props.isVisible) return;
    console.log(`Saving zoom state for chart ${props.id}`);
    controls.saveCurrentZoomState();
  }, [props.visibility]);

  React.useEffect(() => {
    (async () => {
      if (!inited) {
        return;
      }

      if (props.loadData) {
        if (!loaded) {
          setTimeout(async () => {
            await props.loadData(props.id);
            setLoaded(true);
          }, 500);
        }
      } else {
        setLoaded(true);
      }

      if (loaded && props.startData) {
        setTimeout(async () => {
          await props.startData(props.id);
        }, 2000);
      }

    })();
  }, [inited, loaded]);

  const divElementId = props.id + "_" + suffixChartID;
  const divOverviewId = props.id + "_" + suffixChartOverviewID;
  // <div id={divOverviewId} className="chartoverview"></div>

  return (
    <div id={props.id} className="chartContainer" style={chartContainerStyle}>
      {closeButton}
      {collapseButton}
      <div id={divElementId} className="chart"></div>
    </div>
  );
}

function SyncCharts(chartSettings) {
  verticalGroup.synchronizeAxisSizes();

  let xAxes = [];
  chartSurfaces?.values().forEach((chart) => {
    chart.zoomExtents();
    let xAxis = chart.xAxes.items[0];
    xAxes.push(xAxis);

//    let yAxis = chart.yAxes.items[0];
//  yAxis.growBy = new NumberRange(0.2, 0.2);
  });

  xAxes.forEach((xAxis) => {
    xAxis.visibleRangeChanged.subscribe((data1) => {
      xAxes.forEach((xAxis2) => {
        xAxis2.visibleRange = data1.visibleRange;
      });
    });
  });

}

export { SyncCharts };
