import align from "../styles/img/AlignToGrid.svg";
import 	bottom from "../styles/img/vertical-bottom.svg";
import 	button from "../styles/img/button.svg";
import 	centerX from "../styles/img/horizontal-center.svg";
import 	centerY from "../styles/img/vertical-middle.svg";
import 	checkbox from "../styles/img/checkbox.svg";
import 	colorpicker from "../styles/img/colorpicker.svg";
import 	combo from "../styles/img/combo.svg";
import 	counter from "../styles/img/counter.svg";
import 	datepicker from "../styles/img/datepicker.svg";
import 	daterangepicker from "../styles/img/daterangepicker.svg";
import 	deleteImg from "../styles/img/Delete.svg";
import 	help from "../styles/img/Helper.svg";
import 	icon from "../styles/img/icon.svg";
import 	label from "../styles/img/label.svg";
import 	left from "../styles/img/horizontal-left.svg";
import 	multicombo from "../styles/img/multicombo.svg";
import 	multiselect from "../styles/img/multiselect.svg";
import 	radio from "../styles/img/radio.svg";
import 	rangeslider from "../styles/img/rangeslider.svg";
import 	redo from "../styles/img/Redo.svg";
import 	richselect from "../styles/img/richselect.svg";
import 	right from "../styles/img/horizontal-right.svg";
import 	search from "../styles/img/search.svg";
import 	segmented from "../styles/img/segmented.svg";
import 	select from "../styles/img/select.svg";
import 	slider from "../styles/img/slider.svg";
import 	spaceX from "../styles/img/distribute-horisontally.svg";
import 	spaceY from "../styles/img/distribute-vertically.svg";
import 	switchImg from "../styles/img/switch.svg";
import 	tabbar from "../styles/img/tabbar.svg";
import 	text from "../styles/img/text.svg";
import 	textarea from "../styles/img/textarea.svg";
import 	toggle from "../styles/img/toggle.svg";
import 	top from "../styles/img/vertical-top.svg";
import 	undo from "../styles/img/Undo.svg";


export const svgs = {
	align,
	bottom,
	button,
	centerX,
	centerY,
	checkbox,
	colorpicker,
	combo,
	counter,
	datepicker,
	daterangepicker,
	delete: deleteImg,
	help,
	icon,
	label,
	left,
	multicombo,
	multiselect,
	radio,
	rangeslider,
	redo,
	richselect,
	right,
	search,
	segmented,
	select,
	slider,
	spaceX,
	spaceY,
	switch: switchImg,
	tabbar,
	text,
	textarea,
	toggle,
	top,
	undo
};