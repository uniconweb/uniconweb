import "webix/webix.css";
import ReactDOM from "react-dom";
import React, { useEffect, useState, useContext } from "react";
import WebixComponent from "./WebixComponent";
import { $$ } from "@xbs/webix-pro";
import { Context } from "./Context";
import moment from "moment";
import { ValueFormatEnum } from "./data_model/param.mjs";
import { observer } from "mobx-react";
import * as webix from "@xbs/webix-pro/webix.js";
import { usePLC } from "./PLCContext";

let actions = {};

function importPLCFunctions(plcFunctions) {
  actions = plcFunctions;
}

function clear() {
  let form = $$("parametersGrid");
  if (!form) return;
  let elements = form.getChildViews();
  let len = elements.length;
  for (let i = 0; i < len; i++) {
    let elem = elements[0];
    form.removeView(elem);
  }
}

function frequencyByMode(editMode, turboMode) {
  return editMode ? (turboMode ? 2 : 1) : turboMode ? 8 : 3;
}

function changeFrequencyByMode(editMode, turboMode) {
  openParamStreams();
}

async function closeParamStreams(id) {
  let tree = $$("parametersGrid");
  let arr = [];
  if (id) {
    arr.push(id);
  } else {
    arr = tree.getOpenItems();
  }

  for (let i = 0; i < arr.length; i++) {
    let groupId = arr[i];
    let rows = getItems(tree, groupId);
    rows.forEach(async function (item, index, array) {
      let row = tree.getItem(item.id);
      await Context.model.closeParamStream(row.param);
    });
  }
}

async function closeAllStreams() {
  if (Context?.currentDevice == undefined) {
    return; // not inited yet
  }
  await Context.model.forceCloseAllStreams(
    Context.currentDevice?.deviceId ?? 0
  );
}

function openParamStreams(frequency) {
  if (!frequency) {
    frequency = frequencyByMode(_editMode, _turboMode);
  }

  let tree = $$("parametersGrid");
  let arr = tree.getOpenItems();

  for (let i = 0; i < arr.length; i++) {
    let groupId = arr[i];
    let rows = getItems(tree, groupId);
    rows.forEach(function (item, index, array) {
      let row = tree.getItem(item.id);
      openParamStream(row.param, row.id, frequency);
    });
  }

  Context.model.startParamStreams(frequency);
}

async function openParamStream(param, rowId, frequency) {
  /*  
  let value = param.lastValue();
  if (value.value == undefined) {
    value = await param.currentValue();
  }
  
  if (value.value != undefined) {
    updateRowValue(value, param, rowId);
  }
*/
  const stream = Context.model.openParamStream(param, frequency);
  const reader = stream?.getReader();

  const updateValues = async () => {
    const { done, value: values } = await reader?.read();

    if (done) {
      return;
    }
    if (values && values.length > 0) {
      let value = values[values.length - 1];
      updateRowValue(value, param, rowId);
      setTimeout(await updateValues());
    }
  };

  setTimeout(await updateValues());
}

let _editMode = false;
let _turboMode = false;

function updateRowValue(value, param, rowId) {
  var timeStr = moment(value.valueTime).format("hh:mm:ss.SSS");
  // var dateStr = "10:30";
  let grid = $$("parametersGrid");

  grid.updateItem(rowId, {
    value: param.displayValue(value.value, true),
    time: timeStr,
  });
}

function setItemsCheckFlag(tree, rowId, checkFlag) {
  let row = tree.getFirstChildId(rowId);
  if (row) {
    let item;
    while (row) {
      item = tree.getItem(row);
      item.chart = checkFlag;
      tree.refresh(row);
      row = tree.getNextSiblingId(row);
    }
  }
}

function updateModuleCheckState(tree, moduleId) {
  let module = tree.getItem(moduleId);
  let allChecked = true;
  let row = tree.getFirstChildId(moduleId);
  while (row) {
    let item = tree.getItem(row);
    if (!item.chart) {
      allChecked = false;
      break;
    }
    row = tree.getNextSiblingId(row);
  }
  module.chart = allChecked ? 1 : 0;
  tree.refresh(moduleId);
}

function getItems(tree, rowId) {
  let row = tree.getFirstChildId(rowId);
  let rowsList = [];
  if (row) {
    rowsList.push({ id: row, row: tree.getItem(row) });
    while ((row = tree.getNextSiblingId(row))) {
      rowsList.push({ id: row, row: tree.getItem(row) });
    }
  }
  return rowsList;
}

function mark_items_editable(value, config) {
  let res = "no_editable";
  if (config.rw == "W") res = "editable";
  return res;
}

function mark_items_readable(value, config) {
  let res = "no_editable";
  return res;
}

function getUImainMenu(props) {
  /*  
  webix.UIManager.addHotKey("any", function(view){
    var pos = view.getSelectedId();
    view.edit(pos);
  }, $$("treetable"));
*/

  return {
    view: "treetable",
    id: "parametersGrid",
    type: {
      height: "auto",
    },
    rowHeight: 25,
    rowLineHeight: 25,
    select: true,
    columns: [
      {
        id: "name",
        width: "300",
        template: "{common.treetable()} #name#",
        header: [
          // "Name",
          { content: "textFilter", placeholder: " Name" },
        ],
      },
      {
        id: "value",
        header: "Value",
        width: "170",
        /*cssFormat:mark_items_editable,*/ editor: "text",
      },
      { id: "dimension", header: "Dimension", width: "80" },
      { id: "time", header: "Time" },
      {
        id: "chart",
        header: "Show",
        width: "60",
        template: "{common.checkbox()}",
      },
      //    { id: "numchart", header: "Trend", width: "40" },
      {
        id: "desc",
        header: "Description",
        fillspace: true,
      },
    ],
    select: "cell",
    navigation: true,
    editable: true,
    editaction: "custom",

    on: {
      onItemClick: function (id) {
        let item = $$("parametersGrid").getItem(id);
        let cellParam = item.param;

        if (cellParam != undefined) {
          if (!cellParam.isReadOnly() & _editMode) {
            this.editCell(id, "value");
          }
        }
        if (id.column == "chart") {
          this.getItem(id.row).chart = !this.getItem(id).chart;
          this.refresh(id.row);
        }
      },

      onBeforeEditStart: function (id) {
        let item = $$("parametersGrid").getItem(id.row);
        let cellParam = item.param;

        if (id.column === "value" && cellParam.rw == "W") {
          // let currentEd = this.getColumnConfig(id.column).editor;
          if (cellParam.valueFormat == ValueFormatEnum.Text) {
            let column = this.getColumnConfig(id.column);
            column.collection = [];
            let val;
            for (let key in cellParam.valueTexts) {
              val = cellParam.valueTexts[key];
              column.collection.push({ id: val, value: val });
            }
            column.editor = "richselect";
          } else {
            this.getColumnConfig(id.column).editor = "text"; // "inline-text";
          }
        }
      },

      /*      
      onAfterEditStart:function(id){
        let item = $$("parametersGrid").getItem(id);
        console.log(item.param);
      },
*/
      onAfterEditStop: function (state, editor, ignoreUpdate) {
        if (state.value === state.old) {
          return;
        }

        let item = $$("parametersGrid").getItem(editor.row);
        let cellParam = item.param;

        let value;
        switch (cellParam.valueFormat) {
          case ValueFormatEnum.Text:
            {
              for (let key in cellParam.valueTexts) {
                if (cellParam.valueTexts[key] === state.value) {
                  value = Number(key).toString();
                  break;
                }
              }
            }
            break;
          default:
            value = state.value.toString();
        }
        console.log("cellParam", cellParam);
        cellParam.asyncSetValue(value);
        if (cellParam.lastError < 0) {
          //todo cancel editing value
          let value = cellParam.lastValue();
          updateRowValue(value, cellParam, editor.row);
        }
      },
      onAfterFilter: function (id) {
        openParamStreams();
      },
      onAfterClose: function (id) {
        closeParamStreams(id);
      },
      onAfterOpen: function (id) {
        let tree = $$("parametersGrid");
        let arr = tree.getOpenItems();
        let index = arr.indexOf(id);
        arr.splice(index, 1);
        arr.forEach(function (item, index, array) {
          tree.close(item);
        });

        openParamStreams();

        // Здесь представлены разные варианты редактирования ячеек в параметрах
        // let item4 = tree.getItem(item.row.id);
        // let editItem = false;
        // if (item4.rw == 'W') {
        //   console.log("play RW-W");
        //   // tree.updateItem(item.row.id, item4.id);
        //   console.log(item4);
        //   // tree.validateEditor();
        //   editItem = true;
        //   // tree.editCell(item.row.id, item4.id, true, true);
        //   // tree.getColumnConfig("value").editable = false;
        //   // tree.refreshColumns();
        //   tree.edit({
        //     row:item4.id,
        //     column:"value",
        //     editable:true,
        //     // editor:"popup",
        //   });
        //   tree.refresh();
        // }
        // if (editItem) tree.editCell(item.row.id, item4.id, false);
      },
    },
  };
}

function designTemplate(props) {
  return {
    rows: [toolBar(), getUImainMenu(props)],
  };
}

const updateParameters = (indexDevice) => {
  ClearTableParams();
  let pGrid = $$("parametersGrid");
  let deviceItem = Context.model.m_devices[indexDevice];
  let dt1 = [];
  if (deviceItem) {
    let dtt = [];
    let i = 1;
    let j = 1;
    let deviceName = deviceItem.displayName();
    let infoCurrentDivice =
      deviceItem.displayName() // + ". Channel: " + deviceItem.interfaceName;

    Context.states.indexDevice = indexDevice;
    let desc = $$("descriptionDevice");
    if (desc) {
      desc.setValue(infoCurrentDivice);
    }
    deviceItem.modules.forEach(function (item, index, array) {
      dtt = [];
      item.params.forEach(function (itemP, indexP, array) {
        infoCurrentDivice =
          infoCurrentDivice +
          " [" +
          Number(itemP.deviceId).toString(10) +
          "]" +
          "</br>Channel: " +
          item.interfaceName;
        const uniqueId = itemP.uniqueId();
        dtt.push({
          ID: itemP.ID,
          id: itemP.uniqueId(),
          //id: "m" + i, // name: itemP.moduleId,  //"[" + itemP.deviceId + "] " + item.name + " [" + itemP.moduleId + "]",
          name: itemP.displayName(),
          value: " ",
          dimension: itemP.valueUnit,
          time: " ",
          desc: itemP.desc,
          chart: actions.selectedParameters[uniqueId] ? 1 : 0,
          numchart: "",
          param: itemP,
          rw: itemP.rw,
          deviceName: deviceName,
          deviceId: itemP.ID.deviceId,
          module: item.displayName(),
          uniqueId: itemP.uniqueId(),
        });
        i++;
      });
      const moduleUniqueId = `${item.deviceId}-${item.id}`;
      dt1.push({
        id: "modul" + j,
        moduleUniqueId: moduleUniqueId,
        value: "",
        name:
          item.name +
          " [" +
          Number(item.id).toString(10).padStart(2, "0") +
          "]",
        displayName: item.displayName(),
        open: false,
        rw: "R",
        data: dtt,
        isModule: true,
        chart: item.params.every(
          (param) => actions.selectedParameters[param.uniqueId()]
        ) ? 1 : 0,
        deviceName: deviceName,
        moduleInfo: {...item, deviceName: deviceName, displayName: item.displayName(),
        },
      });

      j++;
    });

    // Context.elements.menuTop.setState((state, props) => ({
    //         // dt: [{"id":"can","programmInt":"Can", "open":"false", "data":dt1}]
    //         dt: {"id":"can","data":dt1}
    //  }));
  }
  //  console.log(dt1);
  return dt1;
};

const storeCurrentCheckboxStates = (module, state) => {
  actions.setSelectedParameters((prev) => {
    const newStates = { ...prev };
    module.params.forEach((param) => {
      const uniqueIdParam = param.uniqueId();
      const paramID = {
        sysTypeId: param.ID.sysTypeId,
        deviceId: param.ID.deviceId,
        moduleId: param.ID.moduleId,
        paramId: param.ID.paramId,
      };

      if (state) {
        if (!newStates.hasOwnProperty(uniqueIdParam)) {
          newStates[uniqueIdParam] = paramID;
          actions.setShouldSaveConfig(true);
        }
      } else {
        delete newStates[uniqueIdParam];
        actions.setShouldSaveConfig(true);
      }
    });
    return newStates;
  });
};

const ClearTableParams = () => {
  let tree = $$("parametersGrid");
  if (tree) {
    let arr = tree.getOpenItems();
    arr.forEach(function (item) {
      tree.close(item);
    });
    tree.clearAll();
  }
};

const toolBar = () => {
  return {
    view: "toolbar",
    // css:"webix_dark",
    cols: [
      {
        view: "toggle",
        name: "buttonEdit",
        label: "Edit",
        width: 100,
        align: "left",
        // css:"webix_primary" ,
        click: function (id, event) {
          _editMode = !_editMode;

          let tree = $$("parametersGrid");
          let column = tree.getColumnConfig("value");

          column.cssFormat = _editMode ? mark_items_editable : mark_items_readable;
          tree.refresh();
          changeFrequencyByMode(_editMode, _turboMode);
        },
      },
      {
        view: "toggle",
        name: "buttonTurbo",
        label: "Turbo",
        width: 100,
        align: "center",
        click: function (id, event) {
          _turboMode = !_turboMode;
          changeFrequencyByMode(_editMode, _turboMode);
        },
      },
      {},
    ],
  };
};

const selectedModulesId = (selectedParameters) => {
  const moduleParamsMap = {};

  Object.values(Context.model.m_devices).forEach((device) => {
    device.modules.forEach((module) => {
      const moduleUniqueId = `${module.deviceId}-${module.id}`;
      moduleParamsMap[moduleUniqueId] = module.params.map((param) =>
        param.uniqueId()
      );
    });
  });

  const selectedModules = {};
  Object.keys(moduleParamsMap).forEach((moduleUniqueId) => {
    const allParamsSelected = moduleParamsMap[moduleUniqueId].every((paramId) =>
      selectedParameters.hasOwnProperty(paramId)
    );
    if (allParamsSelected) {
      moduleParamsMap[moduleUniqueId].forEach((paramId) => {
        if (!selectedModules.hasOwnProperty(paramId)) {
          selectedModules[paramId] = selectedParameters[paramId];
        }
      });
    }
  });

  return selectedModules;
};

const ParametersViewObserver = observer(() => {
  const [dataTable, setData] = useState([]);
  const { selectedParameters, addParameter, removeParameter } = usePLC();
  const plcFunctions = usePLC();

  const onCheckHandler = function (id, colId, state) {
    const item = this.getItem(id);
    let modulRegexpArr = id.match(/modul(\d{1,4})/);
    if (modulRegexpArr) {
      setItemsCheckFlag(this, id, state);
      if (item && item.moduleInfo) {
        if (item.moduleInfo.params) {
          storeCurrentCheckboxStates(item.moduleInfo, state);
        }
      } else {
        console.log("No module information found for item:", item);
      }
    } else {
      if (state) {
        addParameter(item.param);
      } else {
        removeParameter(item.param);
      }
      let parentId = this.getParentId(id);
      updateModuleCheckState(this, parentId);
    }
    this.refresh();
  };

  useEffect(() => {
    const grid = $$("parametersGrid");
    if (grid) {
      grid.attachEvent("onCheck", onCheckHandler);
    }
    return () => {
      grid.detachEvent("onCheck", onCheckHandler);
    };
  }, []);

  useEffect(() => {
    importPLCFunctions(plcFunctions);
    const updParamStreams = async () => {
      await closeAllStreams();
      let dt = updateParameters(Context.states.indexDevice);
      $$("parametersGrid").parse(dt);
    };

    updParamStreams();

    const handleTabViewChange = async () => {
      if (Context.states.currTabViewId !== "devicesParametersTab") {
        await closeAllStreams();
      } else {
        setTimeout(() => {
          openParamStreams();
        }, 0);
      }
    };

    handleTabViewChange();
  }, [
    Context.states.indexDevice,
    Context.devices,
    Context.updateModel,
    Context.states.currTabViewId,
  ]);

  useEffect(() => {
    const syncCheckboxStatesWithSelectedParams = () => {
      const newSelectedParameters = { ...selectedParameters };

      const selectedModules = selectedModulesId(selectedParameters);
      Object.assign(newSelectedParameters, selectedModules);
      actions.setSelectedParameters((prevState) => {
        if (
          JSON.stringify(prevState) !== JSON.stringify(newSelectedParameters)
        ) {
          return newSelectedParameters;
        }
        return prevState;
      });
    };

    syncCheckboxStatesWithSelectedParams();
  }, [selectedParameters, actions.setSelectedParameters]);

  return <WebixComponent ui={designTemplate()} />;
});

function ViewDevicesParameters(props) {
  return (
    <div id={props.id} className="pages">
      {/* <WebixComponent ui={toolBar()} /> */}
      <ParametersViewObserver />
    </div>
  );
}

export default ViewDevicesParameters;
