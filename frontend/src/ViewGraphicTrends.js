import "webix/webix.css";
import { $$, template } from "@xbs/webix-pro";
import * as webix from "@xbs/webix-pro/webix.js";
import WebixComponent, { scroll } from "./WebixComponent";
import React from "react";

const toolBar = () => {
  return {
    view: "toolbar",
    // id: "myToolbar",
    cols: [
      {
        view: "button",
        value: "Add Chart",
        width: 100,
        align: "left",
        click: function (id, event) {
          // addButtonClick();
        },
      },
      {
        view: "button",
        value: "Remove Chart",
        autowidth: true,
        align: "center",
        click: function (id, event) {
          // removeButtonClick();
        },
      },

      {
        view: "button",
        value: "Start osc",
        autowidth: true,
        align: "center",
        click: async function (id, event) {
        },
      },
      {
        view: "button",
        value: "Stop osc",
        autowidth: true,
        align: "center",
        click: async function (id, event) {
          console.log("Stop osc");
        },
      },
    ],
  };
};

export default class ViewGraphicTrends extends React.Component {
  constructor(props) {
    super(props);
    console.log("create ViewGraphicTrends");
  }

  //visibility:"hidden", display:"none"
  render() {
    return (
      <div id="ViewGraphicTrends">
        <WebixComponent ui={toolBar()} />
        {/*<Chart id="chartTrends" title="&nbsp;" addFunction={addFunction} />*/}
      </div>
    );
  }
}

// export default MenuCenter;
