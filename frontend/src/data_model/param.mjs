import { ParamProvider } from "./services/fr_paramprovider.mjs";
import {default as RequestHelper} from "./services/fr_requesthelper.mjs";
import moment from "moment";

const STREAM_BUFFER_OBJECTS = 1;
const INT_MIN = -2147483648;
const INT_MAX = 2147483648;
const MAX_VALUE_ACTUALITY_MS = 1000;

export class Param {
    constructor() {
        this.id = 0;
        this.ID = {
            sysTypeId : 0,
            deviceId: 0,
            moduleId: 0,
            paramId: 0
        }
        this.deviceId = 0;
        this.moduleId = 0;
        this.name = '';
        this.desc = '';
        this.rw = 'R';
        this.valueUnit = '';
        this.valueFormat = 0;
        this.valueScale = 0.0;
        this.valueTexts = [];
        
        this.value = new ParamValue();

        this.stream = null;
        this.buffer = [];
        this.buffIndex = 0;
        this.buffObjectCount = STREAM_BUFFER_OBJECTS;

        this.lastError = 0;
        this.valueCounter = 0;

        this.paramProvider = new ParamProvider(RequestHelper);

        // Переменные для измерения производительности
        this._byteCount = 0;
        this._msgCount = 0;

        this._timeLabel = new Date().getTime();
    }
    uniqueId() {
        return `${this.ID.sysTypeId}-${this.ID.deviceId}-${this.ID.moduleId}-${this.ID.paramId}`;
    }

    lastValue() {
        return this.value;
    }

    async currentValue() {

        try {
            let valueData = await this.paramProvider.reqParamValue(this.ID);
            this.value.deserialize(valueData);
            this.lastError = 0;

            return this.value;
        } catch (err) {
            this.value = new ParamValue()
            this.value.valueTime = new Date().getTime()
            this.value.value = undefined;
            this.lastError = err;
            console.error(err);
            return undefined;
        }
    }

    convertValue(value) {
        let checkIsNaN = (arg) => {
            if (isNaN(arg)) {
                this.lastError = -1;
                console.log("Set value error! The arg is not a number");
                return "";
            }
            return arg;
        }

        let resValue; 

        switch (this.valueFormat) {
            case ValueFormatEnum.Bin:
                resValue = parseInt(value, 2).toString();
                break;
            case ValueFormatEnum.Hex:
                resValue = parseInt(value, 16).toString();
                break;
            case ValueFormatEnum.Int:
                resValue = value.toString();
                break;
            case ValueFormatEnum.Float:
                resValue = parseFloat(value).toString();
                break;

            default: 
                resValue = value.toString();
        }

        return checkIsNaN(resValue);
    }

    async setValue(value) {
        try {
            let strValue = this.convertValue(value)
            if (strValue === "") return;
            let valueData = await this.paramProvider.setParamValue(this.ID, strValue);
            this.value.deserialize(valueData);
            this.lastError = 0;
        } catch (err) {
            this.value = new ParamValue()
            this.value.valueTime = new Date().getTime()
            this.value.value = undefined;
            this.lastError = err;
        }
    }

    asyncSetValue(value) {

        let strValue = this.convertValue(value);
        if (strValue === "") return;
        this.paramProvider.setParamValue(this.ID, strValue);
        this.lastError = 0;
        this.value.valueTime = new Date().getTime()

        this.value.value = undefined;
    }

    displayValue(value, useFormat, bitNum) {
        const valueAtBit = function (num, bit) {
            var result = []
            while(num != 0) {
                result.push(num % 2)
                num = Math.floor(num / 2)
            }        
            result.unshift(null)
            
            return result[bit]
        }
    
        switch (this.valueFormat) {
            case ValueFormatEnum.Float: {
                var prec = 1;
                if (value > -1 && value < 1) prec = 4;
                else if (value > -10 && value < 10) prec = 2;
    
                return value.toFixed(prec)
            }
            case ValueFormatEnum.Text: return this.valueTexts[value]
            case ValueFormatEnum.Bin: {
                if (!useFormat) return value;

                if (bitNum == undefined) {
                    return value !== 0 ? Number(value).toString(2).padStart(16, "0") : "0000_0000_0000_0000";
                }

                return valueAtBit(Number(value), bitNum);
            }
            case ValueFormatEnum.Hex: {
                if (useFormat) {
                    return  "0x" +((value>>>0).toString(16).toUpperCase());
                } else {
                    return value;
                }
            }
            
            default: return value;
        }
    }

    displayName() {
        return this.name +
        " [" + Number(this.ID.moduleId).toString(10).padStart(2, '0') +
        "."  + Number(this.ID.paramId).toString(10).padStart(2, '0') +
        "]";
    }

    fullName() {
        return this.name +
        " [" + Number(this.ID.deviceId).toString(10).padStart(2, '0')
        + "." + Number(this.ID.moduleId).toString(10).padStart(2, '0')
        + "."  + Number(this.ID.paramId).toString(10).padStart(2, '0')
        + "]";
    }

    isReadOnly() {
        return this.rw === "R";
    }

    start(controller) {
        this.controller = controller;
    }
    
    isStreamming() {
        return this.stream !== null;
    }
    
    openValueStream(frequency, buffObjectCount) {
        try {

     
            this._timeLabel = new Date().getTime();
            console.time(`The stream elapsed time(${this._timeLabel})`);
            this._byteCount = 0;
            this._msgCount = 0;

            this.buffIndex = 0;
            this.buffer = [];
            this.buffObjectCount = (buffObjectCount !== undefined) ? buffObjectCount : this.buffObjectCount;
            if (this.buffObjectCount === 0) {
                this.buffObjectCount = 1
            }

            for (var i = 0; i < this.buffObjectCount; i++) {
                this.buffer[i] = new ParamValue();
            }

            this.stream = new ReadableStream(this);
            this.reader = this.stream.reader;

            this.lastError = 0;
            this.valueCounter = 0;
            this.value = new ParamValue()

//          this.paramProvider.reqParamValue(this.ID, "on", { frequency: frequency });

        } catch (error) {
            this.value = new ParamValue()
            this.lastError = error;
            console.error(error);
        }

        return this.stream;
    }

    async closeValueStream() {
//      if (!this.stream) return;

        try {
            console.log(`try to close param stream, ID = ${JSON.stringify(this.ID)}`);
            this.paramProvider.reqParamValue(this.ID, "off")
            this.destroyStream();

        } catch (error) {
            this.lastError = error;
        }
    }

    destroyStream() {
        if (this.stream === null) {
            return;
        }

        this.valueCounter = 0;
        this.controller.close();

        this.stream = null;
        this.controller = null;
    }

    async streamValue(value, valueTime, messageDataLength) {

//      console.log(`Streaming value, param id = ${this.id}, value = ${value}`)

        if (!this.stream) {
            console.warn(`Receiving value error. The param stream is deactivated now. Param id = ${JSON.stringify(this.ID)}, value = ${JSON.stringify(value)}`);
            await this.closeValueStream();
            return;
        }

        if (value === undefined) {
            console.warn(`Receiving value error. The value is not defined. Param id = ${JSON.stringify(this.ID)}, value = ${JSON.stringify(value)}`);
            return;
        }

//      if (this.value.value == value && this.value.valueTime == valueTime) {
//          return;
//      }

        this.valueCounter = this.valueCounter < INT_MAX ? this.valueCounter + 1 : INT_MAX;
        var timeStr = moment(valueTime).format("hh:mm:ss.SSS");
//      console.log(`Streaming value, param id = ${this.id}, value = ${value}, time = ${timeStr}, counter = ${this.valueCounter}`)

        let pValue = this.buffer[this.buffIndex];
        if (pValue === undefined) return;

        pValue.paramId = this.id;
        pValue.deviceId = this.deviceId;
        pValue.moduleId = this.moduleId;
        pValue.value = value;
        pValue.valueTime = valueTime;
        pValue.format = this.valueFormat;

        this.buffer[this.buffIndex] = pValue;
        this.value = pValue;

        let currTimeMs = new Date().getTime();
        let pValueActualityTime = pValue.valueTime > 0 ? currTimeMs - pValue.valueTime : 0

        if (pValueActualityTime > MAX_VALUE_ACTUALITY_MS) {
            console.warn(`Param value actuality exceeded, id = ${this.id}, delta = ${pValueActualityTime}ms`)
        }

        const isFinished = (pValue.value == -INT_MIN);

        if (isFinished === true) {
            console.log(`The param stream is finished, param id = ${JSON.stringify(this.ID)}, received items = ${this._msgCount}, bytes = ${this._byteCount}`);
            console.timeEnd(`The stream elapsed time(${this._timeLabel})`);

            this.buffer.splice(this.buffIndex)
            if (this.buffer.length > 0) {
                this.controller?.enqueue(this.buffer)
            }

            this.destroyStream();
            return;
        }

        // console.log(`Received: ${JSON.stringify(pValue)}`);
        this._byteCount += messageDataLength;
        this._msgCount++;

        //      this.buffer[this.buffIndex] = pValue;
        this.buffIndex++;

        if (this.buffIndex >= this.buffObjectCount) {
            this.controller?.enqueue(this.buffer)
            this.buffIndex = 0;
        }
        
    }

    lastError() {
        return this.lastError;
    }

    deserialize(data, sysTypeId) {
        this.ID = {
            sysTypeId: sysTypeId,
            deviceId: data.device_id,
            moduleId: data.module_id,
            paramId: data.param_id
        }

        this.deviceId = data.device_id;
        this.moduleId = data.module_id;
        this.id = data.param_id;
        this.name = data.name;
        this.desc = data.desc;
        this.valueUnit = data.value_unit;
        this.valueFormat = data.value_format;
        this.valueScale = data.value_scale;
        this.valueTexts = data.value_texts;

        this.rw = data.rw;
    }
}

class ParamValue {
    constructor() {
        this.paramId = 0;
        this.deviceId = 0;
        this.moduleId = 0;
        this.format = ValueFormatEnum.Undefined;
        this.valueTime = 0;
        this.scale = 0.0;
        this.value = 0.0;
    }

    deserialize(data) {
        // формат ожидаемых данных: {param_id: , device_id:, value:{value, format, time, scale}}
        this.ID = {
            deviceId: data.device_id,
            moduleId: data.module_id,
            paramId: data.param_id
        }

        this.paramId = data.param_id;
        this.deviceId = data.device_id;
        this.moduleId = data.module_id;
        this.value = data.value.value;
        this.valueTime = data.value.time;
        this.format = data.format;
        this.scale = data.scale;
    }
}

export class ValueFormatEnum {
    static Undefined = 0;
    static Bin = 1;
    static Int = 2;
    static Float = 3;
    static Hex = 4;
    static Text = 5;
    static ASCII = 6;
}